#!/usr/bin/python

import inners_data_import_tools
import sys

inners_data_import_tools.setTestMode(True)      # Force logging to console


if(len(sys.argv) > 1):
    if(sys.argv[1] == 'all'):
        print "Aggregating all data..."
        inners_data_import_tools.createAggregationTodoTable()       # Will create connection to database that we can use below

        inners_data_import_tools.dbCursor.execute("truncate agg_plant_data")        # Will speed subsequent inserts
        intervals = inners_data_import_tools.getAggregationIntervals()
        table     = inners_data_import_tools.getIntervalsToUpdateTable()

        for interval in intervals:
            inners_data_import_tools.dbCursor.execute("insert into " + table + " (interval_id, interval_type) values(-2147483648, '" + interval + "')")
            inners_data_import_tools.dbCursor.execute("insert into " + table + " (interval_id, interval_type) values( 2147483647, '" + interval + "')")

        inners_data_import_tools.dbConn.commit()
else:
    print "Aggregating all periods specified in intervals_to_update table.  To aggregate everything, run 'python aggregate.py all'"


inners_data_import_tools.computeAggregateData()
inners_data_import_tools.fillKpiParts()
inners_data_import_tools.fillKpis()


