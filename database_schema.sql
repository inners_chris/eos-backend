--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: inners_data; Type: SCHEMA; Schema: -; Owner: inners_data
--

CREATE SCHEMA inners_data;


ALTER SCHEMA inners_data OWNER TO inners_data;

SET search_path = inners_data, pg_catalog;

--
-- Name: compose_name(text, text, text, integer, text, text, integer, text, text, text); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION compose_name(text, text, text, integer, text, text, integer, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
   begin
      return concat(compose_stem($1, $2, $3, $4, $5, $6, $7, $8, $9), '_', $10);
   end;

$_$;


ALTER FUNCTION inners_data.compose_name(text, text, text, integer, text, text, integer, text, text, text) OWNER TO inners_data;

--
-- Name: compose_stem(text, text, text, integer, text, text, integer, text, text); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION compose_stem(text, text, text, integer, text, text, integer, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
   begin
      return concat($1, '_', $2, COALESCE($3, ''), COALESCE(cast($4 as text), ''), '_', $5, COALESCE($6, ''), COALESCE(cast($7 as text), ''), '_', $8, '_', $9);
   end;

$_$;


ALTER FUNCTION inners_data.compose_stem(text, text, text, integer, text, text, integer, text, text) OWNER TO inners_data;

--
-- Name: day_id_from_timestamp(timestamp without time zone); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION day_id_from_timestamp(timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   declare d timestamp;
   
   begin
      d = $1 - interval '1 second';
      return extract(year from d) * 1000 + CAST( to_char(d, 'DDD') AS integer ); 
   end;

$_$;


ALTER FUNCTION inners_data.day_id_from_timestamp(timestamp without time zone) OWNER TO inners_data;

--
-- Name: days_in_interval(integer, text); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION days_in_interval(integer, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   begin
   return
      case $2 when 'day' then 1
        when 'week' then 7
        when  'month' then 
        DATE_PART('days', 
        DATE_TRUNC('month', timestamp_from_month_id($1)) - 
        (DATE_TRUNC('month', timestamp_from_month_id($1)) - '1 MONTH'::INTERVAL))
        
        when  'year' then  
        DATE_PART('days', 
        DATE_TRUNC('year', timestamp_from_year_id($1)) - 
        (DATE_TRUNC('year', timestamp_from_year_id($1)) - '1 YEAR'::INTERVAL))
     end;
   end

$_$;


ALTER FUNCTION inners_data.days_in_interval(integer, text) OWNER TO inners_data;

--
-- Name: get_benchmark(real, integer); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION get_benchmark(val real, bmid integer) RETURNS real
    LANGUAGE plpgsql
    AS $$
  declare
  xmin real; xmax real; ymin real; ymax real;
  begin
        select y into ymin from benchmark_values where benchmark_id = bmid and x = val;

        if ymin is not null then return ymin; end if;
       
    select x into xmin from benchmark_values where benchmark_id = bmid and x < val order by x desc limit 1;
    select x into xmax from benchmark_values where benchmark_id = bmid and x > val order by x limit 1;
    select y into ymin from benchmark_values where benchmark_id = bmid and x < val order by x desc limit 1;
    select y into ymax from benchmark_values where benchmark_id = bmid and x > val order by x limit 1;

    return ymin + (ymax - ymin) * ((val-xmin) / (xmax-xmin));
  end; $$;


ALTER FUNCTION inners_data.get_benchmark(val real, bmid integer) OWNER TO inners_data;

--
-- Name: hours_in_interval(integer, text); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION hours_in_interval(integer, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   begin
   return
      days_in_interval($1,$2) * 24;
   end;

$_$;


ALTER FUNCTION inners_data.hours_in_interval(integer, text) OWNER TO inners_data;

--
-- Name: month_id_from_timestamp(timestamp without time zone); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION month_id_from_timestamp(timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   declare d timestamp;
   
   begin
      d = $1 - interval '1 second';
      return extract(year from d) * 1000 + CAST( to_char(d, 'MM') AS integer ); 
   end;

$_$;


ALTER FUNCTION inners_data.month_id_from_timestamp(timestamp without time zone) OWNER TO inners_data;

--
-- Name: populate_date_ids(); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION populate_date_ids() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

declare d timestamp;

	begin

   if new.date is NULL then
	raise exception 'Need a date!';
   end if;


   
   d = new.date - interval '1 second';
   new.day_id   := extract(year from d) * 1000 + CAST( to_char(d, 'DDD') AS integer );
   new.week_id  := extract(year from d) * 1000 + CAST( to_char(d, 'IW') AS integer );
   new.month_id := extract(year from d) * 1000 + CAST( to_char(d, 'MM') AS integer );
   new.year_id  := extract(year from d);

	return new;

   end; $$;


ALTER FUNCTION inners_data.populate_date_ids() OWNER TO inners_data;

--
-- Name: product_aggregate(real, real); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION product_aggregate(real, real) RETURNS real
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$ select $1 * $2; $_$;


ALTER FUNCTION inners_data.product_aggregate(real, real) OWNER TO inners_data;

--
-- Name: product_aggregate(double precision, double precision); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION product_aggregate(double precision, double precision) RETURNS double precision
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$ select $1 * $2; $_$;


ALTER FUNCTION inners_data.product_aggregate(double precision, double precision) OWNER TO inners_data;

--
-- Name: timestamp_from_day_id(integer); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION timestamp_from_day_id(integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $_$
   begin
      return date(cast($1 / 1000 as text) || '-01-01') + ($1 % 1000) * interval '1 day';
   end;

$_$;


ALTER FUNCTION inners_data.timestamp_from_day_id(integer) OWNER TO inners_data;

--
-- Name: timestamp_from_month_id(integer); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION timestamp_from_month_id(integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $_$
   begin
      return date(cast($1 / 1000 as text) || '-01-01') + ($1 % 1000) * interval '1 month';
   end;

$_$;


ALTER FUNCTION inners_data.timestamp_from_month_id(integer) OWNER TO inners_data;

--
-- Name: timestamp_from_week_id(integer); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION timestamp_from_week_id(integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $_$
   begin
      return to_date(cast(($1+1) / 1000 as text) || ' ' || cast(($1+1) % 1000 as text), 'IYYY IW');
   end;

$_$;


ALTER FUNCTION inners_data.timestamp_from_week_id(integer) OWNER TO inners_data;

--
-- Name: timestamp_from_year_id(integer); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION timestamp_from_year_id(integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $_$
   begin
      return date(cast($1 / 1000 as text) || '-01-01') + interval '1 year';
   end;

$_$;


ALTER FUNCTION inners_data.timestamp_from_year_id(integer) OWNER TO inners_data;

--
-- Name: week_id_from_timestamp(timestamp without time zone); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION week_id_from_timestamp(timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   declare d timestamp;
   
   begin
      d = $1 - interval '1 second';
      return CAST( to_char(d, 'IYYY') AS integer) * 1000 + CAST( to_char(d, 'IW') AS integer ); 
   end;

$_$;


ALTER FUNCTION inners_data.week_id_from_timestamp(timestamp without time zone) OWNER TO inners_data;

--
-- Name: year_id_from_timestamp(timestamp without time zone); Type: FUNCTION; Schema: inners_data; Owner: inners_data
--

CREATE FUNCTION year_id_from_timestamp(timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
   declare d timestamp;
   
   begin
      d = $1 - interval '1 second';
      return extract(year from d); 
   end;

$_$;


ALTER FUNCTION inners_data.year_id_from_timestamp(timestamp without time zone) OWNER TO inners_data;

--
-- Name: product(real); Type: AGGREGATE; Schema: inners_data; Owner: inners_data
--

CREATE AGGREGATE product(real) (
    SFUNC = inners_data.product_aggregate,
    STYPE = real,
    INITCOND = '1.0'
);


ALTER AGGREGATE inners_data.product(real) OWNER TO inners_data;

--
-- Name: product(double precision); Type: AGGREGATE; Schema: inners_data; Owner: inners_data
--

CREATE AGGREGATE product(double precision) (
    SFUNC = inners_data.product_aggregate,
    STYPE = double precision,
    INITCOND = '1.0'
);


ALTER AGGREGATE inners_data.product(double precision) OWNER TO inners_data;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: kpis; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE kpis (
    id integer NOT NULL,
    date timestamp without time zone,
    kpi_id text,
    value double precision,
    interval_id integer,
    interval_type text,
    plant text
);


ALTER TABLE inners_data.kpis OWNER TO inners_data;

--
-- Name: ALERT rev3; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "ALERT rev3" AS
    ((((((((((((((((((((((((((((((((((((((((((((((((SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 99::double precision)) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '1127700'::text AS "Min value", '1274700'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_TEvol'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 1274700::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 99::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '80'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perOH'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 80::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '935040'::text AS "Min value", '1227240'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPIp_CO2_Emissions'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 1227240::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '13'::text AS "Min value", '20'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 20::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 21::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '7.792'::text AS "Min value", '10.227'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 10.2270000000000003::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '1127700'::text AS "Min value", '1274700'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_TEvol'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 1127700::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '80'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perOH'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '935040'::text AS "Min value", '1227240'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 935040::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '13'::text AS "Min value", '20'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 13::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 16::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '7.792'::text AS "Min value", '10.227'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 7.79199999999999982::double precision));


ALTER TABLE inners_data."ALERT rev3" OWNER TO inners_data;

--
-- Name: ALERT rev4; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "ALERT rev4" AS
    ((((((((((((((((((((((((((((((((((((((((((((((((SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 99::double precision)) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '1127700'::text AS "Min value", '1274700'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_TEvol'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 1274700::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 99::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '80'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perOH'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 80::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0.17'::text AS "Min value", '0.7'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 0.699999999999999956::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '935040'::text AS "Min value", '1227240'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPIp_CO2_Emissions'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 1227240::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '13'::text AS "Min value", '20'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 20::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '10'::text AS "Min value", '40'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 40::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 21::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 21::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0.04'::text AS "Min value", '0.125'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 0.125::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 100::double precision))) UNION SELECT 'ALERT: value>max'::text AS "ALERT", '7.792'::text AS "Min value", '10.227'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value > 10.2270000000000003::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '1127700'::text AS "Min value", '1274700'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_TEvol'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 1127700::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '99'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '80'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perOH'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0.17'::text AS "Min value", '0.7'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0.170000000000000012::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '935040'::text AS "Min value", '1227240'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 935040::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '13'::text AS "Min value", '20'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 13::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '10'::text AS "Min value", '40'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 10::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 16::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '16'::text AS "Min value", '21'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 16::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0.04'::text AS "Min value", '0.125'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0.0400000000000000008::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '0'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 0::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '80'::text AS "Min value", '100'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 80::double precision))) UNION SELECT 'ALERT: value<min'::text AS "ALERT", '7.792'::text AS "Min value", '10.227'::text AS "Max value", kpis.date, kpis.kpi_id, kpis.value, kpis.interval_type, kpis.plant FROM kpis WHERE (((kpis.kpi_id = 'KPI_CO2_EmissionPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.value < 7.79199999999999982::double precision));


ALTER TABLE inners_data."ALERT rev4" OWNER TO inners_data;

--
-- Name: agg_plant_data; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE agg_plant_data (
    id integer NOT NULL,
    date timestamp without time zone,
    sensor_id text,
    value double precision,
    interval_id integer,
    interval_type text NOT NULL,
    plant text,
    component_count integer,
    zero_count integer
);


ALTER TABLE inners_data.agg_plant_data OWNER TO inners_data;

--
-- Name: TABLE agg_plant_data; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE agg_plant_data IS 'Aggregated plant data.';


--
-- Name: COLUMN agg_plant_data.plant; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN agg_plant_data.plant IS 'Plant to which the data pertains';


--
-- Name: COLUMN agg_plant_data.component_count; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN agg_plant_data.component_count IS 'A count of the number of items that were aggregated to generate this value';


--
-- Name: COLUMN agg_plant_data.zero_count; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN agg_plant_data.zero_count IS 'How many values of zero were included in this aggregate?';


--
-- Name: AVG_check; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "AVG_check" AS
    SELECT day.sensor_idx, day.daily_avg, week.weekly_avg, month.monthly_avg, year.year_avg FROM ((((SELECT agg_plant_data.sensor_id AS sensor_idx, avg(agg_plant_data.value) AS daily_avg FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'day'::text) GROUP BY agg_plant_data.sensor_id) day LEFT JOIN (SELECT agg_plant_data.sensor_id, avg(agg_plant_data.value) AS weekly_avg FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'week'::text) GROUP BY agg_plant_data.sensor_id) week ON ((day.sensor_idx = week.sensor_id))) LEFT JOIN (SELECT agg_plant_data.sensor_id, avg(agg_plant_data.value) AS monthly_avg FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'month'::text) GROUP BY agg_plant_data.sensor_id) month ON ((day.sensor_idx = month.sensor_id))) LEFT JOIN (SELECT agg_plant_data.sensor_id, avg(agg_plant_data.value) AS year_avg FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'year'::text) GROUP BY agg_plant_data.sensor_id) year ON ((day.sensor_idx = year.sensor_id)));


ALTER TABLE inners_data."AVG_check" OWNER TO inners_data;

--
-- Name: BUR_Report_rev2; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_Report_rev2" AS
    SELECT dariotable.datatime, dariotable."BUR_ANDI_PUMhsl1_SUM_CAL_EA", dariotable."BUR_ANDI_PUMhsl2_SUM_CAL_EA", dariotable."BUR_ANDIi1_STIR_SUM_EA", dariotable."BUR_ANDIi2_STIR_SUM_EA", dariotable."BUR_BIO_BLO1_SUM_CAL_EA", dariotable."BUR_BIO_BLO12_SUM_EA", dariotable."BUR_BIO_BLO2_SUM_CAL_EA", dariotable."BUR_BIO_BLO3_SUM_CAL_EA", dariotable."BUR_BIO_BLO34_SUM_EA", dariotable."BUR_BIO_BLO4_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_EA", dariotable."BUR_BIO_BLO6_SUM_CAL_EA", dariotable."BUR_BIO_BLO6_SUM_EA", dariotable."BUR_BIO_PUMrcir1_SUM_CAL_EA", dariotable."BUR_BIO_PUMrcir2_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR2_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR4_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR2_SUM_CAL_EA", dariotable."BUR_DSTH_PFE_SUM_OH", dariotable."BUR_DSTHo_PUM1_SUM_OH", dariotable."BUR_DSTHo_PUM2_SUM_OH", dariotable."BUR_ELPS_ELPS_SUM_EA", dariotable."BUR_FLA_TOR_SUM_OH", dariotable."BUR_FLOF_BLO4_SUM_CAL_EA", dariotable."BUR_FLOF_BLO5_SUM_CAL_EA", dariotable."BUR_FLOF_BLObw1_SUM_EA", dariotable."BUR_FLOF_BLObw2_SUM_EA", dariotable."BUR_FLOF_BLObw3_SUM_EA", dariotable."BUR_FLOF_PUMbw1_SUM_EA", dariotable."BUR_FLOF_PUMbw2_SUM_EA", dariotable."BUR_FLOF_PUMbw3_SUM_EA", dariotable."BUR_FLOF_PUMdrw_SUM_OH", dariotable."BUR_FLOF_PUMpre1_SUM_OH", dariotable."BUR_FLOF_PUMpre2_SUM_OH", dariotable."BUR_FLOF_PUMsluw1_SUM_CAL_EA", dariotable."BUR_FLOF_PUMsluw2_SUM_CAL_EA", dariotable."BUR_FLOF_STIRsluw_SUM_CAL_EA", dariotable."BUR_FLOFi_SAMPww_SUM_OH", dariotable."BUR_GLOB_BOIL_SUM_OH", dariotable."BUR_HPS_PUM1_SUM_CAL_EA", dariotable."BUR_HPS_PUM2_SUM_CAL_EA", dariotable."BUR_HPS_PUM3_SUM_CAL_EA", dariotable."BUR_HPS_PUM4_SUM_CAL_EA", dariotable."BUR_IPS_PUM1_SUM_EA", dariotable."BUR_IPS_PUM2_SUM_EA", dariotable."BUR_IPS_PUM3_SUM_EA", dariotable."BUR_IPS_PUM4_SUM_EA", dariotable."BUR_IPS_PUM5_SUM_EA", dariotable."BUR_IPS_PUM6_SUM_EA", dariotable."BUR_PCL_PUMps1_SUM_CAL_EA", dariotable."BUR_PCL_PUMps2_SUM_CAL_EA", dariotable."BUR_PCL_SCRA1_SUM_CAL_EA", dariotable."BUR_PCL_SCRA2_SUM_CAL_EA", dariotable."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", dariotable."BUR_SAND_BLO2_SUM_CAL_EA", dariotable."BUR_SAND_BLO3_SUM_CAL_EA", dariotable."BUR_SAND_BLOair1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA2_SUM_CAL_EA", dariotable."BUR_SAND_STIR_SUM_CAL_EA", dariotable."BUR_SBA_JCL1_SUM_CAL_EA", dariotable."BUR_SBA_JCL2_SUM_CAL_EA", dariotable."BUR_SBA_PUM1_SUM_CAL_EA", dariotable."BUR_SBA_PUM2_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas1_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas2_SUM_CAL_EA", dariotable."BUR_SCL1_SCRA_SUM_CAL_EA", dariotable."BUR_SCL2_SCRA_SUM_CAL_EA", dariotable."BUR_SCL3_SCRA_SUM_CAL_EA", dariotable."BUR_SCR_GRI1_SUM_CAL_EA", dariotable."BUR_SCR_GRI2_SUM_CAL_EA", dariotable."BUR_SDEW_COMP_SUM_OH", dariotable."BUR_SDEW_DOSflo1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSflo2_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre2_SUM_CAL_EA", dariotable."BUR_SDEW_PUMhp_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", dariotable."BUR_SSTH_CEN1_SUM_CAL_EA", dariotable."BUR_SSTH_CEN2_SUM_CAL_EA", dariotable."BUR_SSTHi1_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHi2_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", dariotable."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", dariotable."BUR_TWT_PUM1_SUM_CAL_EA", dariotable."BUR_TWT_PUM2_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw1_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw2_SUM_CAL_EA", dariotable."BUR_WWTPo_SAMPww_SUM_OH" FROM (SELECT "time".datatime, ah1."BUR_ANDI_PUMhsl1_SUM_CAL_EA", ah2."BUR_ANDI_PUMhsl2_SUM_CAL_EA", b4."BUR_ANDIi1_STIR_SUM_EA", b5."BUR_ANDIi2_STIR_SUM_EA", h14."BUR_BIO_BLO1_SUM_CAL_EA", b0."BUR_BIO_BLO12_SUM_EA", h15."BUR_BIO_BLO2_SUM_CAL_EA", h16."BUR_BIO_BLO3_SUM_CAL_EA", b3."BUR_BIO_BLO34_SUM_EA", h170."BUR_BIO_BLO4_SUM_CAL_EA", h17."BUR_BIO_BLO5_SUM_CAL_EA", b1."BUR_BIO_BLO5_SUM_EA", h18."BUR_BIO_BLO6_SUM_CAL_EA", b2."BUR_BIO_BLO6_SUM_EA", h210."BUR_BIO_PUMrcir1_SUM_CAL_EA", h21."BUR_BIO_PUMrcir2_SUM_CAL_EA", h1."BUR_BIOaer1_STIR1_SUM_CAL_EA", h100."BUR_BIOaer1_STIR2_SUM_CAL_EA", h2."BUR_BIOaer2_STIR1_SUM_CAL_EA", h3."BUR_BIOaer2_STIR2_SUM_CAL_EA", h4."BUR_BIOaer3_STIR1_SUM_CAL_EA", h6."BUR_BIOaer3_STIR2_SUM_CAL_EA", h7."BUR_BIOanox1_STIR3_SUM_CAL_EA", h8."BUR_BIOanox1_STIR4_SUM_CAL_EA", h9."BUR_BIOanox2_STIR3_SUM_CAL_EA", h10."BUR_BIOanox2_STIR4_SUM_CAL_EA", h11."BUR_BIOanox3_STIR3_SUM_CAL_EA", h12."BUR_BIOanox3_STIR4_SUM_CAL_EA", h19."BUR_BIOP2_STIR1_SUM_CAL_EA", h20."BUR_BIOP2_STIR2_SUM_CAL_EA", oth3."BUR_DSTH_PFE_SUM_OH", oth1."BUR_DSTHo_PUM1_SUM_OH", oth2."BUR_DSTHo_PUM2_SUM_OH", b6."BUR_ELPS_ELPS_SUM_EA", oth4."BUR_FLA_TOR_SUM_OH", flof1."BUR_FLOF_BLO4_SUM_CAL_EA", flof2."BUR_FLOF_BLO5_SUM_CAL_EA", b7."BUR_FLOF_BLObw1_SUM_EA", b8."BUR_FLOF_BLObw2_SUM_EA", b9."BUR_FLOF_BLObw3_SUM_EA", b10."BUR_FLOF_PUMbw1_SUM_EA", b11."BUR_FLOF_PUMbw2_SUM_EA", b12."BUR_FLOF_PUMbw3_SUM_EA", flof4."BUR_FLOF_PUMdrw_SUM_OH", flof5."BUR_FLOF_PUMpre1_SUM_OH", flof6."BUR_FLOF_PUMpre2_SUM_OH", flof7."BUR_FLOF_PUMsluw1_SUM_CAL_EA", flof8."BUR_FLOF_PUMsluw2_SUM_CAL_EA", flof9."BUR_FLOF_STIRsluw_SUM_CAL_EA", flof3."BUR_FLOFi_SAMPww_SUM_OH", oth5."BUR_GLOB_BOIL_SUM_OH", oth6."BUR_HPS_PUM1_SUM_CAL_EA", oth7."BUR_HPS_PUM2_SUM_CAL_EA", oth8."BUR_HPS_PUM3_SUM_CAL_EA", oth9."BUR_HPS_PUM4_SUM_CAL_EA", b13."BUR_IPS_PUM1_SUM_EA", b15."BUR_IPS_PUM2_SUM_EA", b16."BUR_IPS_PUM3_SUM_EA", b17."BUR_IPS_PUM4_SUM_EA", b18."BUR_IPS_PUM5_SUM_EA", b19."BUR_IPS_PUM6_SUM_EA", oth10."BUR_PCL_PUMps1_SUM_CAL_EA", oth11."BUR_PCL_PUMps2_SUM_CAL_EA", oth12."BUR_PCL_SCRA1_SUM_CAL_EA", oth13."BUR_PCL_SCRA2_SUM_CAL_EA", oth14."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", oth15."BUR_SAND_BLO2_SUM_CAL_EA", oth16."BUR_SAND_BLO3_SUM_CAL_EA", oth17."BUR_SAND_BLOair1_SUM_CAL_EA", oth18."BUR_SAND_SCRA1_SUM_CAL_EA", oth19."BUR_SAND_SCRA2_SUM_CAL_EA", oth20."BUR_SAND_STIR_SUM_CAL_EA", oth21."BUR_SBA_JCL1_SUM_CAL_EA", oth22."BUR_SBA_JCL2_SUM_CAL_EA", oth23."BUR_SBA_PUM1_SUM_CAL_EA", oth24."BUR_SBA_PUM2_SUM_CAL_EA", oth28."BUR_SCL_PUMsas1_SUM_CAL_EA", oth29."BUR_SCL_PUMsas2_SUM_CAL_EA", oth25."BUR_SCL1_SCRA_SUM_CAL_EA", oth26."BUR_SCL2_SCRA_SUM_CAL_EA", oth27."BUR_SCL3_SCRA_SUM_CAL_EA", oth30."BUR_SCR_GRI1_SUM_CAL_EA", oth31."BUR_SCR_GRI2_SUM_CAL_EA", oth58."BUR_SDEW_COMP_SUM_OH", oth33."BUR_SDEW_DOSflo1_SUM_CAL_EA", oth34."BUR_SDEW_DOSflo2_SUM_CAL_EA", oth35."BUR_SDEW_DOSpre1_SUM_CAL_EA", oth36."BUR_SDEW_DOSpre2_SUM_CAL_EA", oth39."BUR_SDEW_PUMhp_SUM_CAL_EA", oth37."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", oth38."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", oth40."BUR_SSTH_CEN1_SUM_CAL_EA", oth41."BUR_SSTH_CEN2_SUM_CAL_EA", oth42."BUR_SSTHi1_PUMsas_SUM_CAL_EA", oth43."BUR_SSTHi2_PUMsas_SUM_CAL_EA", oth44."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", oth71."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", oth46."BUR_TWT_PUM1_SUM_CAL_EA", oth47."BUR_TWT_PUM2_SUM_CAL_EA", oth49."BUR_WWTP_PUMpw1_SUM_CAL_EA", oth50."BUR_WWTP_PUMpw2_SUM_CAL_EA", oth48."BUR_WWTPo_SAMPww_SUM_OH" FROM (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'day'::text) ORDER BY agg_plant_data.date) "time" LEFT JOIN (SELECT DISTINCT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO12_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO12_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b0 ON (("time".datatime = b0.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b1 ON (("time".datatime = b1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b2 ON (("time".datatime = b2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO34_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO34_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b3 ON (("time".datatime = b3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h1 ON (("time".datatime = h1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h100 ON (("time".datatime = h100.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h2 ON (("time".datatime = h2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h3 ON (("time".datatime = h3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h4 ON (("time".datatime = h4.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h6 ON (("time".datatime = h6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h7 ON (("time".datatime = h7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h8 ON (("time".datatime = h8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h9 ON (("time".datatime = h9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h10 ON (("time".datatime = h10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h11 ON (("time".datatime = h11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h12 ON (("time".datatime = h12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h14 ON (("time".datatime = h14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h15 ON (("time".datatime = h15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h16 ON (("time".datatime = h16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h170 ON (("time".datatime = h170.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h17 ON (("time".datatime = h17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO6_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h18 ON (("time".datatime = h18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.6)::double precision) AS "BUR_BIOP2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h19 ON (("time".datatime = h19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.4)::double precision) AS "BUR_BIOP2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h20 ON (("time".datatime = h20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h210 ON (("time".datatime = h210.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h21 ON (("time".datatime = h21.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi1_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi1_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b4 ON (("time".datatime = b4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi2_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi2_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b5 ON (("time".datatime = b5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah1 ON (("time".datatime = ah1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah2 ON (("time".datatime = ah2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ELPS_ELPS_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ELPS_ELPS_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b6 ON (("time".datatime = b6.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b7 ON (("time".datatime = b7.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b8 ON (("time".datatime = b8.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b9 ON (("time".datatime = b9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b10 ON (("time".datatime = b10.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b11 ON (("time".datatime = b11.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b12 ON (("time".datatime = b12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof1 ON (("time".datatime = flof1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof2 ON (("time".datatime = flof2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOFi_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOFi_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof3 ON (("time".datatime = flof3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMdrw_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMdrw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof4 ON (("time".datatime = flof4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre1_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof5 ON (("time".datatime = flof5.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre2_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof6 ON (("time".datatime = flof6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof7 ON (("time".datatime = flof7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof8 ON (("time".datatime = flof8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.8)::double precision) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_STIRsluw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof9 ON (("time".datatime = flof9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b13 ON (("time".datatime = b13.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b15 ON (("time".datatime = b15.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b16 ON (("time".datatime = b16.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM4_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM4_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b17 ON (("time".datatime = b17.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b18 ON (("time".datatime = b18.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b19 ON (("time".datatime = b19.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM1_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth1 ON (("time".datatime = oth1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM2_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth2 ON (("time".datatime = oth2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_DSTH_PFE_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTH_PFE_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth3 ON (("time".datatime = oth3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLA_TOR_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLA_TOR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth4 ON (("time".datatime = oth4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_GLOB_BOIL_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_GLOB_BOIL_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth5 ON (("time".datatime = oth5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth6 ON (("time".datatime = oth6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth7 ON (("time".datatime = oth7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth8 ON (("time".datatime = oth8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth9 ON (("time".datatime = oth9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth10 ON (("time".datatime = oth10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth11 ON (("time".datatime = oth11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth12 ON (("time".datatime = oth12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth13 ON (("time".datatime = oth13.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PSTHo_PUMtpsl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth14 ON (("time".datatime = oth14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth15 ON (("time".datatime = oth15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth16 ON (("time".datatime = oth16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLOair1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLOair1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth17 ON (("time".datatime = oth17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth18 ON (("time".datatime = oth18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth19 ON (("time".datatime = oth19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SAND_STIR_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_STIR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth20 ON (("time".datatime = oth20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8.5)::double precision) AS "BUR_SBA_JCL1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth21 ON (("time".datatime = oth21.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (11.58)::double precision) AS "BUR_SBA_JCL2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth22 ON (("time".datatime = oth22.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth23 ON (("time".datatime = oth23.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth24 ON (("time".datatime = oth24.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL1_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL1_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth25 ON (("time".datatime = oth25.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL2_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL2_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth26 ON (("time".datatime = oth26.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL3_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL3_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth27 ON (("time".datatime = oth27.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth28 ON (("time".datatime = oth28.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth29 ON (("time".datatime = oth29.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth30 ON (("time".datatime = oth30.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth31 ON (("time".datatime = oth31.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth33 ON (("time".datatime = oth33.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth34 ON (("time".datatime = oth34.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth35 ON (("time".datatime = oth35.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth36 ON (("time".datatime = oth36.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth37 ON (("time".datatime = oth37.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth38 ON (("time".datatime = oth38.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (46)::double precision) AS "BUR_SDEW_PUMhp_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_PUMhp_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth39 ON (("time".datatime = oth39.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth40 ON (("time".datatime = oth40.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth41 ON (("time".datatime = oth41.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi1_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth42 ON (("time".datatime = oth42.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi2_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth43 ON (("time".datatime = oth43.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo1_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth44 ON (("time".datatime = oth44.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth46 ON (("time".datatime = oth46.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth47 ON (("time".datatime = oth47.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_WWTPo_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTPo_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth48 ON (("time".datatime = oth48.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth49 ON (("time".datatime = oth49.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth50 ON (("time".datatime = oth50.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_SDEW_COMP_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_COMP_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth58 ON (("time".datatime = oth58.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo2_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth71 ON (("time".datatime = oth71.date)))) dariotable;


ALTER TABLE inners_data."BUR_Report_rev2" OWNER TO inners_data;

--
-- Name: BUR_Check_import_process; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_Check_import_process" AS
    (SELECT 'number of values'::text, count("BUR_Report_rev2".datatime) AS datatime, count("BUR_Report_rev2"."BUR_ANDI_PUMhsl1_SUM_CAL_EA") AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_ANDI_PUMhsl2_SUM_CAL_EA") AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_ANDIi1_STIR_SUM_EA") AS "BUR_ANDIi1_STIR_SUM_EA", count("BUR_Report_rev2"."BUR_ANDIi2_STIR_SUM_EA") AS "BUR_ANDIi2_STIR_SUM_EA", count("BUR_Report_rev2"."BUR_BIO_BLO1_SUM_CAL_EA") AS "BUR_BIO_BLO1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO12_SUM_EA") AS "BUR_BIO_BLO12_SUM_EA", count("BUR_Report_rev2"."BUR_BIO_BLO2_SUM_CAL_EA") AS "BUR_BIO_BLO2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO3_SUM_CAL_EA") AS "BUR_BIO_BLO3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO34_SUM_EA") AS "BUR_BIO_BLO34_SUM_EA", count("BUR_Report_rev2"."BUR_BIO_BLO4_SUM_CAL_EA") AS "BUR_BIO_BLO4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_CAL_EA") AS "BUR_BIO_BLO5_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_EA") AS "BUR_BIO_BLO5_SUM_EA", count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_CAL_EA") AS "BUR_BIO_BLO6_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_EA") AS "BUR_BIO_BLO6_SUM_EA", count("BUR_Report_rev2"."BUR_BIO_PUMrcir1_SUM_CAL_EA") AS "BUR_BIO_PUMrcir1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIO_PUMrcir2_SUM_CAL_EA") AS "BUR_BIO_PUMrcir2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer1_STIR1_SUM_CAL_EA") AS "BUR_BIOaer1_STIR1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer1_STIR2_SUM_CAL_EA") AS "BUR_BIOaer1_STIR2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer2_STIR1_SUM_CAL_EA") AS "BUR_BIOaer2_STIR1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer2_STIR2_SUM_CAL_EA") AS "BUR_BIOaer2_STIR2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer3_STIR1_SUM_CAL_EA") AS "BUR_BIOaer3_STIR1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOaer3_STIR2_SUM_CAL_EA") AS "BUR_BIOaer3_STIR2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox1_STIR3_SUM_CAL_EA") AS "BUR_BIOanox1_STIR3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox1_STIR4_SUM_CAL_EA") AS "BUR_BIOanox1_STIR4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox2_STIR3_SUM_CAL_EA") AS "BUR_BIOanox2_STIR3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox2_STIR4_SUM_CAL_EA") AS "BUR_BIOanox2_STIR4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox3_STIR3_SUM_CAL_EA") AS "BUR_BIOanox3_STIR3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOanox3_STIR4_SUM_CAL_EA") AS "BUR_BIOanox3_STIR4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOP2_STIR1_SUM_CAL_EA") AS "BUR_BIOP2_STIR1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_BIOP2_STIR2_SUM_CAL_EA") AS "BUR_BIOP2_STIR2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_DSTH_PFE_SUM_OH") AS "BUR_DSTH_PFE_SUM_OH", count("BUR_Report_rev2"."BUR_DSTHo_PUM1_SUM_OH") AS "BUR_DSTHo_PUM1_SUM_OH", count("BUR_Report_rev2"."BUR_DSTHo_PUM2_SUM_OH") AS "BUR_DSTHo_PUM2_SUM_OH", count("BUR_Report_rev2"."BUR_ELPS_ELPS_SUM_EA") AS "BUR_ELPS_ELPS_SUM_EA", count("BUR_Report_rev2"."BUR_FLA_TOR_SUM_OH") AS "BUR_FLA_TOR_SUM_OH", count("BUR_Report_rev2"."BUR_FLOF_BLO4_SUM_CAL_EA") AS "BUR_FLOF_BLO4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_FLOF_BLO5_SUM_CAL_EA") AS "BUR_FLOF_BLO5_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_FLOF_BLObw1_SUM_EA") AS "BUR_FLOF_BLObw1_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_BLObw2_SUM_EA") AS "BUR_FLOF_BLObw2_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_BLObw3_SUM_EA") AS "BUR_FLOF_BLObw3_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_PUMbw1_SUM_EA") AS "BUR_FLOF_PUMbw1_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_PUMbw2_SUM_EA") AS "BUR_FLOF_PUMbw2_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_PUMbw3_SUM_EA") AS "BUR_FLOF_PUMbw3_SUM_EA", count("BUR_Report_rev2"."BUR_FLOF_PUMdrw_SUM_OH") AS "BUR_FLOF_PUMdrw_SUM_OH", count("BUR_Report_rev2"."BUR_FLOF_PUMpre1_SUM_OH") AS "BUR_FLOF_PUMpre1_SUM_OH", count("BUR_Report_rev2"."BUR_FLOF_PUMpre2_SUM_OH") AS "BUR_FLOF_PUMpre2_SUM_OH", count("BUR_Report_rev2"."BUR_FLOF_PUMsluw1_SUM_CAL_EA") AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_FLOF_PUMsluw2_SUM_CAL_EA") AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_FLOF_STIRsluw_SUM_CAL_EA") AS "BUR_FLOF_STIRsluw_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_FLOFi_SAMPww_SUM_OH") AS "BUR_FLOFi_SAMPww_SUM_OH", count("BUR_Report_rev2"."BUR_GLOB_BOIL_SUM_OH") AS "BUR_GLOB_BOIL_SUM_OH", count("BUR_Report_rev2"."BUR_HPS_PUM1_SUM_CAL_EA") AS "BUR_HPS_PUM1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_HPS_PUM2_SUM_CAL_EA") AS "BUR_HPS_PUM2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_HPS_PUM3_SUM_CAL_EA") AS "BUR_HPS_PUM3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_HPS_PUM4_SUM_CAL_EA") AS "BUR_HPS_PUM4_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_IPS_PUM1_SUM_EA") AS "BUR_IPS_PUM1_SUM_EA", count("BUR_Report_rev2"."BUR_IPS_PUM2_SUM_EA") AS "BUR_IPS_PUM2_SUM_EA", count("BUR_Report_rev2"."BUR_IPS_PUM3_SUM_EA") AS "BUR_IPS_PUM3_SUM_EA", count("BUR_Report_rev2"."BUR_IPS_PUM4_SUM_EA") AS "BUR_IPS_PUM4_SUM_EA", count("BUR_Report_rev2"."BUR_IPS_PUM5_SUM_EA") AS "BUR_IPS_PUM5_SUM_EA", count("BUR_Report_rev2"."BUR_IPS_PUM6_SUM_EA") AS "BUR_IPS_PUM6_SUM_EA", count("BUR_Report_rev2"."BUR_PCL_PUMps1_SUM_CAL_EA") AS "BUR_PCL_PUMps1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_PCL_PUMps2_SUM_CAL_EA") AS "BUR_PCL_PUMps2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_PCL_SCRA1_SUM_CAL_EA") AS "BUR_PCL_SCRA1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_PCL_SCRA2_SUM_CAL_EA") AS "BUR_PCL_SCRA2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_PSTHo_PUMtpsl_SUM_CAL_EA") AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_BLO2_SUM_CAL_EA") AS "BUR_SAND_BLO2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_BLO3_SUM_CAL_EA") AS "BUR_SAND_BLO3_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_BLOair1_SUM_CAL_EA") AS "BUR_SAND_BLOair1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_SCRA1_SUM_CAL_EA") AS "BUR_SAND_SCRA1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_SCRA2_SUM_CAL_EA") AS "BUR_SAND_SCRA2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SAND_STIR_SUM_CAL_EA") AS "BUR_SAND_STIR_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SBA_JCL1_SUM_CAL_EA") AS "BUR_SBA_JCL1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SBA_JCL2_SUM_CAL_EA") AS "BUR_SBA_JCL2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SBA_PUM1_SUM_CAL_EA") AS "BUR_SBA_PUM1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SBA_PUM2_SUM_CAL_EA") AS "BUR_SBA_PUM2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCL_PUMsas1_SUM_CAL_EA") AS "BUR_SCL_PUMsas1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCL_PUMsas2_SUM_CAL_EA") AS "BUR_SCL_PUMsas2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCL1_SCRA_SUM_CAL_EA") AS "BUR_SCL1_SCRA_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCL2_SCRA_SUM_CAL_EA") AS "BUR_SCL2_SCRA_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCL3_SCRA_SUM_CAL_EA") AS "BUR_SCL3_SCRA_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCR_GRI1_SUM_CAL_EA") AS "BUR_SCR_GRI1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SCR_GRI2_SUM_CAL_EA") AS "BUR_SCR_GRI2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEW_COMP_SUM_OH") AS "BUR_SDEW_COMP_SUM_OH", count("BUR_Report_rev2"."BUR_SDEW_DOSflo1_SUM_CAL_EA") AS "BUR_SDEW_DOSflo1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEW_DOSflo2_SUM_CAL_EA") AS "BUR_SDEW_DOSflo2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEW_DOSpre1_SUM_CAL_EA") AS "BUR_SDEW_DOSpre1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEW_DOSpre2_SUM_CAL_EA") AS "BUR_SDEW_DOSpre2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEW_PUMhp_SUM_CAL_EA") AS "BUR_SDEW_PUMhp_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA") AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA") AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTH_CEN1_SUM_CAL_EA") AS "BUR_SSTH_CEN1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTH_CEN2_SUM_CAL_EA") AS "BUR_SSTH_CEN2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTHi1_PUMsas_SUM_CAL_EA") AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTHi2_PUMsas_SUM_CAL_EA") AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTHo1_PUMtssl_SUM_CAL_EA") AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_SSTHo2_PUMtssl_SUM_CAL_EA") AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_TWT_PUM1_SUM_CAL_EA") AS "BUR_TWT_PUM1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_TWT_PUM2_SUM_CAL_EA") AS "BUR_TWT_PUM2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_WWTP_PUMpw1_SUM_CAL_EA") AS "BUR_WWTP_PUMpw1_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_WWTP_PUMpw2_SUM_CAL_EA") AS "BUR_WWTP_PUMpw2_SUM_CAL_EA", count("BUR_Report_rev2"."BUR_WWTPo_SAMPww_SUM_OH") AS "BUR_WWTPo_SAMPww_SUM_OH" FROM "BUR_Report_rev2" UNION ALL SELECT 'number of missing values'::text, (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2".datatime)) AS datatime, (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_ANDI_PUMhsl1_SUM_CAL_EA")) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_ANDI_PUMhsl2_SUM_CAL_EA")) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_ANDIi1_STIR_SUM_EA")) AS "BUR_ANDIi1_STIR_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_ANDIi2_STIR_SUM_EA")) AS "BUR_ANDIi2_STIR_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO1_SUM_CAL_EA")) AS "BUR_BIO_BLO1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO12_SUM_EA")) AS "BUR_BIO_BLO12_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO2_SUM_CAL_EA")) AS "BUR_BIO_BLO2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO3_SUM_CAL_EA")) AS "BUR_BIO_BLO3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO34_SUM_EA")) AS "BUR_BIO_BLO34_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO4_SUM_CAL_EA")) AS "BUR_BIO_BLO4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_CAL_EA")) AS "BUR_BIO_BLO5_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_EA")) AS "BUR_BIO_BLO5_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_CAL_EA")) AS "BUR_BIO_BLO6_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_EA")) AS "BUR_BIO_BLO6_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_PUMrcir1_SUM_CAL_EA")) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIO_PUMrcir2_SUM_CAL_EA")) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer1_STIR1_SUM_CAL_EA")) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer1_STIR2_SUM_CAL_EA")) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer2_STIR1_SUM_CAL_EA")) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer2_STIR2_SUM_CAL_EA")) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer3_STIR1_SUM_CAL_EA")) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOaer3_STIR2_SUM_CAL_EA")) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox1_STIR3_SUM_CAL_EA")) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox1_STIR4_SUM_CAL_EA")) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox2_STIR3_SUM_CAL_EA")) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox2_STIR4_SUM_CAL_EA")) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox3_STIR3_SUM_CAL_EA")) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOanox3_STIR4_SUM_CAL_EA")) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOP2_STIR1_SUM_CAL_EA")) AS "BUR_BIOP2_STIR1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_BIOP2_STIR2_SUM_CAL_EA")) AS "BUR_BIOP2_STIR2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_DSTH_PFE_SUM_OH")) AS "BUR_DSTH_PFE_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_DSTHo_PUM1_SUM_OH")) AS "BUR_DSTHo_PUM1_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_DSTHo_PUM2_SUM_OH")) AS "BUR_DSTHo_PUM2_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_ELPS_ELPS_SUM_EA")) AS "BUR_ELPS_ELPS_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLA_TOR_SUM_OH")) AS "BUR_FLA_TOR_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_BLO4_SUM_CAL_EA")) AS "BUR_FLOF_BLO4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_BLO5_SUM_CAL_EA")) AS "BUR_FLOF_BLO5_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_BLObw1_SUM_EA")) AS "BUR_FLOF_BLObw1_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_BLObw2_SUM_EA")) AS "BUR_FLOF_BLObw2_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_BLObw3_SUM_EA")) AS "BUR_FLOF_BLObw3_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMbw1_SUM_EA")) AS "BUR_FLOF_PUMbw1_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMbw2_SUM_EA")) AS "BUR_FLOF_PUMbw2_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMbw3_SUM_EA")) AS "BUR_FLOF_PUMbw3_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMdrw_SUM_OH")) AS "BUR_FLOF_PUMdrw_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMpre1_SUM_OH")) AS "BUR_FLOF_PUMpre1_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMpre2_SUM_OH")) AS "BUR_FLOF_PUMpre2_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMsluw1_SUM_CAL_EA")) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_PUMsluw2_SUM_CAL_EA")) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOF_STIRsluw_SUM_CAL_EA")) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_FLOFi_SAMPww_SUM_OH")) AS "BUR_FLOFi_SAMPww_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_GLOB_BOIL_SUM_OH")) AS "BUR_GLOB_BOIL_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_HPS_PUM1_SUM_CAL_EA")) AS "BUR_HPS_PUM1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_HPS_PUM2_SUM_CAL_EA")) AS "BUR_HPS_PUM2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_HPS_PUM3_SUM_CAL_EA")) AS "BUR_HPS_PUM3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_HPS_PUM4_SUM_CAL_EA")) AS "BUR_HPS_PUM4_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM1_SUM_EA")) AS "BUR_IPS_PUM1_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM2_SUM_EA")) AS "BUR_IPS_PUM2_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM3_SUM_EA")) AS "BUR_IPS_PUM3_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM4_SUM_EA")) AS "BUR_IPS_PUM4_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM5_SUM_EA")) AS "BUR_IPS_PUM5_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_IPS_PUM6_SUM_EA")) AS "BUR_IPS_PUM6_SUM_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_PCL_PUMps1_SUM_CAL_EA")) AS "BUR_PCL_PUMps1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_PCL_PUMps2_SUM_CAL_EA")) AS "BUR_PCL_PUMps2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_PCL_SCRA1_SUM_CAL_EA")) AS "BUR_PCL_SCRA1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_PCL_SCRA2_SUM_CAL_EA")) AS "BUR_PCL_SCRA2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_PSTHo_PUMtpsl_SUM_CAL_EA")) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_BLO2_SUM_CAL_EA")) AS "BUR_SAND_BLO2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_BLO3_SUM_CAL_EA")) AS "BUR_SAND_BLO3_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_BLOair1_SUM_CAL_EA")) AS "BUR_SAND_BLOair1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_SCRA1_SUM_CAL_EA")) AS "BUR_SAND_SCRA1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_SCRA2_SUM_CAL_EA")) AS "BUR_SAND_SCRA2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SAND_STIR_SUM_CAL_EA")) AS "BUR_SAND_STIR_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SBA_JCL1_SUM_CAL_EA")) AS "BUR_SBA_JCL1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SBA_JCL2_SUM_CAL_EA")) AS "BUR_SBA_JCL2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SBA_PUM1_SUM_CAL_EA")) AS "BUR_SBA_PUM1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SBA_PUM2_SUM_CAL_EA")) AS "BUR_SBA_PUM2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCL_PUMsas1_SUM_CAL_EA")) AS "BUR_SCL_PUMsas1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCL_PUMsas2_SUM_CAL_EA")) AS "BUR_SCL_PUMsas2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCL1_SCRA_SUM_CAL_EA")) AS "BUR_SCL1_SCRA_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCL2_SCRA_SUM_CAL_EA")) AS "BUR_SCL2_SCRA_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCL3_SCRA_SUM_CAL_EA")) AS "BUR_SCL3_SCRA_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCR_GRI1_SUM_CAL_EA")) AS "BUR_SCR_GRI1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SCR_GRI2_SUM_CAL_EA")) AS "BUR_SCR_GRI2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_COMP_SUM_OH")) AS "BUR_SDEW_COMP_SUM_OH", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_DOSflo1_SUM_CAL_EA")) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_DOSflo2_SUM_CAL_EA")) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_DOSpre1_SUM_CAL_EA")) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_DOSpre2_SUM_CAL_EA")) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEW_PUMhp_SUM_CAL_EA")) AS "BUR_SDEW_PUMhp_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA")) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA")) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTH_CEN1_SUM_CAL_EA")) AS "BUR_SSTH_CEN1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTH_CEN2_SUM_CAL_EA")) AS "BUR_SSTH_CEN2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTHi1_PUMsas_SUM_CAL_EA")) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTHi2_PUMsas_SUM_CAL_EA")) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTHo1_PUMtssl_SUM_CAL_EA")) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_SSTHo2_PUMtssl_SUM_CAL_EA")) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_TWT_PUM1_SUM_CAL_EA")) AS "BUR_TWT_PUM1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_TWT_PUM2_SUM_CAL_EA")) AS "BUR_TWT_PUM2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_WWTP_PUMpw1_SUM_CAL_EA")) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_WWTP_PUMpw2_SUM_CAL_EA")) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA", (count("BUR_Report_rev2".datatime) - count("BUR_Report_rev2"."BUR_WWTPo_SAMPww_SUM_OH")) AS "BUR_WWTPo_SAMPww_SUM_OH" FROM "BUR_Report_rev2") UNION ALL SELECT '% of missing values'::text, (100 - ((100 * count("BUR_Report_rev2".datatime)) / count("BUR_Report_rev2".datatime))) AS datatime, (100 - ((100 * count("BUR_Report_rev2"."BUR_ANDI_PUMhsl1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_ANDI_PUMhsl2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_ANDIi1_STIR_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_ANDIi1_STIR_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_ANDIi2_STIR_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_ANDIi2_STIR_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO12_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO12_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO34_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO34_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO5_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO5_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO5_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO6_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_BLO6_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_BLO6_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_PUMrcir1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIO_PUMrcir2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer1_STIR1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer1_STIR2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer2_STIR1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer2_STIR2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer3_STIR1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOaer3_STIR2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox1_STIR3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox1_STIR4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox2_STIR3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox2_STIR4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox3_STIR3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOanox3_STIR4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOP2_STIR1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOP2_STIR1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_BIOP2_STIR2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_BIOP2_STIR2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_DSTH_PFE_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_DSTH_PFE_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_DSTHo_PUM1_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_DSTHo_PUM1_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_DSTHo_PUM2_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_DSTHo_PUM2_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_ELPS_ELPS_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_ELPS_ELPS_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLA_TOR_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLA_TOR_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_BLO4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_BLO4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_BLO5_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_BLO5_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_BLObw1_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_BLObw1_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_BLObw2_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_BLObw2_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_BLObw3_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_BLObw3_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMbw1_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMbw1_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMbw2_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMbw2_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMbw3_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMbw3_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMdrw_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMdrw_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMpre1_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMpre1_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMpre2_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMpre2_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMsluw1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_PUMsluw2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOF_STIRsluw_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_FLOFi_SAMPww_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_FLOFi_SAMPww_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_GLOB_BOIL_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_GLOB_BOIL_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_HPS_PUM1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_HPS_PUM1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_HPS_PUM2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_HPS_PUM2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_HPS_PUM3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_HPS_PUM3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_HPS_PUM4_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_HPS_PUM4_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM1_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM1_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM2_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM2_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM3_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM3_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM4_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM4_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM5_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM5_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_IPS_PUM6_SUM_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_IPS_PUM6_SUM_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_PCL_PUMps1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_PCL_PUMps1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_PCL_PUMps2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_PCL_PUMps2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_PCL_SCRA1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_PCL_SCRA1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_PCL_SCRA2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_PCL_SCRA2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_PSTHo_PUMtpsl_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_BLO2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_BLO2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_BLO3_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_BLO3_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_BLOair1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_BLOair1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_SCRA1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_SCRA1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_SCRA2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_SCRA2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SAND_STIR_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SAND_STIR_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SBA_JCL1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SBA_JCL1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SBA_JCL2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SBA_JCL2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SBA_PUM1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SBA_PUM1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SBA_PUM2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SBA_PUM2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCL_PUMsas1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCL_PUMsas1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCL_PUMsas2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCL_PUMsas2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCL1_SCRA_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCL1_SCRA_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCL2_SCRA_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCL2_SCRA_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCL3_SCRA_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCL3_SCRA_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCR_GRI1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCR_GRI1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SCR_GRI2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SCR_GRI2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_COMP_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_COMP_SUM_OH", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_DOSflo1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_DOSflo2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_DOSpre1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_DOSpre2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEW_PUMhp_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEW_PUMhp_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTH_CEN1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTH_CEN1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTH_CEN2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTH_CEN2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTHi1_PUMsas_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTHi2_PUMsas_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTHo1_PUMtssl_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_SSTHo2_PUMtssl_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_TWT_PUM1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_TWT_PUM1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_TWT_PUM2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_TWT_PUM2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_WWTP_PUMpw1_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_WWTP_PUMpw2_SUM_CAL_EA")) / count("BUR_Report_rev2".datatime))) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA", (100 - ((100 * count("BUR_Report_rev2"."BUR_WWTPo_SAMPww_SUM_OH")) / count("BUR_Report_rev2".datatime))) AS "BUR_WWTPo_SAMPww_SUM_OH" FROM "BUR_Report_rev2";


ALTER TABLE inners_data."BUR_Check_import_process" OWNER TO inners_data;

--
-- Name: BUR_DC_Report_rev2; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_DC_Report_rev2" AS
    SELECT b0.datatime, ah1."BUR_ANDI_PUMhsl1_SUM_CAL_EA", ah2."BUR_ANDI_PUMhsl2_SUM_CAL_EA", b4."BUR_ANDIi1_STIR_SUM_EA", b5."BUR_ANDIi2_STIR_SUM_EA", h14."BUR_BIO_BLO1_SUM_CAL_EA", b00."BUR_BIO_BLO12_SUM_EA", h15."BUR_BIO_BLO2_SUM_CAL_EA", h16."BUR_BIO_BLO3_SUM_CAL_EA", b3."BUR_BIO_BLO34_SUM_EA", h170."BUR_BIO_BLO4_SUM_CAL_EA", h17."BUR_BIO_BLO5_SUM_CAL_EA", b1."BUR_BIO_BLO5_SUM_EA", h18."BUR_BIO_BLO6_SUM_CAL_EA", b2."BUR_BIO_BLO6_SUM_EA", h210."BUR_BIO_PUMrcir1_SUM_CAL_EA", h21."BUR_BIO_PUMrcir2_SUM_CAL_EA", h1."BUR_BIOaer1_STIR1_SUM_CAL_EA", h100."BUR_BIOaer1_STIR2_SUM_CAL_EA", h2."BUR_BIOaer2_STIR1_SUM_CAL_EA", h3."BUR_BIOaer2_STIR2_SUM_CAL_EA", h4."BUR_BIOaer3_STIR1_SUM_CAL_EA", h6."BUR_BIOaer3_STIR2_SUM_CAL_EA", h7."BUR_BIOanox1_STIR3_SUM_CAL_EA", h8."BUR_BIOanox1_STIR4_SUM_CAL_EA", h9."BUR_BIOanox2_STIR3_SUM_CAL_EA", h10."BUR_BIOanox2_STIR4_SUM_CAL_EA", h11."BUR_BIOanox3_STIR3_SUM_CAL_EA", h12."BUR_BIOanox3_STIR4_SUM_CAL_EA", h19."BUR_BIOP2_STIR1_SUM_CAL_EA", h20."BUR_BIOP2_STIR2_SUM_CAL_EA", oth3."BUR_DSTH_PFE_SUM_OH", oth1."BUR_DSTHo_PUM1_SUM_OH", oth2."BUR_DSTHo_PUM2_SUM_OH", b6."BUR_ELPS_ELPS_SUM_EA", oth4."BUR_FLA_TOR_SUM_OH", flof1."BUR_FLOF_BLO4_SUM_CAL_EA", flof2."BUR_FLOF_BLO5_SUM_CAL_EA", b7."BUR_FLOF_BLObw1_SUM_EA", b8."BUR_FLOF_BLObw2_SUM_EA", b9."BUR_FLOF_BLObw3_SUM_EA", b10."BUR_FLOF_PUMbw1_SUM_EA", b11."BUR_FLOF_PUMbw2_SUM_EA", b12."BUR_FLOF_PUMbw3_SUM_EA", flof4."BUR_FLOF_PUMdrw_SUM_OH", flof5."BUR_FLOF_PUMpre1_SUM_OH", flof6."BUR_FLOF_PUMpre2_SUM_OH", flof7."BUR_FLOF_PUMsluw1_SUM_CAL_EA", flof8."BUR_FLOF_PUMsluw2_SUM_CAL_EA", flof9."BUR_FLOF_STIRsluw_SUM_CAL_EA", flof3."BUR_FLOFi_SAMPww_SUM_OH", oth5."BUR_GLOB_BOIL_SUM_OH", oth6."BUR_HPS_PUM1_SUM_CAL_EA", oth7."BUR_HPS_PUM2_SUM_CAL_EA", oth8."BUR_HPS_PUM3_SUM_CAL_EA", oth9."BUR_HPS_PUM4_SUM_CAL_EA", b13."BUR_IPS_PUM1_SUM_EA", b15."BUR_IPS_PUM2_SUM_EA", b16."BUR_IPS_PUM3_SUM_EA", b17."BUR_IPS_PUM4_SUM_EA", b18."BUR_IPS_PUM5_SUM_EA", b19."BUR_IPS_PUM6_SUM_EA", oth10."BUR_PCL_PUMps1_SUM_CAL_EA", oth11."BUR_PCL_PUMps2_SUM_CAL_EA", oth12."BUR_PCL_SCRA1_SUM_CAL_EA", oth13."BUR_PCL_SCRA2_SUM_CAL_EA", oth14."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", oth15."BUR_SAND_BLO2_SUM_CAL_EA", oth16."BUR_SAND_BLO3_SUM_CAL_EA", oth17."BUR_SAND_BLOair1_SUM_CAL_EA", oth18."BUR_SAND_SCRA1_SUM_CAL_EA", oth19."BUR_SAND_SCRA2_SUM_CAL_EA", oth20."BUR_SAND_STIR_SUM_CAL_EA", oth21."BUR_SBA_JCL1_SUM_CAL_EA", oth22."BUR_SBA_JCL2_SUM_CAL_EA", oth23."BUR_SBA_PUM1_SUM_CAL_EA", oth24."BUR_SBA_PUM2_SUM_CAL_EA", oth28."BUR_SCL_PUMsas1_SUM_CAL_EA", oth29."BUR_SCL_PUMsas2_SUM_CAL_EA", oth25."BUR_SCL1_SCRA_SUM_CAL_EA", oth26."BUR_SCL2_SCRA_SUM_CAL_EA", oth27."BUR_SCL3_SCRA_SUM_CAL_EA", oth30."BUR_SCR_GRI1_SUM_CAL_EA", oth31."BUR_SCR_GRI2_SUM_CAL_EA", oth58."BUR_SDEW_COMP_SUM_OH", oth33."BUR_SDEW_DOSflo1_SUM_CAL_EA", oth34."BUR_SDEW_DOSflo2_SUM_CAL_EA", oth35."BUR_SDEW_DOSpre1_SUM_CAL_EA", oth36."BUR_SDEW_DOSpre2_SUM_CAL_EA", oth39."BUR_SDEW_PUMhp_SUM_CAL_EA", oth37."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", oth38."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", oth40."BUR_SSTH_CEN1_SUM_CAL_EA", oth41."BUR_SSTH_CEN2_SUM_CAL_EA", oth42."BUR_SSTHi1_PUMsas_SUM_CAL_EA", oth43."BUR_SSTHi2_PUMsas_SUM_CAL_EA", oth44."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", oth71."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", oth46."BUR_TWT_PUM1_SUM_CAL_EA", oth47."BUR_TWT_PUM2_SUM_CAL_EA", oth49."BUR_WWTP_PUMpw1_SUM_CAL_EA", oth50."BUR_WWTP_PUMpw2_SUM_CAL_EA", oth48."BUR_WWTPo_SAMPww_SUM_OH" FROM (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data ORDER BY agg_plant_data.date) b0 FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO12_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO12_SUM_EA'::text) AND (agg_plant_data.value <= (1680)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b00 ON ((b0.datatime = b00.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO5_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_EA'::text) AND (agg_plant_data.value <= (840)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b1 ON ((b0.datatime = b1.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO6_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_EA'::text) AND (agg_plant_data.value <= (840)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b2 ON ((b0.datatime = b2.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO34_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO34_SUM_EA'::text) AND (agg_plant_data.value <= (1680)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b3 ON ((b0.datatime = b3.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h1 ON ((b0.datatime = h1.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h100 ON ((b0.datatime = h100.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h2 ON ((b0.datatime = h2.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h3 ON ((b0.datatime = h3.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h4 ON ((b0.datatime = h4.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h6 ON ((b0.datatime = h6.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h7 ON ((b0.datatime = h7.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h8 ON ((b0.datatime = h8.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h9 ON ((b0.datatime = h9.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h10 ON ((b0.datatime = h10.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h11 ON ((b0.datatime = h11.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h12 ON ((b0.datatime = h12.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h14 ON ((b0.datatime = h14.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h15 ON ((b0.datatime = h15.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h16 ON ((b0.datatime = h16.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h170 ON ((b0.datatime = h170.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h17 ON ((b0.datatime = h17.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO6_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h18 ON ((b0.datatime = h18.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.6)::double precision) AS "BUR_BIOP2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h19 ON ((b0.datatime = h19.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.4)::double precision) AS "BUR_BIOP2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h20 ON ((b0.datatime = h20.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h210 ON ((b0.datatime = h210.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h21 ON ((b0.datatime = h21.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi1_STIR_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_ANDIi1_STIR_SUM_EA'::text) AND (agg_plant_data.value <= (420)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b4 ON ((b0.datatime = b4.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi2_STIR_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_ANDIi2_STIR_SUM_EA'::text) AND (agg_plant_data.value <= (420)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b5 ON ((b0.datatime = b5.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah1 ON ((b0.datatime = ah1.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah2 ON ((b0.datatime = ah2.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ELPS_ELPS_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_ELPS_ELPS_SUM_EA'::text) AND (agg_plant_data.value <= (12480)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b6 ON ((b0.datatime = b6.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw1_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw1_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b7 ON ((b0.datatime = b7.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw2_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw2_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b8 ON ((b0.datatime = b8.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw3_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw3_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b9 ON ((b0.datatime = b9.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw1_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw1_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b10 ON ((b0.datatime = b10.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw2_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw2_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b11 ON ((b0.datatime = b11.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw3_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw3_SUM_EA'::text) AND (agg_plant_data.value <= (120)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b12 ON ((b0.datatime = b12.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_BLO4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof1 ON ((b0.datatime = flof1.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_BLO5_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof2 ON ((b0.datatime = flof2.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOFi_SAMPww_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOFi_SAMPww_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof3 ON ((b0.datatime = flof3.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMdrw_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMdrw_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof4 ON ((b0.datatime = flof4.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre1_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof5 ON ((b0.datatime = flof5.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre2_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof6 ON ((b0.datatime = flof6.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof7 ON ((b0.datatime = flof7.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof8 ON ((b0.datatime = flof8.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.8)::double precision) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLOF_STIRsluw_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof9 ON ((b0.datatime = flof9.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM1_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM1_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b13 ON ((b0.datatime = b13.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM2_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM2_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b15 ON ((b0.datatime = b15.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM3_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM3_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b16 ON ((b0.datatime = b16.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM4_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM4_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b17 ON ((b0.datatime = b17.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM5_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM5_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b18 ON ((b0.datatime = b18.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM6_SUM_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_IPS_PUM6_SUM_EA'::text) AND (agg_plant_data.value <= (3000)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b19 ON ((b0.datatime = b19.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM1_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth1 ON ((b0.datatime = oth1.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM2_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth2 ON ((b0.datatime = oth2.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_DSTH_PFE_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_DSTH_PFE_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth3 ON ((b0.datatime = oth3.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLA_TOR_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_FLA_TOR_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth4 ON ((b0.datatime = oth4.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_GLOB_BOIL_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_GLOB_BOIL_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth5 ON ((b0.datatime = oth5.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_HPS_PUM1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth6 ON ((b0.datatime = oth6.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_HPS_PUM2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth7 ON ((b0.datatime = oth7.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_HPS_PUM3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth8 ON ((b0.datatime = oth8.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM4_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_HPS_PUM4_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth9 ON ((b0.datatime = oth9.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_PCL_PUMps1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth10 ON ((b0.datatime = oth10.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_PCL_PUMps2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth11 ON ((b0.datatime = oth11.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_PCL_SCRA1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth12 ON ((b0.datatime = oth12.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_PCL_SCRA2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth13 ON ((b0.datatime = oth13.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_PSTHo_PUMtpsl_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth14 ON ((b0.datatime = oth14.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_BLO2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth15 ON ((b0.datatime = oth15.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_BLO3_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth16 ON ((b0.datatime = oth16.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLOair1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_BLOair1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth17 ON ((b0.datatime = oth17.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_SCRA1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth18 ON ((b0.datatime = oth18.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_SCRA2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth19 ON ((b0.datatime = oth19.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SAND_STIR_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SAND_STIR_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth20 ON ((b0.datatime = oth20.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8.5)::double precision) AS "BUR_SBA_JCL1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SBA_JCL1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth21 ON ((b0.datatime = oth21.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (11.58)::double precision) AS "BUR_SBA_JCL2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SBA_JCL2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth22 ON ((b0.datatime = oth22.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SBA_PUM1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth23 ON ((b0.datatime = oth23.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SBA_PUM2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth24 ON ((b0.datatime = oth24.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL1_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCL1_SCRA_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth25 ON ((b0.datatime = oth25.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL2_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCL2_SCRA_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth26 ON ((b0.datatime = oth26.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL3_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCL3_SCRA_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth27 ON ((b0.datatime = oth27.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth28 ON ((b0.datatime = oth28.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth29 ON ((b0.datatime = oth29.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCR_GRI1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth30 ON ((b0.datatime = oth30.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SCR_GRI2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth31 ON ((b0.datatime = oth31.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth33 ON ((b0.datatime = oth33.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth34 ON ((b0.datatime = oth34.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth35 ON ((b0.datatime = oth35.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth36 ON ((b0.datatime = oth36.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth37 ON ((b0.datatime = oth37.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth38 ON ((b0.datatime = oth38.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (46)::double precision) AS "BUR_SDEW_PUMhp_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_PUMhp_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth39 ON ((b0.datatime = oth39.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTH_CEN1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth40 ON ((b0.datatime = oth40.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTH_CEN2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth41 ON ((b0.datatime = oth41.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTHi1_PUMsas_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth42 ON ((b0.datatime = oth42.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTHi2_PUMsas_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth43 ON ((b0.datatime = oth43.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTHo1_PUMtssl_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth44 ON ((b0.datatime = oth44.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_TWT_PUM1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth46 ON ((b0.datatime = oth46.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_TWT_PUM2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth47 ON ((b0.datatime = oth47.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_WWTPo_SAMPww_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_WWTPo_SAMPww_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth48 ON ((b0.datatime = oth48.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw1_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth49 ON ((b0.datatime = oth49.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw2_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth50 ON ((b0.datatime = oth50.date))) FULL JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_SDEW_COMP_SUM_OH" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SDEW_COMP_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth58 ON ((b0.datatime = oth58.date))) FULL JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE (((agg_plant_data.sensor_id = 'BUR_SSTHo2_PUMtssl_SUM_OH'::text) AND (agg_plant_data.value <= (24)::double precision)) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth71 ON ((b0.datatime = oth71.date)));


ALTER TABLE inners_data."BUR_DC_Report_rev2" OWNER TO inners_data;

--
-- Name: BUR_REPORT_PROD; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_REPORT_PROD" AS
    SELECT "time".datatime, prod1."BUR_COGEto1_SENea_SUM_PROD", prod2."BUR_COGEto2_SENea_SUM_PROD", prod3."BUR_EMPS_SENea_SUM_PROD" FROM ((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data) "time" LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_COGEto1_SENea_SUM_PROD" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_COGEto1_SENea_SUM_PROD'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) prod1 ON (("time".datatime = prod1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_COGEto2_SENea_SUM_PROD" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_COGEto2_SENea_SUM_PROD'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) prod2 ON (("time".datatime = prod2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_EMPS_SENea_SUM_PROD" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_EMPS_SENea_SUM_PROD'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) prod3 ON (("time".datatime = prod3.date)));


ALTER TABLE inners_data."BUR_REPORT_PROD" OWNER TO inners_data;

--
-- Name: kpi_parts; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE kpi_parts (
    id integer NOT NULL,
    date timestamp without time zone,
    sensor_id text,
    value double precision,
    interval_id integer,
    interval_type text,
    plant text NOT NULL
);


ALTER TABLE inners_data.kpi_parts OWNER TO inners_data;

--
-- Name: TABLE kpi_parts; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE kpi_parts IS 'Components for calculating KPIs.';


--
-- Name: BUR_Report_rev3; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_Report_rev3" AS
    SELECT dariotable.datatime, dariotable."BUR_ANDI_PUMhsl1_SUM_CAL_EA", dariotable."BUR_ANDI_PUMhsl2_SUM_CAL_EA", dariotable."BUR_ANDIi1_STIR_SUM_EA", dariotable."BUR_ANDIi2_STIR_SUM_EA", dariotable."BUR_BIO_BLO1_SUM_CAL_EA", dariotable."BUR_BIO_BLO12_SUM_EA", dariotable."BUR_BIO_BLO2_SUM_CAL_EA", dariotable."BUR_BIO_BLO3_SUM_CAL_EA", dariotable."BUR_BIO_BLO34_SUM_EA", dariotable."BUR_BIO_BLO4_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_EA", dariotable."BUR_BIO_BLO6_SUM_CAL_EA", dariotable."BUR_BIO_BLO6_SUM_EA", dariotable."BUR_BIO_PUMrcir1_SUM_CAL_EA", dariotable."BUR_BIO_PUMrcir2_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR2_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR4_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR2_SUM_CAL_EA", dariotable."BUR_DSTH_PFE_SUM_OH", dariotable."BUR_DSTHo_PUM1_SUM_OH", dariotable."BUR_DSTHo_PUM2_SUM_OH", dariotable."BUR_ELPS_ELPS_SUM_EA", dariotable."BUR_FLA_TOR_SUM_OH", dariotable."BUR_FLOF_BLO4_SUM_CAL_EA", dariotable."BUR_FLOF_BLO5_SUM_CAL_EA", dariotable."BUR_FLOF_BLObw1_SUM_EA", dariotable."BUR_FLOF_BLObw2_SUM_EA", dariotable."BUR_FLOF_BLObw3_SUM_EA", dariotable."BUR_FLOF_PUMbw1_SUM_EA", dariotable."BUR_FLOF_PUMbw2_SUM_EA", dariotable."BUR_FLOF_PUMbw3_SUM_EA", dariotable."BUR_FLOF_PUMdrw_SUM_OH", dariotable."BUR_FLOF_PUMpre1_SUM_OH", dariotable."BUR_FLOF_PUMpre2_SUM_OH", dariotable."BUR_FLOF_PUMsluw1_SUM_CAL_EA", dariotable."BUR_FLOF_PUMsluw2_SUM_CAL_EA", dariotable."BUR_FLOF_STIRsluw_SUM_CAL_EA", dariotable."BUR_FLOFi_SAMPww_SUM_OH", dariotable."BUR_GLOB_BOIL_SUM_OH", dariotable."BUR_HPS_PUM1_SUM_CAL_EA", dariotable."BUR_HPS_PUM2_SUM_CAL_EA", dariotable."BUR_HPS_PUM3_SUM_CAL_EA", dariotable."BUR_HPS_PUM4_SUM_CAL_EA", dariotable."BUR_IPS_PUM1_SUM_EA", dariotable."BUR_IPS_PUM2_SUM_EA", dariotable."BUR_IPS_PUM3_SUM_EA", dariotable."BUR_IPS_PUM4_SUM_EA", dariotable."BUR_IPS_PUM5_SUM_EA", dariotable."BUR_IPS_PUM6_SUM_EA", dariotable."BUR_PCL_PUMps1_SUM_CAL_EA", dariotable."BUR_PCL_PUMps2_SUM_CAL_EA", dariotable."BUR_PCL_SCRA1_SUM_CAL_EA", dariotable."BUR_PCL_SCRA2_SUM_CAL_EA", dariotable."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", dariotable."BUR_SAND_BLO2_SUM_CAL_EA", dariotable."BUR_SAND_BLO3_SUM_CAL_EA", dariotable."BUR_SAND_BLOair1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA2_SUM_CAL_EA", dariotable."BUR_SAND_STIR_SUM_CAL_EA", dariotable."BUR_SBA_JCL1_SUM_CAL_EA", dariotable."BUR_SBA_JCL2_SUM_CAL_EA", dariotable."BUR_SBA_PUM1_SUM_CAL_EA", dariotable."BUR_SBA_PUM2_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas1_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas2_SUM_CAL_EA", dariotable."BUR_SCL1_SCRA_SUM_CAL_EA", dariotable."BUR_SCL2_SCRA_SUM_CAL_EA", dariotable."BUR_SCL3_SCRA_SUM_CAL_EA", dariotable."BUR_SCR_GRI1_SUM_CAL_EA", dariotable."BUR_SCR_GRI2_SUM_CAL_EA", dariotable."BUR_SDEW_COMP_SUM_OH", dariotable."BUR_SDEW_DOSflo1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSflo2_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre2_SUM_CAL_EA", dariotable."BUR_SDEW_PUMhp_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", dariotable."BUR_SSTH_CEN1_SUM_CAL_EA", dariotable."BUR_SSTH_CEN2_SUM_CAL_EA", dariotable."BUR_SSTHi1_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHi2_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", dariotable."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", dariotable."BUR_TWT_PUM1_SUM_CAL_EA", dariotable."BUR_TWT_PUM2_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw1_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw2_SUM_CAL_EA", dariotable."BUR_WWTPo_SAMPww_SUM_OH", dariotable."BUR_COGEto_SUM_PROD" FROM (SELECT "time".datatime, ah1."BUR_ANDI_PUMhsl1_SUM_CAL_EA", ah2."BUR_ANDI_PUMhsl2_SUM_CAL_EA", b4."BUR_ANDIi1_STIR_SUM_EA", b5."BUR_ANDIi2_STIR_SUM_EA", h14."BUR_BIO_BLO1_SUM_CAL_EA", b0."BUR_BIO_BLO12_SUM_EA", h15."BUR_BIO_BLO2_SUM_CAL_EA", h16."BUR_BIO_BLO3_SUM_CAL_EA", b3."BUR_BIO_BLO34_SUM_EA", h170."BUR_BIO_BLO4_SUM_CAL_EA", h17."BUR_BIO_BLO5_SUM_CAL_EA", b1."BUR_BIO_BLO5_SUM_EA", h18."BUR_BIO_BLO6_SUM_CAL_EA", b2."BUR_BIO_BLO6_SUM_EA", h210."BUR_BIO_PUMrcir1_SUM_CAL_EA", h21."BUR_BIO_PUMrcir2_SUM_CAL_EA", h1."BUR_BIOaer1_STIR1_SUM_CAL_EA", h100."BUR_BIOaer1_STIR2_SUM_CAL_EA", h2."BUR_BIOaer2_STIR1_SUM_CAL_EA", h3."BUR_BIOaer2_STIR2_SUM_CAL_EA", h4."BUR_BIOaer3_STIR1_SUM_CAL_EA", h6."BUR_BIOaer3_STIR2_SUM_CAL_EA", h7."BUR_BIOanox1_STIR3_SUM_CAL_EA", h8."BUR_BIOanox1_STIR4_SUM_CAL_EA", h9."BUR_BIOanox2_STIR3_SUM_CAL_EA", h10."BUR_BIOanox2_STIR4_SUM_CAL_EA", h11."BUR_BIOanox3_STIR3_SUM_CAL_EA", h12."BUR_BIOanox3_STIR4_SUM_CAL_EA", h19."BUR_BIOP2_STIR1_SUM_CAL_EA", h20."BUR_BIOP2_STIR2_SUM_CAL_EA", oth3."BUR_DSTH_PFE_SUM_OH", oth1."BUR_DSTHo_PUM1_SUM_OH", oth2."BUR_DSTHo_PUM2_SUM_OH", b6."BUR_ELPS_ELPS_SUM_EA", oth4."BUR_FLA_TOR_SUM_OH", flof1."BUR_FLOF_BLO4_SUM_CAL_EA", flof2."BUR_FLOF_BLO5_SUM_CAL_EA", b7."BUR_FLOF_BLObw1_SUM_EA", b8."BUR_FLOF_BLObw2_SUM_EA", b9."BUR_FLOF_BLObw3_SUM_EA", b10."BUR_FLOF_PUMbw1_SUM_EA", b11."BUR_FLOF_PUMbw2_SUM_EA", b12."BUR_FLOF_PUMbw3_SUM_EA", flof4."BUR_FLOF_PUMdrw_SUM_OH", flof5."BUR_FLOF_PUMpre1_SUM_OH", flof6."BUR_FLOF_PUMpre2_SUM_OH", flof7."BUR_FLOF_PUMsluw1_SUM_CAL_EA", flof8."BUR_FLOF_PUMsluw2_SUM_CAL_EA", flof9."BUR_FLOF_STIRsluw_SUM_CAL_EA", flof3."BUR_FLOFi_SAMPww_SUM_OH", oth5."BUR_GLOB_BOIL_SUM_OH", oth6."BUR_HPS_PUM1_SUM_CAL_EA", oth7."BUR_HPS_PUM2_SUM_CAL_EA", oth8."BUR_HPS_PUM3_SUM_CAL_EA", oth9."BUR_HPS_PUM4_SUM_CAL_EA", b13."BUR_IPS_PUM1_SUM_EA", b15."BUR_IPS_PUM2_SUM_EA", b16."BUR_IPS_PUM3_SUM_EA", b17."BUR_IPS_PUM4_SUM_EA", b18."BUR_IPS_PUM5_SUM_EA", b19."BUR_IPS_PUM6_SUM_EA", oth10."BUR_PCL_PUMps1_SUM_CAL_EA", oth11."BUR_PCL_PUMps2_SUM_CAL_EA", oth12."BUR_PCL_SCRA1_SUM_CAL_EA", oth13."BUR_PCL_SCRA2_SUM_CAL_EA", oth14."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", oth15."BUR_SAND_BLO2_SUM_CAL_EA", oth16."BUR_SAND_BLO3_SUM_CAL_EA", oth17."BUR_SAND_BLOair1_SUM_CAL_EA", oth18."BUR_SAND_SCRA1_SUM_CAL_EA", oth19."BUR_SAND_SCRA2_SUM_CAL_EA", oth20."BUR_SAND_STIR_SUM_CAL_EA", oth21."BUR_SBA_JCL1_SUM_CAL_EA", oth22."BUR_SBA_JCL2_SUM_CAL_EA", oth23."BUR_SBA_PUM1_SUM_CAL_EA", oth24."BUR_SBA_PUM2_SUM_CAL_EA", oth28."BUR_SCL_PUMsas1_SUM_CAL_EA", oth29."BUR_SCL_PUMsas2_SUM_CAL_EA", oth25."BUR_SCL1_SCRA_SUM_CAL_EA", oth26."BUR_SCL2_SCRA_SUM_CAL_EA", oth27."BUR_SCL3_SCRA_SUM_CAL_EA", oth30."BUR_SCR_GRI1_SUM_CAL_EA", oth31."BUR_SCR_GRI2_SUM_CAL_EA", oth58."BUR_SDEW_COMP_SUM_OH", oth33."BUR_SDEW_DOSflo1_SUM_CAL_EA", oth34."BUR_SDEW_DOSflo2_SUM_CAL_EA", oth35."BUR_SDEW_DOSpre1_SUM_CAL_EA", oth36."BUR_SDEW_DOSpre2_SUM_CAL_EA", oth39."BUR_SDEW_PUMhp_SUM_CAL_EA", oth37."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", oth38."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", oth40."BUR_SSTH_CEN1_SUM_CAL_EA", oth41."BUR_SSTH_CEN2_SUM_CAL_EA", oth42."BUR_SSTHi1_PUMsas_SUM_CAL_EA", oth43."BUR_SSTHi2_PUMsas_SUM_CAL_EA", oth44."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", oth71."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", oth46."BUR_TWT_PUM1_SUM_CAL_EA", oth47."BUR_TWT_PUM2_SUM_CAL_EA", oth49."BUR_WWTP_PUMpw1_SUM_CAL_EA", oth50."BUR_WWTP_PUMpw2_SUM_CAL_EA", oth48."BUR_WWTPo_SAMPww_SUM_OH", oth98."BUR_COGEto_SUM_PROD" FROM ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'day'::text) ORDER BY agg_plant_data.date) "time" LEFT JOIN (SELECT DISTINCT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO12_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO12_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b0 ON (("time".datatime = b0.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b1 ON (("time".datatime = b1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b2 ON (("time".datatime = b2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO34_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO34_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b3 ON (("time".datatime = b3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h1 ON (("time".datatime = h1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h100 ON (("time".datatime = h100.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h2 ON (("time".datatime = h2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h3 ON (("time".datatime = h3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h4 ON (("time".datatime = h4.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h6 ON (("time".datatime = h6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h7 ON (("time".datatime = h7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h8 ON (("time".datatime = h8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h9 ON (("time".datatime = h9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h10 ON (("time".datatime = h10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h11 ON (("time".datatime = h11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h12 ON (("time".datatime = h12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h14 ON (("time".datatime = h14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h15 ON (("time".datatime = h15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h16 ON (("time".datatime = h16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h170 ON (("time".datatime = h170.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h17 ON (("time".datatime = h17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO6_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h18 ON (("time".datatime = h18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.6)::double precision) AS "BUR_BIOP2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h19 ON (("time".datatime = h19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.4)::double precision) AS "BUR_BIOP2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h20 ON (("time".datatime = h20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h210 ON (("time".datatime = h210.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h21 ON (("time".datatime = h21.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi1_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi1_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b4 ON (("time".datatime = b4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi2_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi2_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b5 ON (("time".datatime = b5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah1 ON (("time".datatime = ah1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah2 ON (("time".datatime = ah2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ELPS_ELPS_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ELPS_ELPS_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b6 ON (("time".datatime = b6.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b7 ON (("time".datatime = b7.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b8 ON (("time".datatime = b8.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b9 ON (("time".datatime = b9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b10 ON (("time".datatime = b10.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b11 ON (("time".datatime = b11.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b12 ON (("time".datatime = b12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof1 ON (("time".datatime = flof1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof2 ON (("time".datatime = flof2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOFi_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOFi_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof3 ON (("time".datatime = flof3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMdrw_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMdrw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof4 ON (("time".datatime = flof4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre1_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof5 ON (("time".datatime = flof5.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMpre2_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof6 ON (("time".datatime = flof6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof7 ON (("time".datatime = flof7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof8 ON (("time".datatime = flof8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.8)::double precision) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_STIRsluw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof9 ON (("time".datatime = flof9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b13 ON (("time".datatime = b13.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b15 ON (("time".datatime = b15.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b16 ON (("time".datatime = b16.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM4_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM4_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b17 ON (("time".datatime = b17.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b18 ON (("time".datatime = b18.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b19 ON (("time".datatime = b19.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM1_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth1 ON (("time".datatime = oth1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM2_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth2 ON (("time".datatime = oth2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_DSTH_PFE_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTH_PFE_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth3 ON (("time".datatime = oth3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLA_TOR_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLA_TOR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth4 ON (("time".datatime = oth4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_GLOB_BOIL_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_GLOB_BOIL_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth5 ON (("time".datatime = oth5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth6 ON (("time".datatime = oth6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth7 ON (("time".datatime = oth7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth8 ON (("time".datatime = oth8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth9 ON (("time".datatime = oth9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth10 ON (("time".datatime = oth10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth11 ON (("time".datatime = oth11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth12 ON (("time".datatime = oth12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth13 ON (("time".datatime = oth13.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PSTHo_PUMtpsl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth14 ON (("time".datatime = oth14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth15 ON (("time".datatime = oth15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth16 ON (("time".datatime = oth16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLOair1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLOair1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth17 ON (("time".datatime = oth17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth18 ON (("time".datatime = oth18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth19 ON (("time".datatime = oth19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SAND_STIR_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_STIR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth20 ON (("time".datatime = oth20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8.5)::double precision) AS "BUR_SBA_JCL1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth21 ON (("time".datatime = oth21.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (11.58)::double precision) AS "BUR_SBA_JCL2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth22 ON (("time".datatime = oth22.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth23 ON (("time".datatime = oth23.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth24 ON (("time".datatime = oth24.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL1_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL1_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth25 ON (("time".datatime = oth25.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL2_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL2_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth26 ON (("time".datatime = oth26.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL3_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL3_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth27 ON (("time".datatime = oth27.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth28 ON (("time".datatime = oth28.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth29 ON (("time".datatime = oth29.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth30 ON (("time".datatime = oth30.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth31 ON (("time".datatime = oth31.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth33 ON (("time".datatime = oth33.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth34 ON (("time".datatime = oth34.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth35 ON (("time".datatime = oth35.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth36 ON (("time".datatime = oth36.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth37 ON (("time".datatime = oth37.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth38 ON (("time".datatime = oth38.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (46)::double precision) AS "BUR_SDEW_PUMhp_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_PUMhp_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth39 ON (("time".datatime = oth39.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth40 ON (("time".datatime = oth40.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth41 ON (("time".datatime = oth41.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi1_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth42 ON (("time".datatime = oth42.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi2_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth43 ON (("time".datatime = oth43.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo1_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth44 ON (("time".datatime = oth44.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth46 ON (("time".datatime = oth46.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth47 ON (("time".datatime = oth47.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_WWTPo_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTPo_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth48 ON (("time".datatime = oth48.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth49 ON (("time".datatime = oth49.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMpw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth50 ON (("time".datatime = oth50.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_SDEW_COMP_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_COMP_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth58 ON (("time".datatime = oth58.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo2_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth71 ON (("time".datatime = oth71.date))) LEFT JOIN (SELECT kpi_parts.date, kpi_parts.value AS "BUR_COGEto_SUM_PROD" FROM kpi_parts WHERE ((kpi_parts.sensor_id ~~ '%COGEto_SUM_PROD%'::text) AND (kpi_parts.interval_type = 'day'::text)) ORDER BY kpi_parts.date) oth98 ON (("time".datatime = oth98.date)))) dariotable;


ALTER TABLE inners_data."BUR_Report_rev3" OWNER TO inners_data;

--
-- Name: BUR_Report_rev4; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "BUR_Report_rev4" AS
    SELECT dariotable.datatime, dariotable."BUR_ANDI_PUMhsl1_SUM_CAL_EA", dariotable."BUR_ANDI_PUMhsl2_SUM_CAL_EA", dariotable."BUR_ANDIi1_STIR_SUM_EA", dariotable."BUR_ANDIi2_STIR_SUM_EA", dariotable."BUR_BIO_BLO1_SUM_CAL_EA", dariotable."BUR_BIO_BLO12_SUM_EA", dariotable."BUR_BIO_BLO2_SUM_CAL_EA", dariotable."BUR_BIO_BLO3_SUM_CAL_EA", dariotable."BUR_BIO_BLO34_SUM_EA", dariotable."BUR_BIO_BLO4_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_CAL_EA", dariotable."BUR_BIO_BLO5_SUM_EA", dariotable."BUR_BIO_BLO6_SUM_CAL_EA", dariotable."BUR_BIO_BLO6_SUM_EA", dariotable."BUR_BIO_PUMrcir1_SUM_CAL_EA", dariotable."BUR_BIO_PUMrcir2_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer1_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer2_STIR2_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR1_SUM_CAL_EA", dariotable."BUR_BIOaer3_STIR2_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox1_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox2_STIR4_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR3_SUM_CAL_EA", dariotable."BUR_BIOanox3_STIR4_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR1_SUM_CAL_EA", dariotable."BUR_BIOP2_STIR2_SUM_CAL_EA", dariotable."BUR_DSTH_PFE_CAL_EA", dariotable."BUR_DSTHo_PUM1_SUM_OH", dariotable."BUR_DSTHo_PUM2_SUM_OH", dariotable."BUR_ELPS_ELPS_SUM_EA", dariotable."BUR_FLA_TOR_SUM_OH", dariotable."BUR_FLOF_BLO4_SUM_CAL_EA", dariotable."BUR_FLOF_BLO5_SUM_CAL_EA", dariotable."BUR_FLOF_BLObw1_SUM_EA", dariotable."BUR_FLOF_BLObw2_SUM_EA", dariotable."BUR_FLOF_BLObw3_SUM_EA", dariotable."BUR_FLOF_PUMbw1_SUM_EA", dariotable."BUR_FLOF_PUMbw2_SUM_EA", dariotable."BUR_FLOF_PUMbw3_SUM_EA", dariotable."BUR_FLOF_PUMdrw_CAL_EA", dariotable."BUR_FLOF_PUMpre1_CAL_EA", dariotable."BUR_FLOF_PUMpre2_CAL_EA", dariotable."BUR_FLOF_PUMsluw1_SUM_CAL_EA", dariotable."BUR_FLOF_PUMsluw2_SUM_CAL_EA", dariotable."BUR_FLOF_STIRsluw_SUM_CAL_EA", dariotable."BUR_FLOFi_SAMPww_SUM_OH", dariotable."BUR_GLOB_BOIL_SUM_OH", dariotable."BUR_HPS_PUM1_SUM_CAL_EA", dariotable."BUR_HPS_PUM2_SUM_CAL_EA", dariotable."BUR_HPS_PUM3_SUM_CAL_EA", dariotable."BUR_HPS_PUM4_SUM_CAL_EA", dariotable."BUR_IPS_PUM1_SUM_EA", dariotable."BUR_IPS_PUM2_SUM_EA", dariotable."BUR_IPS_PUM3_SUM_EA", dariotable."BUR_IPS_PUM4_SUM_EA", dariotable."BUR_IPS_PUM5_SUM_EA", dariotable."BUR_IPS_PUM6_SUM_EA", dariotable."BUR_PCL_PUMps1_SUM_CAL_EA", dariotable."BUR_PCL_PUMps2_SUM_CAL_EA", dariotable."BUR_PCL_SCRA1_SUM_CAL_EA", dariotable."BUR_PCL_SCRA2_SUM_CAL_EA", dariotable."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", dariotable."BUR_SAND_BLO2_SUM_CAL_EA", dariotable."BUR_SAND_BLO3_SUM_CAL_EA", dariotable."BUR_SAND_BLOair1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA1_SUM_CAL_EA", dariotable."BUR_SAND_SCRA2_SUM_CAL_EA", dariotable."BUR_SAND_STIR_SUM_CAL_EA", dariotable."BUR_SBA_JCL1_SUM_CAL_EA", dariotable."BUR_SBA_JCL2_SUM_CAL_EA", dariotable."BUR_SBA_PUM1_SUM_CAL_EA", dariotable."BUR_SBA_PUM2_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas1_SUM_CAL_EA", dariotable."BUR_SCL_PUMsas2_SUM_CAL_EA", dariotable."BUR_SCL1_SCRA_SUM_CAL_EA", dariotable."BUR_SCL2_SCRA_SUM_CAL_EA", dariotable."BUR_SCL3_SCRA_SUM_CAL_EA", dariotable."BUR_SCR_GRI1_SUM_CAL_EA", dariotable."BUR_SCR_GRI2_SUM_CAL_EA", dariotable."BUR_SDEW_COMP_CAL_EA", dariotable."BUR_SDEW_DOSflo1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSflo2_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre1_SUM_CAL_EA", dariotable."BUR_SDEW_DOSpre2_SUM_CAL_EA", dariotable."BUR_SDEW_PUMhp_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", dariotable."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", dariotable."BUR_SSTH_CEN1_SUM_CAL_EA", dariotable."BUR_SSTH_CEN2_SUM_CAL_EA", dariotable."BUR_SSTHi1_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHi2_PUMsas_SUM_CAL_EA", dariotable."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", dariotable."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", dariotable."BUR_TWT_PUM1_SUM_CAL_EA", dariotable."BUR_TWT_PUM2_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw1_SUM_CAL_EA", dariotable."BUR_WWTP_PUMpw2_SUM_CAL_EA", dariotable."BUR_WWTPo_SAMPww_SUM_OH", dariotable."BUR_COGEto_SUM_PROD" FROM (SELECT "time".datatime, ah1."BUR_ANDI_PUMhsl1_SUM_CAL_EA", ah2."BUR_ANDI_PUMhsl2_SUM_CAL_EA", b4."BUR_ANDIi1_STIR_SUM_EA", b5."BUR_ANDIi2_STIR_SUM_EA", h14."BUR_BIO_BLO1_SUM_CAL_EA", b0."BUR_BIO_BLO12_SUM_EA", h15."BUR_BIO_BLO2_SUM_CAL_EA", h16."BUR_BIO_BLO3_SUM_CAL_EA", b3."BUR_BIO_BLO34_SUM_EA", h170."BUR_BIO_BLO4_SUM_CAL_EA", h17."BUR_BIO_BLO5_SUM_CAL_EA", b1."BUR_BIO_BLO5_SUM_EA", h18."BUR_BIO_BLO6_SUM_CAL_EA", b2."BUR_BIO_BLO6_SUM_EA", h210."BUR_BIO_PUMrcir1_SUM_CAL_EA", h21."BUR_BIO_PUMrcir2_SUM_CAL_EA", h1."BUR_BIOaer1_STIR1_SUM_CAL_EA", h100."BUR_BIOaer1_STIR2_SUM_CAL_EA", h2."BUR_BIOaer2_STIR1_SUM_CAL_EA", h3."BUR_BIOaer2_STIR2_SUM_CAL_EA", h4."BUR_BIOaer3_STIR1_SUM_CAL_EA", h6."BUR_BIOaer3_STIR2_SUM_CAL_EA", h7."BUR_BIOanox1_STIR3_SUM_CAL_EA", h8."BUR_BIOanox1_STIR4_SUM_CAL_EA", h9."BUR_BIOanox2_STIR3_SUM_CAL_EA", h10."BUR_BIOanox2_STIR4_SUM_CAL_EA", h11."BUR_BIOanox3_STIR3_SUM_CAL_EA", h12."BUR_BIOanox3_STIR4_SUM_CAL_EA", h19."BUR_BIOP2_STIR1_SUM_CAL_EA", h20."BUR_BIOP2_STIR2_SUM_CAL_EA", oth3."BUR_DSTH_PFE_CAL_EA", oth1."BUR_DSTHo_PUM1_SUM_OH", oth2."BUR_DSTHo_PUM2_SUM_OH", b6."BUR_ELPS_ELPS_SUM_EA", oth4."BUR_FLA_TOR_SUM_OH", flof1."BUR_FLOF_BLO4_SUM_CAL_EA", flof2."BUR_FLOF_BLO5_SUM_CAL_EA", b7."BUR_FLOF_BLObw1_SUM_EA", b8."BUR_FLOF_BLObw2_SUM_EA", b9."BUR_FLOF_BLObw3_SUM_EA", b10."BUR_FLOF_PUMbw1_SUM_EA", b11."BUR_FLOF_PUMbw2_SUM_EA", b12."BUR_FLOF_PUMbw3_SUM_EA", flof4."BUR_FLOF_PUMdrw_CAL_EA", flof5."BUR_FLOF_PUMpre1_CAL_EA", flof6."BUR_FLOF_PUMpre2_CAL_EA", flof7."BUR_FLOF_PUMsluw1_SUM_CAL_EA", flof8."BUR_FLOF_PUMsluw2_SUM_CAL_EA", flof9."BUR_FLOF_STIRsluw_SUM_CAL_EA", flof3."BUR_FLOFi_SAMPww_SUM_OH", oth5."BUR_GLOB_BOIL_SUM_OH", oth6."BUR_HPS_PUM1_SUM_CAL_EA", oth7."BUR_HPS_PUM2_SUM_CAL_EA", oth8."BUR_HPS_PUM3_SUM_CAL_EA", oth9."BUR_HPS_PUM4_SUM_CAL_EA", b13."BUR_IPS_PUM1_SUM_EA", b15."BUR_IPS_PUM2_SUM_EA", b16."BUR_IPS_PUM3_SUM_EA", b17."BUR_IPS_PUM4_SUM_EA", b18."BUR_IPS_PUM5_SUM_EA", b19."BUR_IPS_PUM6_SUM_EA", oth10."BUR_PCL_PUMps1_SUM_CAL_EA", oth11."BUR_PCL_PUMps2_SUM_CAL_EA", oth12."BUR_PCL_SCRA1_SUM_CAL_EA", oth13."BUR_PCL_SCRA2_SUM_CAL_EA", oth14."BUR_PSTHo_PUMtpsl_SUM_CAL_EA", oth15."BUR_SAND_BLO2_SUM_CAL_EA", oth16."BUR_SAND_BLO3_SUM_CAL_EA", oth17."BUR_SAND_BLOair1_SUM_CAL_EA", oth18."BUR_SAND_SCRA1_SUM_CAL_EA", oth19."BUR_SAND_SCRA2_SUM_CAL_EA", oth20."BUR_SAND_STIR_SUM_CAL_EA", oth21."BUR_SBA_JCL1_SUM_CAL_EA", oth22."BUR_SBA_JCL2_SUM_CAL_EA", oth23."BUR_SBA_PUM1_SUM_CAL_EA", oth24."BUR_SBA_PUM2_SUM_CAL_EA", oth28."BUR_SCL_PUMsas1_SUM_CAL_EA", oth29."BUR_SCL_PUMsas2_SUM_CAL_EA", oth25."BUR_SCL1_SCRA_SUM_CAL_EA", oth26."BUR_SCL2_SCRA_SUM_CAL_EA", oth27."BUR_SCL3_SCRA_SUM_CAL_EA", oth30."BUR_SCR_GRI1_SUM_CAL_EA", oth31."BUR_SCR_GRI2_SUM_CAL_EA", oth58."BUR_SDEW_COMP_CAL_EA", oth33."BUR_SDEW_DOSflo1_SUM_CAL_EA", oth34."BUR_SDEW_DOSflo2_SUM_CAL_EA", oth35."BUR_SDEW_DOSpre1_SUM_CAL_EA", oth36."BUR_SDEW_DOSpre2_SUM_CAL_EA", oth39."BUR_SDEW_PUMhp_SUM_CAL_EA", oth37."BUR_SDEWi_PUMtdsl1_SUM_CAL_EA", oth38."BUR_SDEWi_PUMtdsl2_SUM_CAL_EA", oth40."BUR_SSTH_CEN1_SUM_CAL_EA", oth41."BUR_SSTH_CEN2_SUM_CAL_EA", oth42."BUR_SSTHi1_PUMsas_SUM_CAL_EA", oth43."BUR_SSTHi2_PUMsas_SUM_CAL_EA", oth44."BUR_SSTHo1_PUMtssl_SUM_CAL_EA", oth71."BUR_SSTHo2_PUMtssl_SUM_CAL_EA", oth46."BUR_TWT_PUM1_SUM_CAL_EA", oth47."BUR_TWT_PUM2_SUM_CAL_EA", oth49."BUR_WWTP_PUMpw1_SUM_CAL_EA", oth50."BUR_WWTP_PUMpw2_SUM_CAL_EA", oth48."BUR_WWTPo_SAMPww_SUM_OH", oth98."BUR_COGEto_SUM_PROD" FROM ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data WHERE (agg_plant_data.interval_type = 'day'::text) ORDER BY agg_plant_data.date) "time" LEFT JOIN (SELECT DISTINCT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO12_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO12_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b0 ON (("time".datatime = b0.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b1 ON (("time".datatime = b1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b2 ON (("time".datatime = b2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_BIO_BLO34_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO34_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b3 ON (("time".datatime = b3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h1 ON (("time".datatime = h1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer1_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer1_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h100 ON (("time".datatime = h100.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h2 ON (("time".datatime = h2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h3 ON (("time".datatime = h3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h4 ON (("time".datatime = h4.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2.7)::double precision) AS "BUR_BIOaer3_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOaer3_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h6 ON (("time".datatime = h6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h7 ON (("time".datatime = h7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox1_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox1_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h8 ON (("time".datatime = h8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h9 ON (("time".datatime = h9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox2_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox2_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h10 ON (("time".datatime = h10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h11 ON (("time".datatime = h11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.5)::double precision) AS "BUR_BIOanox3_STIR4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOanox3_STIR4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h12 ON (("time".datatime = h12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h14 ON (("time".datatime = h14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h15 ON (("time".datatime = h15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h16 ON (("time".datatime = h16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (80)::double precision) AS "BUR_BIO_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h170 ON (("time".datatime = h170.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h17 ON (("time".datatime = h17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (53)::double precision) AS "BUR_BIO_BLO6_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_BLO6_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h18 ON (("time".datatime = h18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.6)::double precision) AS "BUR_BIOP2_STIR1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h19 ON (("time".datatime = h19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.4)::double precision) AS "BUR_BIOP2_STIR2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIOP2_STIR2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h20 ON (("time".datatime = h20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h210 ON (("time".datatime = h210.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (35)::double precision) AS "BUR_BIO_PUMrcir2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_BIO_PUMrcir2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) h21 ON (("time".datatime = h21.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi1_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi1_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b4 ON (("time".datatime = b4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ANDIi2_STIR_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDIi2_STIR_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b5 ON (("time".datatime = b5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah1 ON (("time".datatime = ah1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7.6)::double precision) AS "BUR_ANDI_PUMhsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ANDI_PUMhsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) ah2 ON (("time".datatime = ah2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_ELPS_ELPS_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_ELPS_ELPS_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b6 ON (("time".datatime = b6.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b7 ON (("time".datatime = b7.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b8 ON (("time".datatime = b8.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_BLObw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLObw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b9 ON (("time".datatime = b9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b10 ON (("time".datatime = b10.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b11 ON (("time".datatime = b11.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOF_PUMbw3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMbw3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b12 ON (("time".datatime = b12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof1 ON (("time".datatime = flof1.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_FLOF_BLO5_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_BLO5_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof2 ON (("time".datatime = flof2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLOFi_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOFi_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof3 ON (("time".datatime = flof3.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (3.15)::double precision) AS "BUR_FLOF_PUMdrw_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMdrw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof4 ON (("time".datatime = flof4.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.385)::double precision) AS "BUR_FLOF_PUMpre1_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof5 ON (("time".datatime = flof5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.385)::double precision) AS "BUR_FLOF_PUMpre2_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof6 ON (("time".datatime = flof6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof7 ON (("time".datatime = flof7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5)::double precision) AS "BUR_FLOF_PUMsluw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_PUMsluw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof8 ON (("time".datatime = flof8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.8)::double precision) AS "BUR_FLOF_STIRsluw_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLOF_STIRsluw_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) flof9 ON (("time".datatime = flof9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM1_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM1_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b13 ON (("time".datatime = b13.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM2_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM2_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b15 ON (("time".datatime = b15.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM3_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM3_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b16 ON (("time".datatime = b16.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM4_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM4_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b17 ON (("time".datatime = b17.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM5_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM5_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b18 ON (("time".datatime = b18.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_IPS_PUM6_SUM_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_IPS_PUM6_SUM_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) b19 ON (("time".datatime = b19.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM1_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth1 ON (("time".datatime = oth1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_DSTHo_PUM2_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTHo_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth2 ON (("time".datatime = oth2.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_DSTH_PFE_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_DSTH_PFE_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth3 ON (("time".datatime = oth3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_FLA_TOR_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_FLA_TOR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth4 ON (("time".datatime = oth4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_GLOB_BOIL_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_GLOB_BOIL_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth5 ON (("time".datatime = oth5.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth6 ON (("time".datatime = oth6.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth7 ON (("time".datatime = oth7.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth8 ON (("time".datatime = oth8.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_HPS_PUM4_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_HPS_PUM4_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth9 ON (("time".datatime = oth9.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth10 ON (("time".datatime = oth10.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (4.5)::double precision) AS "BUR_PCL_PUMps2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_PUMps2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth11 ON (("time".datatime = oth11.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth12 ON (("time".datatime = oth12.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_PCL_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PCL_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth13 ON (("time".datatime = oth13.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_PSTHo_PUMtpsl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_PSTHo_PUMtpsl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth14 ON (("time".datatime = oth14.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth15 ON (("time".datatime = oth15.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLO3_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO3_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth16 ON (("time".datatime = oth16.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.7)::double precision) AS "BUR_SAND_BLOair1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_BLO1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth17 ON (("time".datatime = oth17.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth18 ON (("time".datatime = oth18.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.25)::double precision) AS "BUR_SAND_SCRA2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_SCRA2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth19 ON (("time".datatime = oth19.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SAND_STIR_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SAND_STIR_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth20 ON (("time".datatime = oth20.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8.5)::double precision) AS "BUR_SBA_JCL1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth21 ON (("time".datatime = oth21.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (11.58)::double precision) AS "BUR_SBA_JCL2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_JCL2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth22 ON (("time".datatime = oth22.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth23 ON (("time".datatime = oth23.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (7)::double precision) AS "BUR_SBA_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SBA_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth24 ON (("time".datatime = oth24.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL1_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL1_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth25 ON (("time".datatime = oth25.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL2_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL2_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth26 ON (("time".datatime = oth26.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.3)::double precision) AS "BUR_SCL3_SCRA_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL3_SCRA_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth27 ON (("time".datatime = oth27.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth28 ON (("time".datatime = oth28.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.3)::double precision) AS "BUR_SCL_PUMsas2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCL_PUMsas2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth29 ON (("time".datatime = oth29.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth30 ON (("time".datatime = oth30.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (1.2)::double precision) AS "BUR_SCR_GRI2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SCR_GRI2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth31 ON (("time".datatime = oth31.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth33 ON (("time".datatime = oth33.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (2)::double precision) AS "BUR_SDEW_DOSflo2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSflo2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth34 ON (("time".datatime = oth34.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth35 ON (("time".datatime = oth35.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (0.6)::double precision) AS "BUR_SDEW_DOSpre2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_DOSpre2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth36 ON (("time".datatime = oth36.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth37 ON (("time".datatime = oth37.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (6)::double precision) AS "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEWi_PUMtdsl2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth38 ON (("time".datatime = oth38.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (46)::double precision) AS "BUR_SDEW_PUMhp_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_PUMhp_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth39 ON (("time".datatime = oth39.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth40 ON (("time".datatime = oth40.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (28)::double precision) AS "BUR_SSTH_CEN2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTH_CEN2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth41 ON (("time".datatime = oth41.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi1_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi1_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth42 ON (("time".datatime = oth42.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (13)::double precision) AS "BUR_SSTHi2_PUMsas_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHi2_PUMsas_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth43 ON (("time".datatime = oth43.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo1_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo1_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth44 ON (("time".datatime = oth44.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth46 ON (("time".datatime = oth46.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (8)::double precision) AS "BUR_TWT_PUM2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_TWT_PUM2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth47 ON (("time".datatime = oth47.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "BUR_WWTPo_SAMPww_SUM_OH" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTPo_SAMPww_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth48 ON (("time".datatime = oth48.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw1_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMprw1_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth49 ON (("time".datatime = oth49.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (21)::double precision) AS "BUR_WWTP_PUMpw2_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_WWTP_PUMprw2_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth50 ON (("time".datatime = oth50.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (10.5)::double precision) AS "BUR_SDEW_COMP_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SDEW_COMP_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth58 ON (("time".datatime = oth58.date))) LEFT JOIN (SELECT agg_plant_data.date, (agg_plant_data.value * (5.11)::double precision) AS "BUR_SSTHo2_PUMtssl_SUM_CAL_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'BUR_SSTHo2_PUMtssl_SUM_OH'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.date) oth71 ON (("time".datatime = oth71.date))) LEFT JOIN (SELECT kpi_parts.date, kpi_parts.value AS "BUR_COGEto_SUM_PROD" FROM kpi_parts WHERE ((kpi_parts.sensor_id ~~ '%COGEto_SUM_PROD%'::text) AND (kpi_parts.interval_type = 'day'::text)) ORDER BY kpi_parts.date) oth98 ON (("time".datatime = oth98.date)))) dariotable;


ALTER TABLE inners_data."BUR_Report_rev4" OWNER TO inners_data;

--
-- Name: benchmark_values; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE benchmark_values (
    id integer NOT NULL,
    benchmark_id integer NOT NULL,
    x integer DEFAULT 0 NOT NULL,
    y real DEFAULT 0 NOT NULL
);


ALTER TABLE inners_data.benchmark_values OWNER TO inners_data;

--
-- Name: benchmarks; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE benchmarks (
    id integer NOT NULL,
    name text,
    description text,
    x_axis_label text,
    y_axis_label text,
    x_units text,
    y_units text
);


ALTER TABLE inners_data.benchmarks OWNER TO inners_data;

--
-- Name: Bench_view; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "Bench_view" AS
    SELECT b.name, v.x AS "PE", v.y, b.y_units FROM ((SELECT benchmark_values.id, benchmark_values.benchmark_id, benchmark_values.x, benchmark_values.y FROM benchmark_values) v LEFT JOIN (SELECT benchmarks.id, benchmarks.name, benchmarks.description, benchmarks.x_axis_label, benchmarks.y_axis_label, benchmarks.x_units, benchmarks.y_units FROM benchmarks) b ON ((v.benchmark_id = b.id)));


ALTER TABLE inners_data."Bench_view" OWNER TO inners_data;

--
-- Name: kpi_equations; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE kpi_equations (
    kpi_name text,
    component_list text,
    equation text,
    constraints text,
    is_kpi boolean,
    free_text text,
    issues text,
    id integer NOT NULL,
    CONSTRAINT component_list_is_valid CHECK ((component_list ~* '^\s*\(\s*''[A-Za-z0-9_]*''(\s*,\s*''[A-Za-z0-9_]*'')*\)\s*$'::text))
);


ALTER TABLE inners_data.kpi_equations OWNER TO inners_data;

--
-- Name: kpi_plant_benchmark; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE kpi_plant_benchmark (
    id integer NOT NULL,
    kpi_id text NOT NULL,
    plant_id text NOT NULL,
    benchmark_id integer,
    too_high_value double precision,
    too_low_value double precision,
    too_high_message_id integer,
    too_low_message_id integer,
    "Unit of measurement" text
);


ALTER TABLE inners_data.kpi_plant_benchmark OWNER TO inners_data;

--
-- Name: DARIO_KPI_Unit_of_Measurement; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "DARIO_KPI_Unit_of_Measurement" AS
    SELECT a.kpi_id, a.plant_id, a."Unit of measurement", k.component_list, k.equation FROM (kpi_plant_benchmark a FULL JOIN (SELECT kpi_equations.kpi_name, kpi_equations.component_list, kpi_equations.equation, kpi_equations.constraints, kpi_equations.is_kpi, kpi_equations.free_text, kpi_equations.issues, kpi_equations.id FROM kpi_equations WHERE (kpi_equations.kpi_name = ANY (ARRAY['KPI_PCL_HRT'::text, 'KPI_PCL_HRT'::text, 'KPI_ANDI_HRT'::text, 'KPI_ANDI_HRT'::text, 'KPI_BIO1_HRT'::text, 'KPI_BIO1_HRT'::text, 'KPI_SCL_HRT'::text, 'KPI_SCL_HRT'::text, 'KPI_WWTP_SAMP_NH4NM_ELI'::text, 'KPI_WWTP_SAMP_NH4NM_ELI'::text, 'KPI_WWTPi_NH4NTN_RATIO'::text, 'KPI_WWTPi_NH4NTN_RATIO'::text, 'KPI_BIO_BLO_CONS_EA_perOH'::text, 'KPI_BIO1_CONS_EAstir_perVOLtan'::text, 'KPI_BIO_CONS_EAstir_perVOLtan'::text, 'KPIp_CO2_Emissions'::text, 'KPI_BIO_CONS_EAblo_perVOLair'::text, 'KPI_BIO_BLO_CONS_EA_perPE'::text, 'KPI_BIO_CONS_EAblo_perVOLww'::text, 'KPI_WWTPi_CONS_EApum_perCODM'::text, 'KPI_WWTP_CONS_EA_perVOLww'::text, 'KPI_WWTP_COVel_RATIO'::text, 'KPI_WWTP_CONS_EA_perPEd'::text, 'KPI_SAND_CONS_EAblo_perVOLww'::text, 'KPI_WWTPi_CODM_perCODMd_RATIO'::text, 'KPI_WWTP_CONS_EA_perPE'::text, 'KPI_WWTP_SEN_NH4NM_ELI'::text, 'KPI_UV_CONS_EAuvl_perVOLww'::text, 'KPI_WWTP_PEcal_perPEd_RATIO'::text]))) k ON ((k.kpi_name = a.kpi_id)));


ALTER TABLE inners_data."DARIO_KPI_Unit_of_Measurement" OWNER TO inners_data;

--
-- Name: Dario_Report_Jan2014; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE "Dario_Report_Jan2014" (
    dariodate timestamp without time zone,
    "BUR_ANDI_PUMhsl1_SUM_CAL_EA" double precision,
    "BUR_ANDI_PUMhsl2_SUM_CAL_EA" double precision,
    "BUR_ANDIi1_STIR_SUM_EA" double precision,
    "BUR_ANDIi2_STIR_SUM_EA" double precision,
    "BUR_BIO_BLO1_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO12_SUM_EA" double precision,
    "BUR_BIO_BLO2_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO3_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO34_SUM_EA" double precision,
    "BUR_BIO_BLO4_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO5_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO5_SUM_EA" double precision,
    "BUR_BIO_BLO6_SUM_CAL_EA" double precision,
    "BUR_BIO_BLO6_SUM_EA" double precision,
    "BUR_BIO_PUMrcir1_SUM_CAL_EA" double precision,
    "BUR_BIO_PUMrcir2_SUM_CAL_EA" double precision,
    "BUR_BIOaer1_STIR1_SUM_CAL_EA" double precision,
    "BUR_BIOaer1_STIR2_SUM_CAL_EA" double precision,
    "BUR_BIOaer2_STIR1_SUM_CAL_EA" double precision,
    "BUR_BIOaer2_STIR2_SUM_CAL_EA" double precision,
    "BUR_BIOaer3_STIR1_SUM_CAL_EA" double precision,
    "BUR_BIOaer3_STIR2_SUM_CAL_EA" double precision,
    "BUR_BIOanox1_STIR3_SUM_CAL_EA" double precision,
    "BUR_BIOanox1_STIR4_SUM_CAL_EA" double precision,
    "BUR_BIOanox2_STIR3_SUM_CAL_EA" double precision,
    "BUR_BIOanox2_STIR4_SUM_CAL_EA" double precision,
    "BUR_BIOanox3_STIR3_SUM_CAL_EA" double precision,
    "BUR_BIOanox3_STIR4_SUM_CAL_EA" double precision,
    "BUR_BIOP2_STIR1_SUM_CAL_EA" double precision,
    "BUR_BIOP2_STIR2_SUM_CAL_EA" double precision,
    "BUR_DSTH_PFE_SUM_OH" double precision,
    "BUR_DSTHo_PUM1_SUM_OH" double precision,
    "BUR_DSTHo_PUM2_SUM_OH" double precision,
    "BUR_ELPS_ELPS_SUM_EA" double precision,
    "BUR_FLA_TOR_SUM_OH" double precision,
    "BUR_FLOF_BLO4_SUM_CAL_EA" double precision,
    "BUR_FLOF_BLO5_SUM_CAL_EA" double precision,
    "BUR_FLOF_BLObw1_SUM_EA" double precision,
    "BUR_FLOF_BLObw2_SUM_EA" double precision,
    "BUR_FLOF_BLObw3_SUM_EA" double precision,
    "BUR_FLOF_PUMbw1_SUM_EA" double precision,
    "BUR_FLOF_PUMbw2_SUM_EA" double precision,
    "BUR_FLOF_PUMbw3_SUM_EA" double precision,
    "BUR_FLOF_PUMdrw_SUM_OH" double precision,
    "BUR_FLOF_PUMpre1_SUM_OH" double precision,
    "BUR_FLOF_PUMpre2_SUM_OH" double precision,
    "BUR_FLOF_PUMsluw1_SUM_CAL_EA" double precision,
    "BUR_FLOF_PUMsluw2_SUM_CAL_EA" double precision,
    "BUR_FLOF_STIRsluw_SUM_CAL_EA" double precision,
    "BUR_FLOFi_SAMPww_SUM_OH" double precision,
    "BUR_GLOB_BOIL_SUM_OH" double precision,
    "BUR_HPS_PUM1_SUM_CAL_EA" double precision,
    "BUR_HPS_PUM2_SUM_CAL_EA" double precision,
    "BUR_HPS_PUM3_SUM_CAL_EA" double precision,
    "BUR_HPS_PUM4_SUM_CAL_EA" double precision,
    "BUR_IPS_PUM1_SUM_EA" double precision,
    "BUR_IPS_PUM2_SUM_EA" double precision,
    "BUR_IPS_PUM3_SUM_EA" double precision,
    "BUR_IPS_PUM4_SUM_EA" double precision,
    "BUR_IPS_PUM5_SUM_EA" double precision,
    "BUR_IPS_PUM6_SUM_EA" double precision,
    "BUR_PCL_PUMps1_SUM_CAL_EA" double precision,
    "BUR_PCL_PUMps2_SUM_CAL_EA" double precision,
    "BUR_PCL_SCRA1_SUM_CAL_EA" double precision,
    "BUR_PCL_SCRA2_SUM_CAL_EA" double precision,
    "BUR_PSTHo_PUMtpsl_SUM_CAL_EA" double precision,
    "BUR_SAND_BLO2_SUM_CAL_EA" double precision,
    "BUR_SAND_BLO3_SUM_CAL_EA" double precision,
    "BUR_SAND_BLOair1_SUM_CAL_EA" double precision,
    "BUR_SAND_SCRA1_SUM_CAL_EA" double precision,
    "BUR_SAND_SCRA2_SUM_CAL_EA" double precision,
    "BUR_SAND_STIR_SUM_CAL_EA" double precision,
    "BUR_SBA_JCL1_SUM_CAL_EA" double precision,
    "BUR_SBA_JCL2_SUM_CAL_EA" double precision,
    "BUR_SBA_PUM1_SUM_CAL_EA" double precision,
    "BUR_SBA_PUM2_SUM_CAL_EA" double precision,
    "BUR_SCL_PUMsas1_SUM_CAL_EA" double precision,
    "BUR_SCL_PUMsas2_SUM_CAL_EA" double precision,
    "BUR_SCL1_SCRA_SUM_CAL_EA" double precision,
    "BUR_SCL2_SCRA_SUM_CAL_EA" double precision,
    "BUR_SCL3_SCRA_SUM_CAL_EA" double precision,
    "BUR_SCR_GRI1_SUM_CAL_EA" double precision,
    "BUR_SCR_GRI2_SUM_CAL_EA" double precision,
    "BUR_SDEW_COMP_SUM_OH" double precision,
    "BUR_SDEW_DOSflo1_SUM_CAL_EA" double precision,
    "BUR_SDEW_DOSflo2_SUM_CAL_EA" double precision,
    "BUR_SDEW_DOSpre1_SUM_CAL_EA" double precision,
    "BUR_SDEW_DOSpre2_SUM_CAL_EA" double precision,
    "BUR_SDEW_PUMhp_SUM_CAL_EA" double precision,
    "BUR_SDEWi_PUMtdsl1_SUM_CAL_EA" double precision,
    "BUR_SDEWi_PUMtdsl2_SUM_CAL_EA" double precision,
    "BUR_SSTH_CEN1_SUM_CAL_EA" double precision,
    "BUR_SSTH_CEN2_SUM_CAL_EA" double precision,
    "BUR_SSTHi1_PUMsas_SUM_CAL_EA" double precision,
    "BUR_SSTHi2_PUMsas_SUM_CAL_EA" double precision,
    "BUR_SSTHo1_PUMtssl_SUM_CAL_EA" double precision,
    "BUR_SSTHo2_PUMtssl_SUM_CAL_EA" double precision,
    "BUR_TWT_PUM1_SUM_CAL_EA" double precision,
    "BUR_TWT_PUM2_SUM_CAL_EA" double precision,
    "BUR_WWTP_PUMpw1_SUM_CAL_EA" double precision,
    "BUR_WWTP_PUMpw2_SUM_CAL_EA" double precision,
    "BUR_WWTPo_SAMPww_SUM_OH" double precision
);


ALTER TABLE inners_data."Dario_Report_Jan2014" OWNER TO inners_data;

--
-- Name: HSG_Report; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "HSG_Report" AS
    SELECT "time".datatime, hsg1."HSG_AIR_UVLair_MAX_MIN_EA", hsg6."HSG_BIO_BLOair1_MAX_MIN_EA", hsg7."HSG_BIO_BLOair2_MAX_MIN_EA", hsg8."HSG_BIO_BLOair3_MAX_MIN_EA", hsg2."HSG_BIO1_STIRww1_MAX_MIN_EA", hsg3."HSG_BIO1_STIRww2_MAX_MIN_EA", hsg4."HSG_BIO2_STIRww1_MAX_MIN_EA", hsg5."HSG_BIO2_STIRww2_MAX_MIN_EA", hsg9."HSG_EMPS_SEN_MAX_MIN_EA", hsg10."HSG_MBUI_CCL_MAX_MIN_EA", hsg11."HSG_MBUI_SEN_MAX_MIN_EA", hsg12."HSG_SAND_BLOair4_MAX_MIN_EA", hsg13."HSG_SAND_BLOair5_MAX_MIN_EA", hsg14."HSG_SANF_COMPww_MAX_MIN_EA", hsg15."HSG_SCL1_PUMras_MAX_MIN_EA", hsg16."HSG_SCL2_PUMras_MAX_MIN_EA", hsg17."HSG_SCL3_PUMras_MAX_MIN_EA", hsg18."HSG_SDEW_CENslu_MAX_MIN_EA", hsg19."HSG_SSTO_STIRslu_MAX_MIN_EA", hsg20."HSG_UV_UVLa1_MAX_MIN_EA", hsg21."HSG_UV_UVLa2_MAX_MIN_EA", hsg22."HSG_UV_UVLb1_MAX_MIN_EA", hsg23."HSG_UV_UVLb2_MAX_MIN_EA", hsg24."HSG_WOSH_NA_MAX_MIN_EA", hsg28."HSG_WWTP_NA_MAX_MIN_EA", hsg25."HSG_WWTPi_PUMww1_MAX_MIN_EA", hsg26."HSG_WWTPi_PUMww2_MAX_MIN_EA", hsg27."HSG_WWTPi_PUMww3_MAX_MIN_EA" FROM (((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data ORDER BY agg_plant_data.date) "time" LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_AIR_UVLair_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_AIR_UVLair_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg1 ON (("time".datatime = hsg1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO1_STIRww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO1_STIRww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg2 ON (("time".datatime = hsg2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO1_STIRww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO1_STIRww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg3 ON (("time".datatime = hsg3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO2_STIRww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO2_STIRww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg4 ON (("time".datatime = hsg4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO2_STIRww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO2_STIRww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg5 ON (("time".datatime = hsg5.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLOair1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLOair1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg6 ON (("time".datatime = hsg6.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLOair2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLOair2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg7 ON (("time".datatime = hsg7.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLOair3_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLOair3_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg8 ON (("time".datatime = hsg8.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_EMPS_SEN_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_EMPS_SEN_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg9 ON (("time".datatime = hsg9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_MBUI_CCL_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_MBUI_CCL_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg10 ON (("time".datatime = hsg10.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_MBUI_SEN_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_MBUI_SEN_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg11 ON (("time".datatime = hsg11.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SAND_BLOair4_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SAND_BLOair4_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg12 ON (("time".datatime = hsg12.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SAND_BLOair5_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SAND_BLOair5_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg13 ON (("time".datatime = hsg13.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SANF_COMPww_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SANF_COMPww_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg14 ON (("time".datatime = hsg14.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL1_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL1_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg15 ON (("time".datatime = hsg15.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL2_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL2_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg16 ON (("time".datatime = hsg16.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL3_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL3_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg17 ON (("time".datatime = hsg17.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SDEW_CENslu_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SDEW_CENslu_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg18 ON (("time".datatime = hsg18.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SSTO_STIRslu_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SSTO_STIRslu_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg19 ON (("time".datatime = hsg19.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLa1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLa1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg20 ON (("time".datatime = hsg20.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLa2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLa2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg21 ON (("time".datatime = hsg21.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLb1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLb1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg22 ON (("time".datatime = hsg22.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLb2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLb2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg23 ON (("time".datatime = hsg23.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WOSH_NA_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WOSH_NA_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg24 ON (("time".datatime = hsg24.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg25 ON (("time".datatime = hsg25.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg26 ON (("time".datatime = hsg26.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww3_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww3_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg27 ON (("time".datatime = hsg27.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTP_NA_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTP_NA_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg28 ON (("time".datatime = hsg28.date)));


ALTER TABLE inners_data."HSG_Report" OWNER TO inners_data;

--
-- Name: HSG_Report_rev1; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "HSG_Report_rev1" AS
    SELECT "time".datatime, hsg1."HSG_AIR_UVLair_MAX_MIN_EA", hsg6."HSG_BIO_BLO1_MAX_MIN_EA", hsg7."HSG_BIO_BLO2_MAX_MIN_EA", hsg8."HSG_BIO_BLO3_MAX_MIN_EA", hsg2."HSG_BIO1_STIRww1_MAX_MIN_EA", hsg3."HSG_BIO1_STIRww2_MAX_MIN_EA", hsg4."HSG_BIO2_STIRww1_MAX_MIN_EA", hsg5."HSG_BIO2_STIRww2_MAX_MIN_EA", hsg9."HSG_EMPS_SEN_MAX_MIN_EA", hsg10."HSG_MBUI_CCL_MAX_MIN_EA", hsg11."HSG_MBUI_SEN_MAX_MIN_EA", hsg12."HSG_SAND_BLOair4_MAX_MIN_EA", hsg13."HSG_SAND_BLOair5_MAX_MIN_EA", hsg14."HSG_SANF_COMPww_MAX_MIN_EA", hsg15."HSG_SCL1_PUMras_MAX_MIN_EA", hsg16."HSG_SCL2_PUMras_MAX_MIN_EA", hsg17."HSG_SCL3_PUMras_MAX_MIN_EA", hsg18."HSG_SDEW_CENsas_MAX_MIN_EA", hsg19."HSG_SSTO_STIRsas_MAX_MIN_EA", hsg20."HSG_UV_UVLa1_MAX_MIN_EA", hsg21."HSG_UV_UVLa2_MAX_MIN_EA", hsg22."HSG_UV_UVLb1_MAX_MIN_EA", hsg23."HSG_UV_UVLb2_MAX_MIN_EA", hsg24."HSG_WOSH_NA_MAX_MIN_EA", hsg28."HSG_WWTP_NA_MAX_MIN_EA", hsg25."HSG_WWTPi_PUMww1_MAX_MIN_EA", hsg26."HSG_WWTPi_PUMww2_MAX_MIN_EA", hsg27."HSG_WWTPi_PUMww3_MAX_MIN_EA" FROM (((((((((((((((((((((((((((((SELECT DISTINCT agg_plant_data.date AS datatime FROM agg_plant_data ORDER BY agg_plant_data.date) "time" LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_AIR_UVLair_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_AIR_UVLair_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg1 ON (("time".datatime = hsg1.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO1_STIRww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO1_STIRww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg2 ON (("time".datatime = hsg2.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO1_STIRww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO1_STIRww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg3 ON (("time".datatime = hsg3.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO2_STIRww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO2_STIRww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg4 ON (("time".datatime = hsg4.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO2_STIRww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO2_STIRww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg5 ON (("time".datatime = hsg5.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLO1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLO1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg6 ON (("time".datatime = hsg6.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLO2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLO2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg7 ON (("time".datatime = hsg7.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_BIO_BLO3_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_BIO_BLO3_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg8 ON (("time".datatime = hsg8.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_EMPS_SEN_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_EMPS_SEN_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg9 ON (("time".datatime = hsg9.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_MBUI_CCL_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_MBUI_CCL_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg10 ON (("time".datatime = hsg10.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_MBUI_SEN_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_MBUI_SEN_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg11 ON (("time".datatime = hsg11.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SAND_BLOair4_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SAND_BLOair4_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg12 ON (("time".datatime = hsg12.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SAND_BLOair5_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SAND_BLOair5_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg13 ON (("time".datatime = hsg13.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SANF_COMPww_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SANF_COMPww_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg14 ON (("time".datatime = hsg14.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL1_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL1_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg15 ON (("time".datatime = hsg15.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL2_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL2_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg16 ON (("time".datatime = hsg16.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SCL3_PUMras_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SCL3_PUMras_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg17 ON (("time".datatime = hsg17.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SDEW_CENsas_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SDEW_CENsas_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg18 ON (("time".datatime = hsg18.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_SSTO_STIRsas_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_SSTO_STIRsas_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg19 ON (("time".datatime = hsg19.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLa1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLa1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg20 ON (("time".datatime = hsg20.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLa2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLa2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg21 ON (("time".datatime = hsg21.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLb1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLb1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg22 ON (("time".datatime = hsg22.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_UV_UVLb2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_UV_UVLb2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg23 ON (("time".datatime = hsg23.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WOSH_NA_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WOSH_NA_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg24 ON (("time".datatime = hsg24.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww1_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww1_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg25 ON (("time".datatime = hsg25.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww2_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww2_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg26 ON (("time".datatime = hsg26.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTPi_PUMww3_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTPi_PUMww3_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg27 ON (("time".datatime = hsg27.date))) LEFT JOIN (SELECT agg_plant_data.date, agg_plant_data.value AS "HSG_WWTP_NA_MAX_MIN_EA" FROM agg_plant_data WHERE ((agg_plant_data.sensor_id = 'HSG_WWTP_NA_MAX_MIN_EA'::text) AND (agg_plant_data.interval_type = 'day'::text)) ORDER BY agg_plant_data.interval_type) hsg28 ON (("time".datatime = hsg28.date))) WHERE ("time".datatime > '2014-01-01 00:00:00'::timestamp without time zone);


ALTER TABLE inners_data."HSG_Report_rev1" OWNER TO inners_data;

--
-- Name: KPI_AVG_check; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "KPI_AVG_check" AS
    SELECT m.kpi_idx, y."Y.avg", y."Y.stddev", m."M.avg", m."M.stddev", w."W.avg", w."W.stddev", d."D.avg", d."D.stddev" FROM ((((SELECT kpis.kpi_id, avg(kpis.value) AS "Y.avg", stddev(kpis.value) AS "Y.stddev" FROM kpis WHERE (kpis.interval_type = 'year'::text) GROUP BY kpis.kpi_id) y FULL JOIN (SELECT kpis.kpi_id AS kpi_idx, avg(kpis.value) AS "M.avg", stddev(kpis.value) AS "M.stddev" FROM kpis WHERE (kpis.interval_type = 'month'::text) GROUP BY kpis.kpi_id) m ON ((y.kpi_id = m.kpi_idx))) FULL JOIN (SELECT kpis.kpi_id, avg(kpis.value) AS "W.avg", stddev(kpis.value) AS "W.stddev" FROM kpis WHERE (kpis.interval_type = 'week'::text) GROUP BY kpis.kpi_id) w ON ((y.kpi_id = w.kpi_id))) FULL JOIN (SELECT kpis.kpi_id, avg(kpis.value) AS "D.avg", stddev(kpis.value) AS "D.stddev" FROM kpis WHERE (kpis.interval_type = 'day'::text) GROUP BY kpis.kpi_id) d ON ((y.kpi_id = d.kpi_id)));


ALTER TABLE inners_data."KPI_AVG_check" OWNER TO inners_data;

--
-- Name: KPI_aut_report; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW "KPI_aut_report" AS
    SELECT table1.datatime, table1.plant, table1."KPI_ANDI_HRT", table1."KPI_BIO_BLO_CONS_EA_perPE", table1."KPI_BIO_CONS_EAblo_perVOLair", table1."KPI_BIO_CONS_EAblo_perVOLww", table1."KPI_BIO_CONS_EAstir_perVOLtan", table1."KPI_BIO1_CONS_EAstir_perVOLtan", table1."KPI_BIO1_HRT", table1."KPI_PCL_HRT", table1."KPI_SAND_CONS_EAblo_perVOLww", table1."KPI_SCL_HRT", table1."KPI_UV_CONS_EAuvl_perVOLww", table1."KPI_WWTP_CONS_EA_perPE", table1."KPI_WWTP_CONS_EA_perPEd", table1."KPI_WWTP_CONS_EA_perVOLww", table1."KPI_WWTP_COVel_RATIO", table1."KPI_WWTP_PEcal_perPEd_RATIO", table1."KPI_WWTP_SAMP_NH4NM_ELI", table1."KPI_WWTP_SEN_NH4NM_ELI", table1."KPI_WWTPi_CODM_perCODMd_RATIO", table1."KPI_WWTPi_CONS_EApum_perCODM", table1."KPI_WWTPi_NH4NTN_RATIO" FROM (SELECT hsgkpitable.datatime, (SELECT DISTINCT agg_plant_data.plant FROM agg_plant_data WHERE (agg_plant_data.plant = 'HSG'::text)) AS plant, hsgkpitable."KPI_ANDI_HRT", hsgkpitable."KPI_BIO_BLO_CONS_EA_perPE", hsgkpitable."KPI_BIO_CONS_EAblo_perVOLair", hsgkpitable."KPI_BIO_CONS_EAblo_perVOLww", hsgkpitable."KPI_BIO_CONS_EAstir_perVOLtan", hsgkpitable."KPI_BIO1_CONS_EAstir_perVOLtan", hsgkpitable."KPI_BIO1_HRT", hsgkpitable."KPI_PCL_HRT", hsgkpitable."KPI_SAND_CONS_EAblo_perVOLww", hsgkpitable."KPI_SCL_HRT", hsgkpitable."KPI_UV_CONS_EAuvl_perVOLww", hsgkpitable."KPI_WWTP_CONS_EA_perPE", hsgkpitable."KPI_WWTP_CONS_EA_perPEd", hsgkpitable."KPI_WWTP_CONS_EA_perVOLww", hsgkpitable."KPI_WWTP_COVel_RATIO", hsgkpitable."KPI_WWTP_PEcal_perPEd_RATIO", hsgkpitable."KPI_WWTP_SAMP_NH4NM_ELI", hsgkpitable."KPI_WWTP_SEN_NH4NM_ELI", hsgkpitable."KPI_WWTPi_CODM_perCODMd_RATIO", hsgkpitable."KPI_WWTPi_CONS_EApum_perCODM", hsgkpitable."KPI_WWTPi_NH4NTN_RATIO" FROM ((((((((((((((((((((((SELECT DISTINCT kpis.date AS datatime FROM kpis) d FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_ANDI_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi0 ON ((d.datatime = kpi0.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO1_CONS_EAstir_perVOLtan" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi1 ON ((d.datatime = kpi1.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO1_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi2 ON ((d.datatime = kpi2.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_BLO_CONS_EA_perPE" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi3 ON ((d.datatime = kpi3.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAblo_perVOLair" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi4 ON ((d.datatime = kpi4.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAblo_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi5 ON ((d.datatime = kpi5.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAstir_perVOLtan" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi6 ON ((d.datatime = kpi6.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_PCL_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi7 ON ((d.datatime = kpi7.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_SAND_CONS_EAblo_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi8 ON ((d.datatime = kpi8.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_SCL_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi9 ON ((d.datatime = kpi9.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_UV_CONS_EAuvl_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi10 ON ((d.datatime = kpi10.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perPE" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi11 ON ((d.datatime = kpi11.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perPEd" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi12 ON ((d.datatime = kpi12.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi13 ON ((d.datatime = kpi13.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_COVel_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi14 ON ((d.datatime = kpi14.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_CODM_perCODMd_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi15 ON ((d.datatime = kpi15.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_CONS_EApum_perCODM" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi16 ON ((d.datatime = kpi16.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_NH4NTN_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi17 ON ((d.datatime = kpi17.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_PEcal_perPEd_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi18 ON ((d.datatime = kpi18.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_SAMP_NH4NM_ELI" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi19 ON ((d.datatime = kpi19.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_SEN_NH4NM_ELI" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'HSG'::text)) AND (kpis.interval_type = 'day'::text))) kpi20 ON ((d.datatime = kpi20.date))) hsgkpitable) table1 UNION SELECT table2.datatime, table2.plant, table2."KPI_ANDI_HRT", table2."KPI_BIO_BLO_CONS_EA_perPE", table2."KPI_BIO_CONS_EAblo_perVOLair", table2."KPI_BIO_CONS_EAblo_perVOLww", table2."KPI_BIO_CONS_EAstir_perVOLtan", table2."KPI_BIO1_CONS_EAstir_perVOLtan", table2."KPI_BIO1_HRT", table2."KPI_PCL_HRT", table2."KPI_SAND_CONS_EAblo_perVOLww", table2."KPI_SCL_HRT", table2."KPI_UV_CONS_EAuvl_perVOLww", table2."KPI_WWTP_CONS_EA_perPE", table2."KPI_WWTP_CONS_EA_perPEd", table2."KPI_WWTP_CONS_EA_perVOLww", table2."KPI_WWTP_COVel_RATIO", table2."KPI_WWTP_PEcal_perPEd_RATIO", table2."KPI_WWTP_SAMP_NH4NM_ELI", table2."KPI_WWTP_SEN_NH4NM_ELI", table2."KPI_WWTPi_CODM_perCODMd_RATIO", table2."KPI_WWTPi_CONS_EApum_perCODM", table2."KPI_WWTPi_NH4NTN_RATIO" FROM (SELECT burkpitable.datatime, (SELECT DISTINCT agg_plant_data.plant FROM agg_plant_data WHERE (agg_plant_data.plant = 'BUR'::text)) AS plant, burkpitable."KPI_ANDI_HRT", burkpitable."KPI_BIO_BLO_CONS_EA_perPE", burkpitable."KPI_BIO_CONS_EAblo_perVOLair", burkpitable."KPI_BIO_CONS_EAblo_perVOLww", burkpitable."KPI_BIO_CONS_EAstir_perVOLtan", burkpitable."KPI_BIO1_CONS_EAstir_perVOLtan", burkpitable."KPI_BIO1_HRT", burkpitable."KPI_PCL_HRT", burkpitable."KPI_SAND_CONS_EAblo_perVOLww", burkpitable."KPI_SCL_HRT", burkpitable."KPI_UV_CONS_EAuvl_perVOLww", burkpitable."KPI_WWTP_CONS_EA_perPE", burkpitable."KPI_WWTP_CONS_EA_perPEd", burkpitable."KPI_WWTP_CONS_EA_perVOLww", burkpitable."KPI_WWTP_COVel_RATIO", burkpitable."KPI_WWTP_PEcal_perPEd_RATIO", burkpitable."KPI_WWTP_SAMP_NH4NM_ELI", burkpitable."KPI_WWTP_SEN_NH4NM_ELI", burkpitable."KPI_WWTPi_CODM_perCODMd_RATIO", burkpitable."KPI_WWTPi_CONS_EApum_perCODM", burkpitable."KPI_WWTPi_NH4NTN_RATIO" FROM ((((((((((((((((((((((SELECT DISTINCT kpis.date AS datatime FROM kpis) d FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_ANDI_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_ANDI_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi0 ON ((d.datatime = kpi0.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO1_CONS_EAstir_perVOLtan" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi1 ON ((d.datatime = kpi1.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO1_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO1_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi2 ON ((d.datatime = kpi2.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_BLO_CONS_EA_perPE" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_BLO_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi3 ON ((d.datatime = kpi3.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAblo_perVOLair" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLair'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi4 ON ((d.datatime = kpi4.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAblo_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi5 ON ((d.datatime = kpi5.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_BIO_CONS_EAstir_perVOLtan" FROM kpis WHERE (((kpis.kpi_id = 'KPI_BIO_CONS_EAstir_perVOLtan'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi6 ON ((d.datatime = kpi6.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_PCL_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_PCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi7 ON ((d.datatime = kpi7.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_SAND_CONS_EAblo_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_SAND_CONS_EAblo_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi8 ON ((d.datatime = kpi8.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_SCL_HRT" FROM kpis WHERE (((kpis.kpi_id = 'KPI_SCL_HRT'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi9 ON ((d.datatime = kpi9.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_UV_CONS_EAuvl_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_UV_CONS_EAuvl_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi10 ON ((d.datatime = kpi10.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perPE" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPE'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi11 ON ((d.datatime = kpi11.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perPEd" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perPEd'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi12 ON ((d.datatime = kpi12.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_CONS_EA_perVOLww" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_CONS_EA_perVOLww'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi13 ON ((d.datatime = kpi13.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_COVel_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_COVel_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi14 ON ((d.datatime = kpi14.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_CODM_perCODMd_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CODM_perCODMd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi15 ON ((d.datatime = kpi15.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_CONS_EApum_perCODM" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_CONS_EApum_perCODM'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi16 ON ((d.datatime = kpi16.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTPi_NH4NTN_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTPi_NH4NTN_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi17 ON ((d.datatime = kpi17.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_PEcal_perPEd_RATIO" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_PEcal_perPEd_RATIO'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi18 ON ((d.datatime = kpi18.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_SAMP_NH4NM_ELI" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SAMP_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi19 ON ((d.datatime = kpi19.date))) FULL JOIN (SELECT kpis.plant, kpis.date, kpis.value AS "KPI_WWTP_SEN_NH4NM_ELI" FROM kpis WHERE (((kpis.kpi_id = 'KPI_WWTP_SEN_NH4NM_ELI'::text) AND (kpis.plant = 'BUR'::text)) AND (kpis.interval_type = 'day'::text))) kpi20 ON ((d.datatime = kpi20.date))) burkpitable) table2 ORDER BY 2, 1;


ALTER TABLE inners_data."KPI_aut_report" OWNER TO inners_data;

--
-- Name: Sensors_List; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE "Sensors_List" (
    sensor_id text
);


ALTER TABLE inners_data."Sensors_List" OWNER TO inners_data;

--
-- Name: agg_plant_data_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE agg_plant_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.agg_plant_data_id_seq OWNER TO inners_data;

--
-- Name: agg_plant_data_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE agg_plant_data_id_seq OWNED BY agg_plant_data.id;


--
-- Name: aggregation_methods; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE aggregation_methods (
    id integer NOT NULL,
    aggregation_method text NOT NULL
);


ALTER TABLE inners_data.aggregation_methods OWNER TO inners_data;

--
-- Name: TABLE aggregation_methods; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE aggregation_methods IS 'List of valid aggregation methods';


--
-- Name: aggregation_methods_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE aggregation_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.aggregation_methods_id_seq OWNER TO inners_data;

--
-- Name: aggregation_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE aggregation_methods_id_seq OWNED BY aggregation_methods.id;


--
-- Name: bechhmark_values_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE bechhmark_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.bechhmark_values_id_seq OWNER TO inners_data;

--
-- Name: bechhmark_values_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE bechhmark_values_id_seq OWNED BY benchmark_values.id;


--
-- Name: benchmarks_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE benchmarks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.benchmarks_id_seq OWNER TO inners_data;

--
-- Name: benchmarks_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE benchmarks_id_seq OWNED BY benchmarks.id;


--
-- Name: broken_units; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE broken_units (
    replace_this text NOT NULL,
    with_this text NOT NULL,
    all_or_nothing boolean NOT NULL
);


ALTER TABLE inners_data.broken_units OWNER TO inners_data;

--
-- Name: TABLE broken_units; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE broken_units IS 'Replaces non-standard units (like m³) with ones recognized by the DB (like m^3)';


--
-- Name: constants; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE constants (
    id integer NOT NULL,
    name text NOT NULL,
    value text,
    units text,
    description text,
    notes text
);


ALTER TABLE inners_data.constants OWNER TO inners_data;

--
-- Name: COLUMN constants.name; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN constants.name IS 'Name of the constant';


--
-- Name: COLUMN constants.description; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN constants.description IS 'Description of this constant';


--
-- Name: constants_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE constants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.constants_id_seq OWNER TO inners_data;

--
-- Name: constants_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE constants_id_seq OWNED BY constants.id;


--
-- Name: dario_ke; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE dario_ke (
    kpi_name text,
    component_list text,
    equation text,
    constraints text,
    related_benchmark integer,
    is_kpi boolean,
    free_text text,
    issues text,
    id integer NOT NULL
);


ALTER TABLE inners_data.dario_ke OWNER TO inners_data;

--
-- Name: dario_ke_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE dario_ke_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.dario_ke_id_seq OWNER TO inners_data;

--
-- Name: dario_ke_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE dario_ke_id_seq OWNED BY dario_ke.id;


--
-- Name: dario_ke_novalues; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE dario_ke_novalues (
    kpi_name text,
    component_list text,
    equation text,
    constraints text,
    related_benchmark integer,
    is_kpi boolean,
    free_text text,
    issues text,
    id integer
);


ALTER TABLE inners_data.dario_ke_novalues OWNER TO inners_data;

--
-- Name: ds_messages; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE ds_messages (
    id integer NOT NULL,
    message text
);


ALTER TABLE inners_data.ds_messages OWNER TO inners_data;

--
-- Name: decision_support_message_display; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW decision_support_message_display AS
    SELECT k.date, m.message, k.plant, k.kpi_id, (((100)::double precision * (k.value - kpb.too_high_value)) / kpb.too_high_value) AS "%_Deviation" FROM ((kpi_plant_benchmark kpb JOIN kpis k ON ((kpb.kpi_id = k.kpi_id))) JOIN ds_messages m ON ((m.id = kpb.too_high_message_id))) WHERE ((k.value > kpb.too_high_value) AND (k.interval_type = 'day'::text)) UNION SELECT k.date, m.message, k.plant, k.kpi_id, (((100)::double precision * (kpb.too_low_value - k.value)) / kpb.too_low_value) AS "%_Deviation" FROM ((kpi_plant_benchmark kpb JOIN kpis k ON ((kpb.kpi_id = k.kpi_id))) JOIN ds_messages m ON ((m.id = kpb.too_low_message_id))) WHERE ((k.value < kpb.too_low_value) AND (k.interval_type = 'day'::text)) ORDER BY 1;


ALTER TABLE inners_data.decision_support_message_display OWNER TO inners_data;

--
-- Name: ds_messages_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE ds_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.ds_messages_id_seq OWNER TO inners_data;

--
-- Name: ds_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE ds_messages_id_seq OWNED BY ds_messages.id;


--
-- Name: import_reports; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE import_reports (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    filename text NOT NULL,
    plant text NOT NULL,
    vars_imported text[],
    vars_unknown text[],
    warnings text[],
    errors text[]
);


ALTER TABLE inners_data.import_reports OWNER TO inners_data;

--
-- Name: import_reports_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE import_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.import_reports_id_seq OWNER TO inners_data;

--
-- Name: import_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE import_reports_id_seq OWNED BY import_reports.id;


--
-- Name: interval_types; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE interval_types (
    id integer NOT NULL,
    interval_type text NOT NULL
);


ALTER TABLE inners_data.interval_types OWNER TO inners_data;

--
-- Name: TABLE interval_types; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE interval_types IS 'Table that lists all valid interval types; used for foreign keys and integrity checks';


--
-- Name: interval_types_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE interval_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.interval_types_id_seq OWNER TO inners_data;

--
-- Name: interval_types_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE interval_types_id_seq OWNED BY interval_types.id;


--
-- Name: intervals_to_update; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE intervals_to_update (
    interval_id integer,
    interval_type text,
    id integer NOT NULL
);


ALTER TABLE inners_data.intervals_to_update OWNER TO inners_data;

--
-- Name: intervals_to_update_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE intervals_to_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.intervals_to_update_id_seq OWNER TO inners_data;

--
-- Name: intervals_to_update_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE intervals_to_update_id_seq OWNED BY intervals_to_update.id;


--
-- Name: kpi_equations_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE kpi_equations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.kpi_equations_id_seq OWNER TO inners_data;

--
-- Name: kpi_equations_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE kpi_equations_id_seq OWNED BY kpi_equations.id;


--
-- Name: kpi_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE kpi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.kpi_id_seq OWNER TO inners_data;

--
-- Name: kpi_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE kpi_id_seq OWNED BY kpis.id;


--
-- Name: kpi_parts_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE kpi_parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.kpi_parts_id_seq OWNER TO inners_data;

--
-- Name: kpi_parts_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE kpi_parts_id_seq OWNED BY kpi_parts.id;


--
-- Name: kpi_plant_benchmark_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE kpi_plant_benchmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.kpi_plant_benchmark_id_seq OWNER TO inners_data;

--
-- Name: kpi_plant_benchmark_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE kpi_plant_benchmark_id_seq OWNED BY kpi_plant_benchmark.id;


--
-- Name: kpis_kpi_parts; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW kpis_kpi_parts AS
    SELECT kpis.id, kpis.date, kpis.kpi_id, kpis.value, kpis.interval_id, kpis.interval_type, kpis.plant FROM kpis UNION ALL SELECT kpi_parts.id, kpi_parts.date, kpi_parts.sensor_id AS kpi_id, kpi_parts.value, kpi_parts.interval_id, kpi_parts.interval_type, kpi_parts.plant FROM kpi_parts;


ALTER TABLE inners_data.kpis_kpi_parts OWNER TO inners_data;

--
-- Name: VIEW kpis_kpi_parts; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON VIEW kpis_kpi_parts IS 'Merges kpis and kpi_parts into a single table, a sort of one-stop shop for kpi-like data';


--
-- Name: nomenclature_acquisition_type; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_acquisition_type (
    key text NOT NULL,
    acquisition_type text
);


ALTER TABLE inners_data.nomenclature_acquisition_type OWNER TO inners_data;

--
-- Name: nomenclature_description; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_description (
    name text NOT NULL,
    description text NOT NULL
);


ALTER TABLE inners_data.nomenclature_description OWNER TO inners_data;

--
-- Name: nomenclature_device; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_device (
    key text NOT NULL,
    device text
);


ALTER TABLE inners_data.nomenclature_device OWNER TO inners_data;

--
-- Name: nomenclature_measurement_subject; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_measurement_subject (
    key text NOT NULL,
    measurement_subject text,
    units text
);


ALTER TABLE inners_data.nomenclature_measurement_subject OWNER TO inners_data;

--
-- Name: nomenclature_medium; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_medium (
    key text NOT NULL,
    medium text
);


ALTER TABLE inners_data.nomenclature_medium OWNER TO inners_data;

--
-- Name: nomenclature_resolution; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_resolution (
    key text NOT NULL,
    resolution_in_secs real
);


ALTER TABLE inners_data.nomenclature_resolution OWNER TO inners_data;

--
-- Name: nomenclature_stage; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_stage (
    key text NOT NULL,
    stage text
);


ALTER TABLE inners_data.nomenclature_stage OWNER TO inners_data;

--
-- Name: nomenclature_substage; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE nomenclature_substage (
    key text NOT NULL,
    substage text
);


ALTER TABLE inners_data.nomenclature_substage OWNER TO inners_data;

--
-- Name: plant_data; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE plant_data (
    id integer NOT NULL,
    date timestamp without time zone,
    sensor_id text,
    value double precision,
    day_id integer,
    week_id integer,
    month_id integer,
    year_id integer,
    quality_flags bit(2)
);


ALTER TABLE inners_data.plant_data OWNER TO inners_data;

--
-- Name: TABLE plant_data; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE plant_data IS 'First tier of imported plant data.';


--
-- Name: plant_data_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE plant_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.plant_data_id_seq OWNER TO inners_data;

--
-- Name: plant_data_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE plant_data_id_seq OWNED BY plant_data.id;


--
-- Name: process_log; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE process_log (
    id integer NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    message text,
    plant text,
    success boolean,
    file text
);


ALTER TABLE inners_data.process_log OWNER TO inners_data;

--
-- Name: COLUMN process_log.file; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN process_log.file IS 'Name of input file being processed';


--
-- Name: process_log_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE process_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.process_log_id_seq OWNER TO inners_data;

--
-- Name: process_log_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE process_log_id_seq OWNED BY process_log.id;


--
-- Name: process_queue; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE process_queue (
    file text NOT NULL,
    plant text NOT NULL,
    id integer NOT NULL,
    backup boolean DEFAULT true NOT NULL,
    compress boolean DEFAULT true NOT NULL
);


ALTER TABLE inners_data.process_queue OWNER TO inners_data;

--
-- Name: process_queue_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE process_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.process_queue_id_seq OWNER TO inners_data;

--
-- Name: process_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE process_queue_id_seq OWNED BY process_queue.id;


--
-- Name: replacement_strategies; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE replacement_strategies (
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE inners_data.replacement_strategies OWNER TO inners_data;

--
-- Name: TABLE replacement_strategies; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE replacement_strategies IS 'Table to describe how data can be "fixed" when problems have been detected';


--
-- Name: COLUMN replacement_strategies.name; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN replacement_strategies.name IS 'Name of the strategy';


--
-- Name: COLUMN replacement_strategies.description; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN replacement_strategies.description IS 'Description of the strategy';


--
-- Name: replacement_strategies_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE replacement_strategies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.replacement_strategies_id_seq OWNER TO inners_data;

--
-- Name: replacement_strategies_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE replacement_strategies_id_seq OWNED BY replacement_strategies.id;


--
-- Name: running_queries; Type: VIEW; Schema: inners_data; Owner: inners_data
--

CREATE VIEW running_queries AS
    SELECT pg_stat_activity.datname, pg_stat_activity.procpid, pg_stat_activity.waiting, pg_stat_activity.current_query, pg_stat_activity.query_start FROM pg_stat_activity;


ALTER TABLE inners_data.running_queries OWNER TO inners_data;

--
-- Name: special_kpi_parts; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE special_kpi_parts (
    id integer NOT NULL,
    kpi_part_name text,
    tiered_nomenclature_patterns text[]
);


ALTER TABLE inners_data.special_kpi_parts OWNER TO inners_data;

--
-- Name: COLUMN special_kpi_parts.kpi_part_name; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN special_kpi_parts.kpi_part_name IS 'Name of the kpi_part that this record will be used to calculate';


--
-- Name: COLUMN special_kpi_parts.tiered_nomenclature_patterns; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN special_kpi_parts.tiered_nomenclature_patterns IS 'One or more patterns (without the pant name) that match items in agg_plant_data that will be summed to create the kpi_part.  If there are mutliple patterns, the second will only be used if the first produces no data.';


--
-- Name: special_kpi_parts_id_seq; Type: SEQUENCE; Schema: inners_data; Owner: inners_data
--

CREATE SEQUENCE special_kpi_parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inners_data.special_kpi_parts_id_seq OWNER TO inners_data;

--
-- Name: special_kpi_parts_id_seq; Type: SEQUENCE OWNED BY; Schema: inners_data; Owner: inners_data
--

ALTER SEQUENCE special_kpi_parts_id_seq OWNED BY special_kpi_parts.id;


--
-- Name: static_data; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE static_data (
    sensor_id text NOT NULL,
    daily_value real NOT NULL,
    sum_when_aggregating boolean NOT NULL,
    plant text NOT NULL,
    unit text,
    description text
);


ALTER TABLE inners_data.static_data OWNER TO inners_data;

--
-- Name: TABLE static_data; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON TABLE static_data IS 'Used for storing static data that does not come from the datafiles we receive.  Values here will be inserted as daily values into the agg_plant_data table.';


--
-- Name: COLUMN static_data.sum_when_aggregating; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN static_data.sum_when_aggregating IS 'When aggregating over multi-day periods, should data be summed?  i.e. If True, [1 week value] will be 7 * [1 day value]; if False, [1 week value] == [1 day value]';


--
-- Name: temp; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE temp (
    id integer,
    date timestamp without time zone,
    sensor_id text,
    value double precision,
    day_id integer,
    week_id integer,
    month_id integer,
    year_id integer,
    resolution integer,
    aggregation_method text
);


ALTER TABLE inners_data.temp OWNER TO inners_data;

--
-- Name: transfer; Type: TABLE; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE TABLE transfer (
    remote_name text NOT NULL,
    default_units text,
    plant text,
    inners_stem text NOT NULL,
    target_resolution text,
    nomenclature_name text,
    aggregation_method text,
    min_value real,
    max_value real,
    comments text,
    replacement_strategy_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE inners_data.transfer OWNER TO inners_data;

--
-- Name: COLUMN transfer.remote_name; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.remote_name IS 'Name that the variable has in the data from the respective plant';


--
-- Name: COLUMN transfer.target_resolution; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.target_resolution IS 'We will only accept data with this resolution -- this could be important if we receive data with multiple resolutions.  Leave NULL to accept data of any resolution.';


--
-- Name: COLUMN transfer.nomenclature_name; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.nomenclature_name IS 'Not in use any more!';


--
-- Name: COLUMN transfer.min_value; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.min_value IS 'If this column is populated, the replacement strategy defined in the respective column will take effect.';


--
-- Name: COLUMN transfer.max_value; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.max_value IS 'If this column is populated, the replacement strategy defined in the respective column will take effect. The check against the max-value is only performed after the value has been converted to the standard unit. E.g. if OH is measured in minutes, the max-value will be compared against the converted value in hours.';


--
-- Name: COLUMN transfer.comments; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.comments IS 'Free text -- notes about the item that make it easier to understand';


--
-- Name: COLUMN transfer.replacement_strategy_id; Type: COMMENT; Schema: inners_data; Owner: inners_data
--

COMMENT ON COLUMN transfer.replacement_strategy_id IS 'Id of strategy used to replace imported values that contain errors - see table replacement_strategies';


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY agg_plant_data ALTER COLUMN id SET DEFAULT nextval('agg_plant_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY aggregation_methods ALTER COLUMN id SET DEFAULT nextval('aggregation_methods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY benchmark_values ALTER COLUMN id SET DEFAULT nextval('bechhmark_values_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY benchmarks ALTER COLUMN id SET DEFAULT nextval('benchmarks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY constants ALTER COLUMN id SET DEFAULT nextval('constants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY dario_ke ALTER COLUMN id SET DEFAULT nextval('dario_ke_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY ds_messages ALTER COLUMN id SET DEFAULT nextval('ds_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY import_reports ALTER COLUMN id SET DEFAULT nextval('import_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY interval_types ALTER COLUMN id SET DEFAULT nextval('interval_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY intervals_to_update ALTER COLUMN id SET DEFAULT nextval('intervals_to_update_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_equations ALTER COLUMN id SET DEFAULT nextval('kpi_equations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_parts ALTER COLUMN id SET DEFAULT nextval('kpi_parts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_plant_benchmark ALTER COLUMN id SET DEFAULT nextval('kpi_plant_benchmark_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpis ALTER COLUMN id SET DEFAULT nextval('kpi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY plant_data ALTER COLUMN id SET DEFAULT nextval('plant_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY process_log ALTER COLUMN id SET DEFAULT nextval('process_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY process_queue ALTER COLUMN id SET DEFAULT nextval('process_queue_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY replacement_strategies ALTER COLUMN id SET DEFAULT nextval('replacement_strategies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY special_kpi_parts ALTER COLUMN id SET DEFAULT nextval('special_kpi_parts_id_seq'::regclass);


--
-- Name: aggregation_methods_aggregation_method_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY aggregation_methods
    ADD CONSTRAINT aggregation_methods_aggregation_method_key UNIQUE (aggregation_method);


--
-- Name: aggregation_methods_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY aggregation_methods
    ADD CONSTRAINT aggregation_methods_pkey PRIMARY KEY (id);


--
-- Name: bechhmark_values_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY benchmark_values
    ADD CONSTRAINT bechhmark_values_pkey PRIMARY KEY (id);


--
-- Name: benchmarks_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY benchmarks
    ADD CONSTRAINT benchmarks_pkey PRIMARY KEY (id);


--
-- Name: broken_units_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY broken_units
    ADD CONSTRAINT broken_units_pkey PRIMARY KEY (replace_this);


--
-- Name: constants_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY constants
    ADD CONSTRAINT constants_pkey PRIMARY KEY (id);


--
-- Name: dario_ke_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY dario_ke
    ADD CONSTRAINT dario_ke_pkey PRIMARY KEY (id);


--
-- Name: ds_messages_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY ds_messages
    ADD CONSTRAINT ds_messages_pkey PRIMARY KEY (id);


--
-- Name: import_reports_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY import_reports
    ADD CONSTRAINT import_reports_pkey PRIMARY KEY (id);


--
-- Name: interval_types_interval_type_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY interval_types
    ADD CONSTRAINT interval_types_interval_type_key UNIQUE (interval_type);


--
-- Name: interval_types_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY interval_types
    ADD CONSTRAINT interval_types_pkey PRIMARY KEY (id);


--
-- Name: intervals_to_update_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY intervals_to_update
    ADD CONSTRAINT intervals_to_update_pkey PRIMARY KEY (id);


--
-- Name: kpi_equations_id_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY kpi_equations
    ADD CONSTRAINT kpi_equations_id_key UNIQUE (id);


--
-- Name: kpi_equations_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY kpi_equations
    ADD CONSTRAINT kpi_equations_pkey PRIMARY KEY (id);


--
-- Name: kpi_parts_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY kpi_parts
    ADD CONSTRAINT kpi_parts_pkey PRIMARY KEY (id);


--
-- Name: kpi_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY kpis
    ADD CONSTRAINT kpi_pkey PRIMARY KEY (id);


--
-- Name: kpi_plant_benchmark_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY kpi_plant_benchmark
    ADD CONSTRAINT kpi_plant_benchmark_pkey PRIMARY KEY (id);


--
-- Name: nomenclature_acquisition_type_primary_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_acquisition_type
    ADD CONSTRAINT nomenclature_acquisition_type_primary_key PRIMARY KEY (key);


--
-- Name: nomenclature_device_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_device
    ADD CONSTRAINT nomenclature_device_pkey PRIMARY KEY (key);


--
-- Name: nomenclature_measurement_subject_primary_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_measurement_subject
    ADD CONSTRAINT nomenclature_measurement_subject_primary_key PRIMARY KEY (key);


--
-- Name: nomenclature_medium_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_medium
    ADD CONSTRAINT nomenclature_medium_pkey PRIMARY KEY (key);


--
-- Name: nomenclature_resolution_primary_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_resolution
    ADD CONSTRAINT nomenclature_resolution_primary_key PRIMARY KEY (key);


--
-- Name: nomenclature_stage_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_stage
    ADD CONSTRAINT nomenclature_stage_pkey PRIMARY KEY (key);


--
-- Name: nomenclature_substage_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY nomenclature_substage
    ADD CONSTRAINT nomenclature_substage_pkey PRIMARY KEY (key);


--
-- Name: process_log_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY process_log
    ADD CONSTRAINT process_log_pkey PRIMARY KEY (id);


--
-- Name: process_queue_primary_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY process_queue
    ADD CONSTRAINT process_queue_primary_key PRIMARY KEY (id);


--
-- Name: replacement_strategies_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY replacement_strategies
    ADD CONSTRAINT replacement_strategies_pkey PRIMARY KEY (id);


--
-- Name: special_kpi_parts_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY special_kpi_parts
    ADD CONSTRAINT special_kpi_parts_pkey PRIMARY KEY (id);


--
-- Name: static_data_pkey; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY static_data
    ADD CONSTRAINT static_data_pkey PRIMARY KEY (sensor_id);


--
-- Name: transfer_primar_key; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY transfer
    ADD CONSTRAINT transfer_primar_key PRIMARY KEY (remote_name);


--
-- Name: unique_vals_in_replace_this_column; Type: CONSTRAINT; Schema: inners_data; Owner: inners_data; Tablespace: 
--

ALTER TABLE ONLY broken_units
    ADD CONSTRAINT unique_vals_in_replace_this_column UNIQUE (replace_this);


--
-- Name: agg_plant_data_date_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX agg_plant_data_date_index ON agg_plant_data USING btree (date);


--
-- Name: agg_plant_data_interval_id_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX agg_plant_data_interval_id_index ON agg_plant_data USING btree (interval_id);


--
-- Name: agg_plant_data_interval_type_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX agg_plant_data_interval_type_index ON agg_plant_data USING btree (interval_type);


--
-- Name: agg_plant_data_sensor_id_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX agg_plant_data_sensor_id_index ON agg_plant_data USING btree (sensor_id);


--
-- Name: constants_name_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX constants_name_idx ON constants USING btree (name);


--
-- Name: fki_agg_plant_data_interval_type_pk; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_agg_plant_data_interval_type_pk ON agg_plant_data USING btree (interval_type);


--
-- Name: fki_import_reports_plant_foreign_key; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_import_reports_plant_foreign_key ON import_reports USING btree (plant);


--
-- Name: fki_intervals_to_update_interval_type_foreign_key; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_intervals_to_update_interval_type_foreign_key ON intervals_to_update USING btree (interval_type);


--
-- Name: fki_kpi_parts_plant_foreign_key; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_kpi_parts_plant_foreign_key ON kpi_parts USING btree (plant);


--
-- Name: fki_replacement_strategy_id_foreign_key; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_replacement_strategy_id_foreign_key ON transfer USING btree (replacement_strategy_id);


--
-- Name: fki_static_data_plant; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX fki_static_data_plant ON static_data USING btree (plant);


--
-- Name: kpi_date_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_date_index ON kpis USING btree (date);


--
-- Name: kpi_id_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_id_index ON kpis USING btree (kpi_id);


--
-- Name: kpi_parts_date_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_parts_date_idx ON kpi_parts USING btree (date);


--
-- Name: kpi_parts_interval_id_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_parts_interval_id_idx ON kpi_parts USING btree (interval_id);


--
-- Name: kpi_parts_interval_type_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_parts_interval_type_idx ON kpi_parts USING btree (interval_type);


--
-- Name: kpi_parts_plant_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX kpi_parts_plant_idx ON kpi_parts USING btree (plant);


--
-- Name: nomenclature_measurement_subject_key_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX nomenclature_measurement_subject_key_idx ON nomenclature_measurement_subject USING btree (key);


--
-- Name: plant_data_date_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_date_index ON plant_data USING btree (date);


--
-- Name: plant_data_day_id; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_day_id ON plant_data USING btree (day_id);


--
-- Name: plant_data_month_id; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_month_id ON plant_data USING btree (month_id);


--
-- Name: plant_data_quality_flags_idx; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_quality_flags_idx ON plant_data USING btree (quality_flags);


--
-- Name: plant_data_quality_flags_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_quality_flags_index ON plant_data USING btree (quality_flags);


--
-- Name: plant_data_sensor_id_date_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_sensor_id_date_index ON plant_data USING btree (date, sensor_id);


--
-- Name: plant_data_sensor_id_index; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_sensor_id_index ON plant_data USING btree (sensor_id);


--
-- Name: plant_data_week_id; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_week_id ON plant_data USING btree (week_id);


--
-- Name: plant_data_year_id; Type: INDEX; Schema: inners_data; Owner: inners_data; Tablespace: 
--

CREATE INDEX plant_data_year_id ON plant_data USING btree (year_id);


--
-- Name: agg_plant_data_interval_type_pk; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY agg_plant_data
    ADD CONSTRAINT agg_plant_data_interval_type_pk FOREIGN KEY (interval_type) REFERENCES interval_types(interval_type);


--
-- Name: agg_plant_data_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY agg_plant_data
    ADD CONSTRAINT agg_plant_data_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: bechhmark_values_benchmark_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY benchmark_values
    ADD CONSTRAINT bechhmark_values_benchmark_id_fkey FOREIGN KEY (benchmark_id) REFERENCES benchmarks(id);


--
-- Name: dario_ke_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY dario_ke
    ADD CONSTRAINT dario_ke_id_fkey FOREIGN KEY (id) REFERENCES dario_ke(id);


--
-- Name: import_reports_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY import_reports
    ADD CONSTRAINT import_reports_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: intervals_to_update_interval_type_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY intervals_to_update
    ADD CONSTRAINT intervals_to_update_interval_type_foreign_key FOREIGN KEY (interval_type) REFERENCES interval_types(interval_type);


--
-- Name: kpi_parts_interval_type_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_parts
    ADD CONSTRAINT kpi_parts_interval_type_foreign_key FOREIGN KEY (interval_type) REFERENCES interval_types(interval_type);


--
-- Name: kpi_parts_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_parts
    ADD CONSTRAINT kpi_parts_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: kpi_plant_benchmark_benchmark_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_plant_benchmark
    ADD CONSTRAINT kpi_plant_benchmark_benchmark_id_fkey FOREIGN KEY (benchmark_id) REFERENCES benchmarks(id);


--
-- Name: kpi_plant_benchmark_plant_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_plant_benchmark
    ADD CONSTRAINT kpi_plant_benchmark_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: kpi_plant_benchmark_too_high_message_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_plant_benchmark
    ADD CONSTRAINT kpi_plant_benchmark_too_high_message_id_fkey FOREIGN KEY (too_high_message_id) REFERENCES ds_messages(id);


--
-- Name: kpi_plant_benchmark_too_low_message_id_fkey; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpi_plant_benchmark
    ADD CONSTRAINT kpi_plant_benchmark_too_low_message_id_fkey FOREIGN KEY (too_low_message_id) REFERENCES ds_messages(id);


--
-- Name: kpis_interval_type_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpis
    ADD CONSTRAINT kpis_interval_type_foreign_key FOREIGN KEY (interval_type) REFERENCES interval_types(interval_type);


--
-- Name: kpis_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY kpis
    ADD CONSTRAINT kpis_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: process_log_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY process_log
    ADD CONSTRAINT process_log_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: process_queue_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY process_queue
    ADD CONSTRAINT process_queue_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: replacement_strategy_id_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY transfer
    ADD CONSTRAINT replacement_strategy_id_foreign_key FOREIGN KEY (replacement_strategy_id) REFERENCES replacement_strategies(id);


--
-- Name: static_data_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY static_data
    ADD CONSTRAINT static_data_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: transfer_aggregation_method_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY transfer
    ADD CONSTRAINT transfer_aggregation_method_foreign_key FOREIGN KEY (aggregation_method) REFERENCES aggregation_methods(aggregation_method);


--
-- Name: transfer_plant_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY transfer
    ADD CONSTRAINT transfer_plant_foreign_key FOREIGN KEY (plant) REFERENCES inners_www.user_sys_plants("plantKey");


--
-- Name: transfer_resolution_foreign_key; Type: FK CONSTRAINT; Schema: inners_data; Owner: inners_data
--

ALTER TABLE ONLY transfer
    ADD CONSTRAINT transfer_resolution_foreign_key FOREIGN KEY (target_resolution) REFERENCES nomenclature_resolution(key);


--
-- Name: process_log; Type: ACL; Schema: inners_data; Owner: inners_data
--

REVOKE ALL ON TABLE process_log FROM PUBLIC;
REVOKE ALL ON TABLE process_log FROM inners_data;
GRANT ALL ON TABLE process_log TO inners_data;
GRANT ALL ON TABLE process_log TO PUBLIC;


--
-- Name: process_queue; Type: ACL; Schema: inners_data; Owner: inners_data
--

REVOKE ALL ON TABLE process_queue FROM PUBLIC;
REVOKE ALL ON TABLE process_queue FROM inners_data;
GRANT ALL ON TABLE process_queue TO inners_data;
GRANT ALL ON TABLE process_queue TO PUBLIC;


--
-- PostgreSQL database dump complete
--

