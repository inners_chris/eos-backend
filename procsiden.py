# import datetime                     # For date manipulations
import inners_data_import_tools
import csv                          # For parsing csv files
import gzip

from inners_data_import_tools import logging

filename, testMode = inners_data_import_tools.processParams()

inners_data_import_tools.initialize("HSG")

log = inners_data_import_tools.configureLogging("/home/inners/log/procsiden.log", logging.INFO)     # logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL


def loginfo(msg):
    if testMode:
        print msg
    else:
        log.info(msg)


# Helper class that allows us to open a gzipped file as if it were uncompressed
class gzProcessor(gzip.GzipFile):
    def __enter__(self):
        if self.fileobj is None:
            raise ValueError("I/O operation on closed GzipFile object")
        return self

    def __exit__(self, *args):
        self.close()


# select name from nomenclature as n 
# where not exists (select * from import_data where n.name = import_data.sensor_id limit 1);

loginfo("Processing " + filename)

countedItems = {}

# Choose a processor based on whether the file is compressed or not, which we decide based on the file's extension
if filename.endswith('.gz'):
    processor = gzProcessor
else:
    processor = open


# Process the file!
with processor(filename, 'r') as csvfile:
    lines = csv.reader(csvfile, delimiter=',')

    for line in lines:
        # When we hit a line with fewer than three columns, we're done.  This skips some of the junk at the bottom of the file.
        if(len(line) != 3):
            break

        # Line has 3 parts... put them in nice looking variables
        timeStamp = line[0].split('.')[0]      # Truncates "2013-10-17 00:14:00.000" to "2013-10-17 00:14:00" 
        sidenName = line[1]  
        sidenVal  = line[2]   

        # Skip averaged values
        if sidenName.endswith("_AVG15M") or sidenName.endswith("_AVG2H"):
            continue


        # All Siden data comes with 30 second resolution
        inputResolution = "30S"    # Must be a value from nomenclature_resolution
        inputUnits = inners_data_import_tools.getRemoteUnits(sidenName)

        inners_data_import_tools.addDbRecord(sidenName, sidenVal, inputUnits, inputResolution, timeStamp)


# Dump the results into the database

if testMode:
   inners_data_import_tools.writeToConsole()
else:
   inners_data_import_tools.writeToDatabase()
   

loginfo("Finished.")


