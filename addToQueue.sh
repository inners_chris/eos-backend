#!/bin/bash
#
# Usage: addToQueue <file> <owner>
# Adds file to processing queue if it is of a known type
#
# This script gets run by incron when a new file is added to one of our incoming data folders.
# (run incrontab -l for details)
# $1 contains the full file name including absolute path, $2 contains the plant that the file is for ('HSG', 'BUR', etc.)
# NOTE: This file will also get called as we create temp files, uncompress files, or do anything else that creates a new file
#       in the working folder.  Therefore, this script needs to be very careful about extentions and such.

LOG=/home/inners/log/process.log
BACKUP_MACHINE=innersdev.kirchberg.tudor.lu

pushd `dirname "$1"`      

# Uncompress 7z files
if [[ $1 =~ \.7z$ ]]; then
   echo `date`: Uncompressing $1 >> $LOG
   /usr/bin/p7zip -d "$1"     # -d results in the deletion the original 7z file, even if that is not the purpose of the switch
fi

# Uncompress zip files
# TODO: Add this -- make sure that the original zip file gets deleted here

# Compress csv, xml, and txt files to gz format
if [[ $1 =~ \.csv$ ]] || [[ $1 =~ \.xml$ ]] || [[ $1 =~ \.txt$ ]]; then
   echo `date`: Compressing \(gzip\) $1 >> $LOG

   # We are getting XML files from BURG that contain NULLs.  This sucks.  We'd better
   # try removing them using sed's in-place file editing.  Note that the -i option is
   # not available from all versions of sed, but it is supported by the one we have
   # on the INNERS machine.  Note also that if the datafiles we get are gzipped, this
   # NULL removal will not be run.
   sed -i 's/\x00//g' "$1"
   gzip "$1"
fi

BACKUP_DIR='Unknown folder'

# Add gz files to the queue
if [[ $1 =~ \.gz$ ]]; then

   if [[ "$2" == "HSG" ]]; then
      BACKUP_DIR=data/siden/data
   elif [[ "$2" == "BUR" ]]; then
      BACKUP_DIR=data/wupperverband/data/KLW_Burg
   else
      echo `date`: Unknown plant $2! >> $LOG
      popd
      exit
   fi

   # Backup file
   echo `date`: Backing up "$1" to $BACKUP_MACHINE:$BACKUP_DIR >> $LOG
   scp "$1" "inners@$BACKUP_MACHINE:$BACKUP_DIR/$3"
      
   echo `date`: Queuing "$1" for processing >> $LOG
   psql -U inners_data -d inners_db -c"INSERT INTO process_queue(file, plant, backup) VALUES('$1', '$2', 'true');"
fi

popd        # Restore comsic balance
