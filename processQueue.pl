#!/usr/bin/perl

use strict;
use warnings;

use File::Copy;               # For moving/copying files
use File::Temp 'tempdir';     # For creating a temp folder to work in
use File::Basename;           # For grabbing name/folder from filename
use Fcntl qw(:flock);         # For ensuring we only have one instance of this program running
use Time::HiRes qw( time );   # For determining elapsed time


my $processorFolder = "/home/inners/";    # All processors should live here


# TODO: Put these values in the database

# Define the scripts we'll use for each plant
my %processors = ("HSG", "procsiden.py",
                  "BUR", "procburg.py" );

# This function will grab the next item in the queue.  Items are added to the queue as they arrive, so this function
# will return names in chronological order in which they were loaded into the system.  Therefore, newer data should
# clobber older data if everything is working properly.
sub getNextItemFromQueue
{
   my $row = `psql -U inners_data -d inners_db -t -c"SELECT id, file, plant, backup FROM process_queue ORDER BY id limit 1"`;
   $row =~ s/\n+$//;                      # Strip any trailing newlines -- there may be a few
   return $row;
}


sub logMessage 
{
   my $message = $_[0];
   my $plant   = $_[1];
   my $success = $_[2];
   my $file    = $_[3];

   `psql -U inners_data -d inners_db -t -c"INSERT INTO process_log(message, plant, success, file) VALUES('$message', '$plant', '$success', '$file')"`;
}


sub removeFromQueue
{
   my $id = $_[0];
   `psql -U inners_data -d inners_db -t -c"delete from process_queue where id = $id"`;
}



# Make sure we only have one copy of this running at a time
unless (flock(DATA, LOCK_EX|LOCK_NB)) {
   print "$0 is already running. Exiting.\n";
   exit(1);
}

my $numProcessed = 0;

my $startingDir = `pwd`;

while(1) {
   # Get next file from the queue
   my $row = getNextItemFromQueue();

   last if($row !~ /\|/);    # Quit when queue is empty

   my($id, $originalFile, $plant, $backup) = split(/\|/, $row);  # Split into words

   # Clean up the words a bit more
   $id           =~ s/(^\s+|\s+$)//g;
   $originalFile =~ s/(^\s+|\s+$)//g;  # Full filename with path
   $plant        =~ s/(^\s+|\s+$)//g;
   $backup       =~ s/(^\s+|\s+$)//g;

   my $processor = $processors{$plant};


   my($base, $path) = fileparse($originalFile);   # $base includes the extension, $path is original folder, with trailing /

   # Extract the extension, store in $ext
   $base =~ m/(\.\w+)$/;
   my $ext = $1;

   my $dir = $path;


   # Make sure we have a valid plant... we always should
   if($processor eq "") {
      logMessage("Unknown plant $plant", $plant, 'f', $base);
      removeFromQueue($id);
      next;   
   }

   chdir($dir);                              # Go to our working dir

   my $start = time();

   system("python", $processorFolder.$processor, "$dir/$base");

   my $stop = time();

   if($? != 0) {
      logMessage("Processing failed: $?", $plant, 'f', $base);
      removeFromQueue($id);
      next;
   }


   # Archive the file
   if($backup eq 't') 
   {
      my $processedFolder = $path."processed";

      if(!move("$base", $processedFolder)) 
      {
         logMessage("Could not move file to $processedFolder: $1", $plant, 'f', $base);
         removeFromQueue($id);
         next;
      }
   }

   # Write a record to our log
   my $elapsed = $stop - $start;
   logMessage("Processed file $originalFile -- processing time: $elapsed seconds", $plant, 't', $base);
   $numProcessed++;


   # Remove record from the database
   removeFromQueue($id);

   chdir("$startingDir");  # Something about the quotes makes this work better in testing...
}


if($numProcessed > 0) {
   system("python", "/home/inners/aggregate.py");  # Populates agg_plant_data
}



__DATA__
This exists so flock() code above works.
DO NOT REMOVE THIS DATA SECTION.
