#!/bin/bash
# Adds file to processing queue if it is of a known type -- use for processing a file in place


FILES=/data/wupperverband/data/KLW_Burg/processed/datarequest_report_*

for f in $FILES
do
  echo "Processing $f file..."
  #if(not re.search('datarequest_report_(1_4|2)h_\d+\.xml', filename))
  #/data/wupperverband/data/KLW_Burg/processed/datarequest_report_2h_20130506.xml.gz
  #( [[ $f =~ .*datarequest_report_(1_4|2)h_\d+\.xml\.gz$ ]] || [[ $f =~ .*datarequest_report_(1_4|2)h_\d+\.xml$ ]] ) &&  i
   psql -U inners_data -d inners_db -c"insert into process_queue(file, plant, backup) values('$f', 'BUR', 'false');"
  # take action on each file. $f store current file name
done


FILES=/data/wupperverband/data/KLW_Burg/processed/@@@*

for f in $FILES
do
  echo "Processing $f file..."
  #if(not re.search('datarequest_report_(1_4|2)h_\d+\.xml', filename))
  #/data/wupperverband/data/KLW_Burg/processed/datarequest_report_2h_20130506.xml.gz
  #( [[ $f =~ .*datarequest_report_(1_4|2)h_\d+\.xml\.gz$ ]] || [[ $f =~ .*datarequest_report_(1_4|2)h_\d+\.xml$ ]] ) &&  i
   psql -U inners_data -d inners_db -c"insert into process_queue(file, plant, backup) values('$f', 'BUR', 'false');"
  # take action on each file. $f store current file name
done
