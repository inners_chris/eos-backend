#!/bin/bash
# Adds file to processing queue if it is of a known type -- use for processing a file in place


FILES=/data/siden/data/processed/*.gz
for f in $FILES
do
  echo "Processing $f file..."
  #( [[ $f =~ \.gz$ ]] )  
   psql -U inners_data -d inners_db -c"insert into process_queue(file, plant, backup) values('$f', 'HSG', 'false');"
  # take action on each file. $f store current file name
done
