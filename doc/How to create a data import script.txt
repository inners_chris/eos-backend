How to create a data import script


INNERS EOS import scripts are written in Python, and most of the functionality they require is built into the inners_data_import_tools library.  The scripts themselves must be tailored to the format of the incoming data to be imported; the main task of the script is to parse the incoming data and get it into the correct format.

The first step is to import the library and, for convenience, the logging symbols:

>>> import inners_data_import_tools
>>> from inners_data_import_tools import logging


Next, set up the logging, which uses the standard Python logging infrastructure:

>>> log = inners_data_import_tools.configureLogging(<logfile>, <loglevel>)

Here, logfile is where the log messages should go (usually a file in the log folder of the inners dir), and loglevel is one of [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, or logging.CRITICAL].

You can now write log messages by calling the logger like this:

>>> log.info("Processing " + filename)


When the data parsers are called, the command line will follow a standard pattern.  The following code will help with parsing the command line:

>>> filename, testMode = inners_data_import_tools.processParams()

Here, filename is the name of the input file you will be processing, and testMode will be a boolean that tells you if the user is running with the -t flag.  It is your responsibility to open the data file, and to figure out what to do in the event that -t was specified.  Note that often (but no always), the datafiles will be compressed using gzip, so you will need to figure out how to manage that (which, fortunately, often turns out to be easy).  See the existing data importers for examples of how to parse compressed CSV and XML files.


The next step is to initialize the import tools:

>>> inners_data_import_tools.initialize(<plant_name>)

Here, plant_name is a string value that is the 3 letter code assigned to the plant in the inners_www::user_sys_plants table.


Now you can start parsing your data.  Read your records, and when you have a value to write to the database, call addDbRecord:

>>> inners_data_import_tools.addDbRecord(<remote_name>, <value>, <input_units>, <input_resolution>, <time_stamp>)

Here, inputName is the name of the data element, as defined in the remote_name column of the transfer table.  Value is the value of the sensor for that record.  input_units are the units for that value.  Note that these may differ from the units stored in the EOS database -- the input library will handle the conversion -- but they MUST be dimensionally compatible with the EOS units (e.g. kg/l is compatible with g/m^3, but l/sec is incompatible with kg/hr).  If the units are not specified in the input file, use the getRemoteUnits() function as described below.  input_resolution is the duration over which the data reading is taken (pass a string, and use one of the values defined in the table nomenclature_resolution).  Finally, the timestamp is the time of the ENDING of the period for which the reading occurs.  Note that the timestamp must be in the correct format.  See the notes on timestamps, below.

After all your data records have been added with the addDbRecord command, you can commit them to the database using the following line:

>>> inners_data_import_tools.writeToDatabase()

This will add the records to the database, and do some related database work.  Generally, this will be the last thing you do during the import process.

Alternatively, if you want to see the SQL that would be generated, but do not want to actually commit your records to the database, you can use the command:

>>> inners_data_import_tools.writeToConsole()

This will print out the generated SQL, but leave the database untouched.



The inners_data_import_tools lib provides some useful tools to help you get your data into the correct format and dimensions.

To find the default_units value from the transfer table for a given item, use this:

>>> inners_data_import_tools.getRemoteUnits(<remote_name>)

where remote_name is the corresponding entry in the transfer table.  This can be helpful when an input file does not specify units internally.  (If the file does specify units internally, you should always use those unless you have a reason not to.)


To get the correct data format for specifying time stamps, use:

>>> inners_data_import_tools.getDateFormat()


Important note about timestamps:

All timestamps in the INNERS EOS system that refer to intervals reference the time that the interval ends.  This is how sensor data is typically reported, but it can lead to some confusion at the boundary between days or months.  For example, a 30 second interval referring to the time span 23:59:30 on March 9, 2014 until 00:00:00 on March 10, 2014, will have the timestamp 00:00:00 10-03-2014, even though it is for data that was collected on March 9.  Furthermore, all other 30-second intervals for that same day will have the date 09-03-2014 in their timestamp.  This can be highly confusing to newer users!!!

If, when writing SQL, you get in the habit of writing something like this:

>>> ... WHERE date > start_time and date <= end_time ...
                    ^                      ^
                 Note which is > and which is <= !!!

you will get the correct data for the interval beginning at start_time and ending at end_time.  

Format

All data inserted into the EOS database should be formatted using the format returned by inners_data_import_tools.getDateFormat().  A simple function to format your time might look like this:

>>> def formatDate(date, time):
>>>    return (datetime.datetime
>>>                    .strptime(date + " " + time, '%d.%m.%Y %H:%M')
>>>                    .strftime(inners_data_import_tools.getDateFormat()) )


Good luck!
