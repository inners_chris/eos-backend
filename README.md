Welcome to the INNERS EOS system!

The following scripts form the core of the backend of the INNERS EOS system:

addToQueue.sh --> Used by the data import system to add a new file to the process queue.
aggregate.py --> Runs any queued data aggregations.  To reaggregate all data, use the all parameter.  (python aggregate.py all)
calckpiparts.py --> Recalculates all KPI parts and KPIs.  Runs in 10-15 seconds.
inners_data_import_tools.py --> Library of core EOS functionality.
inners_db_credentials.py --> Passwords and database credentials.  This file is NOT checked into the source repo.
procburg.py/procsiden.py --> Core import scripts for burg and siden.  These interpret the data files we get and get them into the database.
processQueue.pl --> Processes any files in the import queue.
report.py --> Generates a report showing how the data is aggregated.
reprocess_bur/reprocess_hsg --> Bash scripts that queue up all existing data files for reimportation.  Useful if nomenclature changes.
