#!/usr/bin/python

# Run: python procburg.py [-t] <import_file>

from lxml import etree     # For parsing the XML
import sys
import datetime            # For date manipulations
import re                  # For regexp
import gzip                # For reading gzipped CSV files
import csv


import inners_data_import_tools
from inners_data_import_tools import logging


filename, testMode = inners_data_import_tools.processParams()

plantName = "BUR"

# logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL
log = inners_data_import_tools.configureLogging("/home/inners/log/procburg.log", logging.DEBUG)     


# Only process known file types: datarequest_report_1_4h_20121204.xml and datarequest_report_2h_20121204.xml, @@@SAVE...txt

if testMode:
   print "Processing " + filename
else:
   log.info("Processing " + filename)

# Note that we expect the incoming files to be gzipped, but can handle it if it is not
if not (re.search("datarequest_report_(1_4|2)h_\d+\.xml", filename) or  
        re.search("@@@SAVE_.+\Kanal_1.txt",               filename)):  
   # If it's not one of the formats above, ignore it
   sys.exit(0)


inners_data_import_tools.initialize(plantName)


def formatDate(date, time, offset_interval):
   date = datetime.datetime.strptime(date + " " + time, '%d.%m.%Y %H:%M')
   date += offset_interval

   return date.strftime(inners_data_import_tools.getDateFormat())



# Helper class that allows us to open a gzipped file as if it were uncompressed
class gzProcessor(gzip.GzipFile):
    def __enter__(self):
        if self.fileobj is None:
            raise ValueError("I/O operation on closed GzipFile object")
        return self

    def __exit__(self, *args):
        self.close()


# LPEX V2.0
# Datum;Zeit;Kundennummer;Kundenname;eindeutigeKDNr;GEId;GEKANr;KALINr;Linie;eindeutigeLINr;ZPB;Kennzahl;Einheit;Wandlerfaktor;MPDauer;Werte
# 07.02.14;00:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;1555E-5;00000;415E-5;00000;285E-5;00000;240E-5;00000;1755E-5;00000;2380E-5;00000;2465E-5;00000;2375E-5;00000;1155E-5;00000;135E-5;00000;215E-5;00000;555E-5;00000;3110E-5;00000;3765E-5;00000;3900E-5;00000;3885E-5;00000
# 07.02.14;04:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;2575E-5;00000;1650E-5;00000;1810E-5;00000;1840E-5;00000;3325E-5;00000;3855E-5;00000;4210E-5;00000;4560E-5;00000;3595E-5;00000;2920E-5;00000;2785E-5;00000;2665E-5;00000;3940E-5;00000;4330E-5;00000;4425E-5;00000;4390E-5;00000
# 07.02.14;08:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;3010E-5;00000;2300E-5;00000;2055E-5;00000;2025E-5;00000;3445E-5;00000;3745E-5;00000;4160E-5;00000;3730E-5;00000;2900E-5;00000;2325E-5;00000;2255E-5;00000;2135E-5;00000;3570E-5;00000;3845E-5;00000;4380E-5;00000;3715E-5;00000
# 07.02.14;12:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;3285E-5;00000;2045E-5;00000;1990E-5;00000;2120E-5;00000;3515E-5;00000;3845E-5;00000;4130E-5;00000;4375E-5;00000;2860E-5;00000;1975E-5;00000;2340E-5;00000;2085E-5;00000;3440E-5;00000;4090E-5;00000;4025E-5;00000;4030E-5;00000
# 07.02.14;16:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;2750E-5;00000;2135E-5;00000;1960E-5;00000;2015E-5;00000;4005E-5;00000;4475E-5;00000;5115E-5;00000;5620E-5;00000;4865E-5;00000;3975E-5;00000;4015E-5;00000;3780E-5;00000;5185E-5;00000;6040E-5;00000;5860E-5;00000;5600E-5;00000
# 07.02.14;20:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;4260E-5;00000;3225E-5;00000;3155E-5;00000;3070E-5;00000;4245E-5;00000;4550E-5;00000;4420E-5;00000;4245E-5;00000;2965E-5;00000;2105E-5;00000;2185E-5;00000;2145E-5;00000;3510E-5;00000;3885E-5;00000;4275E-5;00000;3890E-5;00000
# 08.02.14;00:15:00;;;;26000235;1;;kWh;;KLW_BG_Kanal_1;SKALAR;kWh;2000;15;2965E-5;00000;2075E-5;00000;2065E-5;00000;2115E-5;00000;3360E-5;00000;3465E-5;00000;3925E-5;00000

def parseTxtFile(filename):
   '''
   Parse data from the LPEX data files... see sample above
   '''
   # Choose a processor based on whether the file is compressed or not, which we decide based on the file's extension
   if filename.endswith('.gz'):
       processor = gzProcessor
   else:
       processor = open

   fifteenMins = datetime.timedelta(0, 60 * 15, 0)      # Interval representing 15 mins


   inputName = "External Energy Supply"    # This is how the item is listed in the transfer table
   inputResolution = "15M"

   with processor(filename, 'r') as csvfile:

      lines = csv.reader(csvfile, delimiter=';')

      for line in lines:
         
         if line[0].startswith("LPEX"):      # Format data
            continue

         if line[0] == "Datum":              # Column headers
            continue

         ct = 0
         values = []
         date = line[0]
         time = line[1]
         # GEid = line[5]
         # GEKANr = line[6]
         inputUnits = line[8]         
         # eindeutigeLINr = line[9]
         # ZPB = line[10] 
         # Kennzahl = line[11]
         # Einheit = line[12]
         multiplier = float(line[13])      # Wandlerfaktor
         # duration = line[14]      # MPDauer, duration of measurement, will always be 15min
         data = line[15:]           # Index 15 is first data value

         skipThisItem = True        # Will toggle to false below, and we will use the first data element

         # Cycle through all the data... skip every second item, which will all be junk ("00000")
         for item in data:
            skipThisItem = not skipThisItem
            if skipThisItem:
               continue

            ct += 1

            # Sometimes, due to a data problems, item will be an empty string; we will treat these as missing data, and insert nothing
            # Here, we will flag the item by setting its value to -1; we'll search for that value below
            if not item:
               item = -1

            values.append(float(item) * multiplier)

         # The final line of this file will only have 7 values... we want to skip this
         # because it contains values for the following day.
         if ct == 7:    
            continue

         innersName = inners_data_import_tools.getInnersName(inputName, inputResolution)

         if innersName is None:
            continue

         startTime = datetime.datetime.strptime(date + " " + time, '%d.%m.%y %H:%M:%S')

         valCount = 0
         for value in values:
            dataTime = startTime + valCount * fifteenMins                              # Calculate the time of the data point
            timeStamp = dataTime.strftime(inners_data_import_tools.getDateFormat())    # And format it

            valCount += 1
            if value == -1:
               continue

            inners_data_import_tools.addDbRecord(inputName, value, inputUnits, inputResolution, timeStamp)
            

   if testMode:
      inners_data_import_tools.writeToConsole()
      print "Finished."

   else:
      inners_data_import_tools.writeToDatabase()
      log.info("Finished.")



def insertPseudoItem(source_name, dest_name, multiplier):
   table_name = inners_data_import_tools.getImportTableName()

   inners_data_import_tools.runSql(
         "WITH data AS (SELECT * FROM " + table_name + " WHERE sensor_id = '" + source_name + "') " +
         "INSERT INTO " + table_name + " " +
         "(date, sensor_id, value, day_id, week_id, month_id, year_id, quality_flags) " +
         "SELECT date, '" + dest_name + "', value * " + str(multiplier) + ", day_id, week_id, month_id, year_id, quality_flags FROM data"
      )



def preprocessData():
   '''
   This function will be called after our data has been stored in the import_data temp table, but before it
   is copied to plant_data.  We need to calculate some new variables here which we will pretend were present
   in the original dataset, and we need to make some corrections to values that have misleading units.
   '''      
   table_name = inners_data_import_tools.getImportTableName()

   # Correct items that are reported as a rate in (m^3/2 hours), but whose units are reported as (m^3/h)
   sql = ( 
      "UPDATE " + table_name + " AS t " +
      "SET value = 0.5 * value " +
      "WHERE sensor_id IN ('BUR_COGEi3_FLObiog_I_VOL_2H','BUR_COGEi4_FLObiog_I_VOL_2H', 'BUR_FLA_TOR_I_VOL_2H')" 
   )

   inners_data_import_tools.runSql(sql)

   # !!! IMPORTANT !!!
   # If you add an entry here, be sure to add a corresponding entry in the transfer table.  This is needed
   # to ensure that variables will be aggregated as you would want them to be.
   #
   # Create some variables we *wish* we had, but don't.  These will be created and inserted into plant_data
   # as if they existed in the imported data file
   #                 Start with this variable      Create this variable     By mulitiplying by this factor  
   insertPseudoItem("BUR_ANDI_PUMhsl1_I_OH_2H",   "BUR_ANDI_PUMhsl1_CAL_EA_2H",         7.6)
   insertPseudoItem("BUR_ANDI_PUMhsl2_I_OH_2H",   "BUR_ANDI_PUMhsl2_CAL_EA_2H",         7.6)
   insertPseudoItem("BUR_BIOaer1_STIR1_I_OH_2H",  "BUR_BIOaer1_STIR1_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOaer1_STIR2_I_OH_2H",  "BUR_BIOaer1_STIR2_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOaer2_STIR1_I_OH_2H",  "BUR_BIOaer2_STIR1_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOaer2_STIR2_I_OH_2H",  "BUR_BIOaer2_STIR2_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOaer3_STIR1_I_OH_2H",  "BUR_BIOaer3_STIR1_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOaer3_STIR2_I_OH_2H",  "BUR_BIOaer3_STIR2_CAL_EA_2H",        2.7)
   insertPseudoItem("BUR_BIOanox1_STIR3_I_OH_2H", "BUR_BIOanox1_STIR3_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIOanox1_STIR4_I_OH_2H", "BUR_BIOanox1_STIR4_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIOanox2_STIR3_I_OH_2H", "BUR_BIOanox2_STIR3_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIOanox2_STIR4_I_OH_2H", "BUR_BIOanox2_STIR4_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIOanox3_STIR3_I_OH_2H", "BUR_BIOanox3_STIR3_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIOanox3_STIR4_I_OH_2H", "BUR_BIOanox3_STIR4_CAL_EA_2H",       1.5)
   insertPseudoItem("BUR_BIO_BLO1_I_OH_2H",       "BUR_BIO_BLO1_CAL_EA_2H",             80)
   insertPseudoItem("BUR_BIO_BLO2_I_OH_2H",       "BUR_BIO_BLO2_CAL_EA_2H",             80)
   insertPseudoItem("BUR_BIO_BLO3_I_OH_2H",       "BUR_BIO_BLO3_CAL_EA_2H",             80)
   insertPseudoItem("BUR_BIO_BLO4_I_OH_2H",       "BUR_BIO_BLO4_CAL_EA_2H",             80)
#   insertPseudoItem("BUR_BIO_BLO5_I_OH_2H",       "BUR_BIO_BLO5_CAL_EA_2H",             53)
#   insertPseudoItem("BUR_BIO_BLO6_I_OH_2H",       "BUR_BIO_BLO6_CAL_EA_2H",             53)
   insertPseudoItem("BUR_BIOP1_STIR1_I_OH_2H",    "BUR_BIOP1_STIR1_CAL_EA_2H",          1.6)
   insertPseudoItem("BUR_BIOP1_STIR2_I_OH_2H",    "BUR_BIOP1_STIR2_CAL_EA_2H",          1.4)
   insertPseudoItem("BUR_BIOP2_STIR1_I_OH_2H",    "BUR_BIOP2_STIR1_CAL_EA_2H",          1.6)
   insertPseudoItem("BUR_BIOP2_STIR2_I_OH_2H",    "BUR_BIOP2_STIR2_CAL_EA_2H",          1.4)
   insertPseudoItem("BUR_BIO_PUMrcir1_I_OH_2H",   "BUR_BIO_PUMrcir1_CAL_EA_2H",         35)
   insertPseudoItem("BUR_BIO_PUMrcir2_I_OH_2H",   "BUR_BIO_PUMrcir2_CAL_EA_2H",         35)
   insertPseudoItem("BUR_DSTH_PFE_I_OH_2H",       "BUR_DSTH_PFE_CAL_EA_2H",             0.6)
   insertPseudoItem("BUR_FATT_PUM_I_OH_2H",       "BUR_FATT_PUM_CAL_EA_2H",             7)
   insertPseudoItem("BUR_FLOF_BLO1_I_OH_2H",      "BUR_FLOF_BLO1_CAL_EA_2H",            43)
   insertPseudoItem("BUR_FLOF_BLO2_I_OH_2H",      "BUR_FLOF_BLO2_CAL_EA_2H",            43)
   insertPseudoItem("BUR_FLOF_BLO3_I_OH_2H",      "BUR_FLOF_BLO3_CAL_EA_2H",            43)
   insertPseudoItem("BUR_FLOF_BLO4_I_OH_2H",      "BUR_FLOF_BLO4_CAL_EA_2H",            2)
   insertPseudoItem("BUR_FLOF_BLO5_I_OH_2H",      "BUR_FLOF_BLO5_CAL_EA_2H",            2)
 #  insertPseudoItem("BUR_FLOF_PUMbw1_I_OH_2H",    "BUR_FLOF_PUMbw1_CAL_EA_2H",          42)
 #  insertPseudoItem("BUR_FLOF_PUMbw2_I_OH_2H",    "BUR_FLOF_PUMbw2_CAL_EA_2H",          42)
 #  insertPseudoItem("BUR_FLOF_PUMbw3_I_OH_2H",    "BUR_FLOF_PUMbw3_CAL_EA_2H",          42)
   insertPseudoItem("BUR_FLOF_PUMcl_I_OH_2H",     "BUR_FLOF_PUMcl_CAL_EA_2H",           5.5)
   insertPseudoItem("BUR_FLOF_PUMdrw_I_OH_2H",    "BUR_FLOF_PUMdrw_CAL_EA_2H",          3.15)
   insertPseudoItem("BUR_FLOF_PUMfoam_I_OH_2H",   "BUR_FLOF_PUMfoam_CAL_EA_2H",         3.85)
   insertPseudoItem("BUR_FLOF_PUMpre1_I_OH_2H",   "BUR_FLOF_PUMpre1_CAL_EA_2H",         0.385)
   insertPseudoItem("BUR_FLOF_PUMpre2_I_OH_2H",   "BUR_FLOF_PUMpre2_CAL_EA_2H",         0.385)
   insertPseudoItem("BUR_FLOF_PUMsluw1_I_OH_2H",  "BUR_FLOF_PUMsluw1_CAL_EA_2H",        5)
   insertPseudoItem("BUR_FLOF_PUMsluw2_I_OH_2H",  "BUR_FLOF_PUMsluw2_CAL_EA_2H",        5)
   insertPseudoItem("BUR_FLOF_STIRsluw_I_OH_2H",  "BUR_FLOF_STIRsluw_CAL_EA_2H",        1.8)
   insertPseudoItem("BUR_HPS_PUM1_I_OH_2H",       "BUR_HPS_PUM1_CAL_EA_2H",             7)
   insertPseudoItem("BUR_HPS_PUM2_I_OH_2H",       "BUR_HPS_PUM2_CAL_EA_2H",             7)
   insertPseudoItem("BUR_HPS_PUM3_I_OH_2H",       "BUR_HPS_PUM3_CAL_EA_2H",             7)
   insertPseudoItem("BUR_HPS_PUM4_I_OH_2H",       "BUR_HPS_PUM4_CAL_EA_2H",             7)
   insertPseudoItem("BUR_PCL_PUMps1_I_OH_2H",     "BUR_PCL_PUMps1_CAL_EA_2H",           4.5)
   insertPseudoItem("BUR_PCL_PUMps2_I_OH_2H",     "BUR_PCL_PUMps2_CAL_EA_2H",           4.5)
   insertPseudoItem("BUR_PCL_SCRA1_I_OH_2H",      "BUR_PCL_SCRA1_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_PCL_SCRA2_I_OH_2H",      "BUR_PCL_SCRA2_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_PSTHo_PUMtpsl_I_OH_2H",  "BUR_PSTHo_PUMtpsl_CAL_EA_2H",        8)
   insertPseudoItem("BUR_SAND_BLO1_I_OH_2H",      "BUR_SAND_BLO1_CAL_EA_2H",            7.5)
   insertPseudoItem("BUR_SAND_BLO2_I_OH_2H",      "BUR_SAND_BLO2_CAL_EA_2H",            3.4)
   insertPseudoItem("BUR_SAND_BLO3_I_OH_2H",      "BUR_SAND_BLO3_CAL_EA_2H",            3.4)
   insertPseudoItem("BUR_SAND_SCRA1_I_OH_2H",     "BUR_SAND_SCRA1_CAL_EA_2H",           0.25)
   insertPseudoItem("BUR_SAND_SCRA2_I_OH_2H",     "BUR_SAND_SCRA2_CAL_EA_2H",           0.25)
   insertPseudoItem("BUR_SAND_STIR_I_OH_2H",      "BUR_SAND_STIR_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_SBA_JCL1_I_OH_2H",       "BUR_SBA_JCL1_CAL_EA_2H",             8.5)
   insertPseudoItem("BUR_SBA_JCL2_I_OH_2H",       "BUR_SBA_JCL2_CAL_EA_2H",             11.58)
   insertPseudoItem("BUR_SBA_PUM1_I_OH_2H",       "BUR_SBA_PUM1_CAL_EA_2H",             7)
   insertPseudoItem("BUR_SBA_PUM2_I_OH_2H",       "BUR_SBA_PUM2_CAL_EA_2H",             7)
   insertPseudoItem("BUR_SCL1_SCRA_I_OH_2H",      "BUR_SCL1_SCRA_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_SCL2_SCRA_I_OH_2H",      "BUR_SCL2_SCRA_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_SCL3_SCRA_I_OH_2H",      "BUR_SCL3_SCRA_CAL_EA_2H",            0.3)
   insertPseudoItem("BUR_SCL_PUMsas1_I_OH_2H",    "BUR_SCL_PUMsas1_CAL_EA_2H",          5.3)
   insertPseudoItem("BUR_SCL_PUMsas2_I_OH_2H",    "BUR_SCL_PUMsas2_CAL_EA_2H",          5.3)
   insertPseudoItem("BUR_SCR_GRI1_I_OH_2H",       "BUR_SCR_GRI1_CAL_EA_2H",             1.2)
   insertPseudoItem("BUR_SCR_GRI2_I_OH_2H",       "BUR_SCR_GRI2_CAL_EA_2H",             1.2)
   insertPseudoItem("BUR_SDEW_COMP_I_OH_2H",      "BUR_SDEW_COMP_CAL_EA_2H",            10.5)
   insertPseudoItem("BUR_SDEW_DOSflo1_I_OH_2H",   "BUR_SDEW_DOSflo1_CAL_EA_2H",         2)
   insertPseudoItem("BUR_SDEW_DOSflo2_I_OH_2H",   "BUR_SDEW_DOSflo2_CAL_EA_2H",         2)
   insertPseudoItem("BUR_SDEW_DOSpre1_I_OH_2H",   "BUR_SDEW_DOSpre1_CAL_EA_2H",         0.6)
   insertPseudoItem("BUR_SDEW_DOSpre2_I_OH_2H",   "BUR_SDEW_DOSpre2_CAL_EA_2H",         0.6)
   insertPseudoItem("BUR_SDEWi_PUMtdsl1_I_OH_2H", "BUR_SDEWi_PUMtdsl1_CAL_EA_2H",       6)
   insertPseudoItem("BUR_SDEWi_PUMtdsl2_I_OH_2H", "BUR_SDEWi_PUMtdsl2_CAL_EA_2H",       6)
   insertPseudoItem("BUR_SDEW_PUMhp_I_OH_2H",     "BUR_SDEW_PUMhp_CAL_EA_2H",           46)
   insertPseudoItem("BUR_SSTH3_BLF_I_OH_2H",      "BUR_SSTH3_BLF_CAL_EA_2H",            0.37)
   insertPseudoItem("BUR_SSTH3_FAN_I_OH_2H",      "BUR_SSTH3_FAN_CAL_EA_2H",            0.03)
   insertPseudoItem("BUR_SSTH3_JCL_I_OH_2H",      "BUR_SSTH3_JCL_CAL_EA_2H",            3.45)
   insertPseudoItem("BUR_SSTH4_BLF_I_OH_2H",      "BUR_SSTH4_BLF_CAL_EA_2H",            0.37)
   insertPseudoItem("BUR_SSTH4_FAN_I_OH_2H",      "BUR_SSTH4_FAN_CAL_EA_2H",            0.03)
   insertPseudoItem("BUR_SSTH4_JCL_I_OH_2H",      "BUR_SSTH4_JCL_CAL_EA_2H",            3.45)
   insertPseudoItem("BUR_SSTH1_CEN_I_OH_2H",      "BUR_SSTH1_CEN_CAL_EA_2H",            28)
   insertPseudoItem("BUR_SSTH2_CEN_I_OH_2H",      "BUR_SSTH2_CEN_CAL_EA_2H",            18)
   insertPseudoItem("BUR_SSTHi1_PUMsas_I_OH_2H",  "BUR_SSTHi1_PUMsas_CAL_EA_2H",        13)
   insertPseudoItem("BUR_SSTHi2_PUMsas_I_OH_2H",  "BUR_SSTHi2_PUMsas_CAL_EA_2H",        13)
   insertPseudoItem("BUR_SSTHi3_PUMsas_I_OH_2H",  "BUR_SSTHi3_PUMsas_CAL_EA_2H",        5.5)
   insertPseudoItem("BUR_SSTHi4_PUMsas_I_OH_2H",  "BUR_SSTHi4_PUMsas_CAL_EA_2H",        5.5)
   insertPseudoItem("BUR_SSTHo1_PUMtssl_I_OH_2H", "BUR_SSTHo1_PUMtssl_CAL_EA_2H",       5.11)
   insertPseudoItem("BUR_SSTHo2_PUMtssl_I_OH_2H", "BUR_SSTHo2_PUMtssl_CAL_EA_2H",       5.11)
   insertPseudoItem("BUR_SSTHo3_PUMtssl_I_OH_2H", "BUR_SSTHo3_PUMtssl_CAL_EA_2H",       5.5)
   insertPseudoItem("BUR_SSTHo4_PUMtssl_I_OH_2H", "BUR_SSTHo4_PUMtssl_CAL_EA_2H",       5.5)
   insertPseudoItem("BUR_TWT_PUM1_I_OH_2H",       "BUR_TWT_PUM1_CAL_EA_2H",             8)
   insertPseudoItem("BUR_TWT_PUM2_I_OH_2H",       "BUR_TWT_PUM2_CAL_EA_2H",             8)
   insertPseudoItem("BUR_WWTP_PUMprw1_I_OH_2H",   "BUR_WWTP_PUMpw1_CAL_EA_2H",          21)
   insertPseudoItem("BUR_WWTP_PUMprw2_I_OH_2H",   "BUR_WWTP_PUMpw2_CAL_EA_2H",          21)




   
def parseXmlFile(filename):
   try:
      parser = etree.XMLParser(recover=True) # recover makes things not break when they hit NULLs in the Burg data!
      # Could add to the above encoding='utf-8'

      # Note that the parser will transparently parse compressed .xml.gz files
      tree = etree.parse(filename, parser=parser)
   except etree.LxmlError as e:
      log.error("Parse error: " + e.error_log.filter_from_level(etree.ErrorLevels.FATAL))
      sys.exit(1)

   # <plant>
   plant = tree.getroot()

   # <plant><report>
   report = plant.find('report')
   
   if(report == None):
      log.error("File %s missing report tag!" % (filename))
      sys.exit(1)


   reportDate = report.get('date')
   reportTime = report.get('aggregation')     # We're expecting 2h or 1/4h


   # Resolution must be a value from nomenclature_resolution
   if(reportTime == "2h"):
      resolution = "2H"
      delta = datetime.timedelta(hours=2)
   elif(reportTime == "1/4h"):
      resolution = "15M"    
      delta = datetime.timedelta(minutes=15) 
   else:
      log.error("Unexpected report aggregation value: " + reportTime)
      sys.exit(1)

   # children of report are <pv>s
   for pv in report:
      hasMw = False  
      pvChildren = list(pv)
      
      for pvChild in pvChildren:

         if(pvChild.tag == "mw"):
            hasMw = True

            inputName       = pv.get('name').encode("UTF-8")
            inputVal        = float(pvChild.get("value"))
            inputUnits      = pv.get('unit').encode("UTF-8")

            timeSearch = re.match('(\d\d):(\d\d)-(\d\d):(\d\d)', pvChild.get("time_slot"))       # 16:00-18:00  or  02:30-02:45

            timeStamp = formatDate(reportDate, timeSearch.group(1) + ':' + timeSearch.group(2), delta)

            inners_data_import_tools.addDbRecord(inputName, inputVal, inputUnits, resolution, timeStamp)


      # If there are no individual <mw> items, check for a <tw> (daily average)

      if(not hasMw):
         tw = pv.find("tw")

         if(tw is not None):

            inputName       = pv.get('name').encode("UTF-8")
            inputVal        = float(tw.get('value'))
            inputUnits      = tw.get('unit').encode("UTF-8")
            inputResolution = "1DAY" 
            timeStamp       = formatDate(reportDate, "00:00", datetime.timedelta(days=1))

            inners_data_import_tools.addDbRecord(inputName, inputVal, inputUnits, inputResolution, timeStamp)

   if testMode:
      inners_data_import_tools.writeToConsole()
      print "Finished processing file " + filename

   else:
      inners_data_import_tools.writeToDatabase(preprocessData)
      log.info("Finished processing file " + filename)





# Select parser
if re.search("\.xml(\.gz)?", filename):
   parseXmlFile(filename)
else:    # Must be a txt file
   parseTxtFile(filename)
