#!/usr/bin/python
# vim: set fileencoding=UTF-8 :

import quantities as pq  # For unit conversions
import re                # For regexp
import psycopg2          # For database access
import sys               # For exit

import logging
# import datetime

from ast import literal_eval


# Get the database connection info
from inners_db_credentials import dbName, dbUsername, dbPassword

outputDateFormat = "%Y-%m-%d %H:%M:%S"                             

# Define a decorator for creating private static vars.  From here:
# http://stackoverflow.com/questions/279561/what-is-the-python-equivalent-of-static-variables-inside-a-function
def static_var(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate



def configureLogging(logfile, loglevel):
    '''
    Set up the logging file -- loglevel should be one of: logging.DEBUG, logging.INFO, 
    logging.WARNING, logging.ERROR, or logging.CRITICAL
    '''

    logger    = logging.getLogger('eos')
    handler   = logging.FileHandler(logfile)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    handler.setFormatter(formatter)
    logger.addHandler(handler) 
    logger.setLevel(loglevel)

    return logger



testMode = False
importFileName = ""

def processParams():
    ''' 
    Process any passed cmd line params    
    Options: -t --> run in test mode (SQL will be printed instead of executed) 
    '''

    global testMode, importFileName

    if len(sys.argv) <= 1:
        print "Usage:", sys.argv[0], "[-t] filename"
        sys.exit()

    importFileName = sys.argv[1]

    if(importFileName == '-t'):
        testMode = True
        importFileName = sys.argv[2]

    return (importFileName, testMode)



def setTestMode(activate):
    global testMode

    testMode = activate



dbConn   = None
dbCursor = None
dbCursor2 = None

def initDatabaseConn():
    '''
    Connect to database and create a cursor we can use for subsequent db operations
    '''
    global dbConn, dbCursor, dbCursor2

    if dbCursor is None:
        # Database credentials are imported from inners_db_credentials.py

        # Establish our database
        dbConn = psycopg2.connect(database=dbName, user=dbUsername, password=dbPassword)

        dbCursor  = dbConn.cursor()
        dbCursor2 = dbConn.cursor()



import_warnings = []
import_errors = []
vars_imported = []
vars_unknown = []



def sanitizeQutes(val):
    '''
    Try to render a string safe for insertion.  We're not trying to defend against an attacker here,
    just to fix some things that routinely make life difficult for us when inserting data.
    '''

    return re.sub("'", "''", val)



def logerr(msg, plant_name=None):
    '''
    Intelligently handle error messages.  Will print rather than log if running in test mode.
    '''    
    global testMode

    if testMode:
        print msg
    else:
        if plant_name:
            dbCursor.execute("INSERT INTO process_log(message, plant, success) VALUES(%s, %s, %s)", (msg, plant_name, False))
        else:
            dbCursor.execute("INSERT INTO process_log(message, success) VALUES(%s, %s)", (msg, False))

        dbConn.commit()

        log = logging.getLogger('eos')
        log.error(msg)

        import_errors.append(msg)



@static_var("alreadySeenMessages", [])
def logwarnx(msg):
    '''
    Intelligently handle warning messages.  Will print rather than log if running in test mode, and
    will suppress subsequent copies of the same message to avoid being swamped.
    '''
    global testMode

    try:
        logwarnx.alreadySeenMessages.index(msg)       # Will throw if msg is not in the list
    except:
        log = logging.getLogger('eos')

        if testMode:
            print msg
        else:
            log.warn(msg)
            import_warnings.append(msg)

        logwarnx.alreadySeenMessages.append(msg)      # Log the message so we don't see it again


plant_name = None        
db_records = []


def initialize(plant):
    global plant_name
    plant_name = plant   # Will be something like "BUR" or "HSG"

    loadCustomUnits()



broken_units = None

# Create a table of string subsititutions we need to run on incoming units.  Based on bad experience.
def loadBrokenUnitsTable():
    global broken_units

    if broken_units is not None:
        return

    initDatabaseConn()

    broken_units = []

    dbCursor.execute("SELECT replace_this, with_this, all_or_nothing FROM broken_units")
    for row in dbCursor:
        broken_units.append({ 'replace_this': row[0], 'with_this': row[1], 'all_or_nothing' : row[2] })



transfer_table = None

def loadTransferTable():
    '''
    Get data element lookup from database.  This is our "transfer table".
    We'll use this for purposes of translating values from our input datasets into our nomenclature.
    '''
    global transfer_table

    if transfer_table is not None:       # Check if we've already loaded the transfer table; don't load twice
        return

    if plant_name is None:
        logging.getLogger('eos').error("plant_name is None --> Must call initialize first!!")
        sys.exit(1)

    initDatabaseConn()

    #                           row[0]       row[1]        row[2]          row[3]         row[4]     row[5]
    dbCursor.execute("SELECT inners_stem, remote_name, default_units, target_resolution, min_value, max_value   \
                      FROM transfer                                                                             \
                      WHERE plant = '" + plant_name + "'")

    transfer_table = { }
    for row in dbCursor:
        transfer_table[row[1].strip()] = { 'inners_stem':  row[0].strip(), 'remote_name':       row[1].strip(), 
                                          'remote_units': row[2],         'target_resolution': row[3],
                                          'min_value':    row[4],         'max_value':         row[5] }



target_units_list = None
resolutionList = None

def loadTargetUnitsTable():
    '''
    resolutionList is a dictionary where we can get the number of seconds in the resolution of an item
    '''

    global target_units_list, resolutionList

    if target_units_list is not None:
        return

    initDatabaseConn()

    sql = "SELECT t.name, m.units, r.resolution_in_secs                                                                           \
              FROM (select inners_stem || '_' || target_resolution as name,                                                       \
                    substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*_[A-Z]+_([A-Za-z0-9]+)$') AS meas_subj,  \
                    target_resolution AS res                                                                                      \
                    from transfer) as t                                                                                           \
                                                                                                                                  \
              INNER JOIN nomenclature_resolution AS r ON r.key = t.res                                                            \
              INNER JOIN nomenclature_measurement_subject AS m ON m.key = t.meas_subj"

    dbCursor.execute(sql)


    target_units_list = { }
    resolutionList  = { }

    for row in dbCursor:
        target_units_list[row[0]] = row[1] or ''      # Protect against null values
        resolutionList[row[0]]  = row[2]            # resolutionList['HSG...'] = 30



eos_name_list = None

def loadInnersNamesTable():
    '''
    Create a list of all known INNERS nomenclature names
    '''
    global eos_name_list

    if eos_name_list is not None:
        return

    initDatabaseConn()

    eos_name_list = { }

    dbCursor.execute("SELECT inners_stem || '_' || target_resolution FROM transfer")
    for row in dbCursor:
        eos_name_list[row[0]] = True



resolutionTable = None
def loadResolutionTable():
    '''
    Create a list of all known INNERS resolution parts.
    '''
    global resolutionTable

    if resolutionTable is not None:
        return

    initDatabaseConn()

    resolutionTable = [ ]

    dbCursor.execute("SELECT key FROM nomenclature_resolution")
    for row in dbCursor:
        resolutionTable.append(row[0])



def fixUnits(unit):
    '''
    Given a unit and our list of broken units, return a fixed up version
    '''
    loadBrokenUnitsTable()

    # Do this in two passes -- first to do the all-or-nothing fixes, then to do the piecemeal ones
    for bu in broken_units:
        if(bu['all_or_nothing']):            # all_or_nothing
            if(unit == bu['replace_this']):   # replace_this
                return bu['with_this']         # with_this

    for bu in broken_units:
        if(not bu['all_or_nothing']):        # not all_or_nothing
            unit = re.sub(bu['replace_this'], bu['with_this'], unit)

    return unit                   # Now fixed up!



def getDateFormat():
    '''
    Return the required date/time format string in format good for use in strftime
    '''
    return outputDateFormat



@static_var("constants", None)
def get_const(const):
    '''
    Get a constant from the database, and return its value -- only used with re.sub!
    '''

    # Check if we already loaded the constants from the database
    if not get_const.constants:
        get_const.constants = {}
        initDatabaseConn()

        dbCursor.execute("SELECT name, value FROM constants")
        for row in dbCursor:
            name, value = row
            get_const.constants['[' + name + ']'] = value

    key = const.group()

    return get_const.constants[key]



@static_var("intervals", None)
def getAggregationIntervals():
    '''
    Return the list of aggregation intervals (currently 'day', 'week', 'month', and 'year').
    Load them from the database if needed.
    '''
    if getAggregationIntervals.intervals:
        return getAggregationIntervals.intervals

    # Otherwise, we need to load the aggregationIntervals from the database
    initDatabaseConn()

    getAggregationIntervals.intervals = []

    dbCursor.execute("SELECT interval_type FROM interval_types")

    for row in dbCursor:
        getAggregationIntervals.intervals.append(row[0])

    return getAggregationIntervals.intervals



@static_var("special_parts", None)
def getSpecialKpiParts():
    '''
    Return the list of values from the special_kpi_parts table
    '''
    if getSpecialKpiParts.special_parts:
        return getSpecialKpiParts.special_parts

    # Otherwise, we need to load the values from the database
    initDatabaseConn()

    getSpecialKpiParts.special_parts = [ ]

    dbCursor.execute("SELECT kpi_part_name, tiered_nomenclature_patterns FROM special_kpi_parts")

    for row in dbCursor:
        getSpecialKpiParts.special_parts.append(row)

    return getSpecialKpiParts.special_parts




# Used in aggregate.py
def getIntervalsToUpdateTable():
    return "intervals_to_update"



def validateResolution(resolution_name):
    ''' 
    Verify that the specified resolution is in our nomenclature_resolution table.
    Returns True or False.
    '''

    loadResolutionTable()       # Lazy initialize

    if resolution_name in resolutionTable:
        return True

    logging.getLogger('eos').error("Unexpected resolution: " + resolution_name + "; "
                                      "to fix, add this value to the nomenclature_resolution table")

    return False



def getAcquisitionType(name):
    ''' 
    Extract Acquisition Type from the passed name 
    '''
    words = name.split('_')
    
    # Parses name:    
    # HSG_WWTPi_FLOww_I_Q_30S
    #  0    1     2   3 4  5
    #                 ^

    return words[3]



def getMeasurementSubject(name):
    ''' 
    Extract Measurement Subject from the passed name 
    '''
    words = name.split('_')
    return words[4]



def setMeasurementSubject(name, subj):
    ''' 
    Set the Measurement Subject of name to subj -- basically swaps out the relevant 
    nomenclaure element to be what is passed in subj
    '''
    words = name.split('_')
    words[4] = subj
    return '_'.join(words)



def isCounterData(name):
    '''
    Return true if name represents counter data
    '''
    return getAcquisitionType(name) == 'Z'



def logUnknownVariable(var):
    '''
    Store this variable in our list of unknown variables for our import_report
    '''
    global vars_unknown

    if not var in vars_unknown:
        vars_unknown.append(var)



def logImportedVariable(var):
    '''
    Store this variable in our list of imported variables for our import_report
    '''
    global vars_imported

    if not var in vars_imported:
        vars_imported.append(var)



def getInnersName(remote_name, input_resolution):
    '''
    Get the INNERS nomenclature name for this item. 
    Return None if that name does not appear in the nomenclature table in the database.
    Also returns None if item does not appear in the transfer table.
    '''
    loadTransferTable()
    loadInnersNamesTable()

    clean_remote_name = remote_name.strip()

    if not clean_remote_name in transfer_table:
        logUnknownVariable(clean_remote_name)
        return None

    # Item is in the transfer table, so we expect to find a matching item in the nomenclature table

    # Check if the resolution is what we expect
    validateResolution(input_resolution)

    resolution_name = input_resolution
    stem            = transfer_table[clean_remote_name]['inners_stem']
    innersName      = stem + '_' + resolution_name

    logImportedVariable(clean_remote_name)


    if transfer_table[clean_remote_name]['target_resolution'] == resolution_name or \
       transfer_table[clean_remote_name]['target_resolution'] is None:

        # Make sure the name is on the "approved" list -- with the removal of nomenclature table, this may be unneeded
        if innersName in eos_name_list:
            return innersName

        logwarnx("Missing nomenclature item: " + innersName)

    else:
        logwarnx("Item found with wrong resolution: " + innersName + " " +
         "(transfer table res is " + transfer_table[clean_remote_name]['target_resolution'] + ", found in datafile with " + resolution_name + ")")

    return None



def getRemoteUnits(remote_name):
    '''
    Return the 'default unit' value for the given remote name, which
    may be blank if default units are not provided for a particular plant
    '''
    loadTransferTable()

    clean_remote_name = remote_name.strip()

    if not clean_remote_name in transfer_table:
        return None

    return transfer_table[remote_name]['remote_units'] or ''



def convertValue(eos_name, input_val, input_units):   
    '''
    Take the input value in input_units, and covert it to whatever output units INNERS wants.  We'll also fix
    known broken units with our handy fixUnits function.
    '''   
    loadTargetUnitsTable()

    # Get the desired output units
    if not eos_name in target_units_list:
        logerr("ERROR: Can't find element " + eos_name + " in the targetUnitsList", plant_name)
        return None

    if eos_name not in resolutionList:
        logerr("ERROR: Could not find a valid resolution for item " + eos_name, plant_name)
        return None


    outputUnits = target_units_list[eos_name]

    if outputUnits is None or outputUnits == "":
        logerr("ERROR: Can't find units for element " + eos_name + "; they must either be specified in the input file or the transfer table", plant_name)
        return None

    # fixUnits() removes any irregularities in the way the input units are expressed
    try:
        inQuantity = pq.Quantity(float(input_val), fixUnits(input_units))
    except LookupError:
        logerr("ERROR: Unknown unit encountered during import: " + input_units + "; if this is a broken unit, add an entry to the broken_units table")
        return None

    timeSlotDuration = pq.Quantity(resolutionList[eos_name], 's')  # Resolution is in seconds


    # Convert from given to desired units
    try: 
        return inQuantity.rescale(outputUnits)
    except ValueError:
        pass


    # Try dividing by time to produce a rate, such as m^3 to m^3/h
    try:
        inQuantityRate = inQuantity / timeSlotDuration

        return inQuantityRate.rescale(outputUnits)
    except ValueError:
        pass

    # Try multiplying by time to handle such conversions as KW to KWh
    try:
        inQuantityTime  = inQuantity * timeSlotDuration
        return inQuantityTime.rescale(outputUnits)
    except ValueError:
        # Incompatible units
        logwarnx("Can't convert " + eos_name + " from " + input_units + " to " + outputUnits)

    return None



def getDbRecord(remote_name, input_value, input_units, input_resolution, timestamp):
    '''
    Helper method for addDbRecord().

    Get a record suitable for writing to the database.  Handles all unit conversion, 
    also does some sanity checks like bounds checking.

    remote_name is the name used by the remote system (not the EOS nomenclature name)

    Returns None if a suitable record can't be generated.
    '''

    quality_flags = "";

    eos_name = getInnersName(remote_name, input_resolution)

    if eos_name is None:      # Means this item is not listed in our transfer table
       return None

    converted_value = convertValue(eos_name, input_value, input_units)

    if converted_value is None:
       if not input_units:
          unitString = "unknown units"
       else:
          unitString = "units " + input_units

       logwarnx("Could not get valid value for item " + eos_name + " with value " + str(converted_value) + " and units " + unitString)

       return None

    #### Quality flags:
    # Bit 1: value is lower than minimum
    # Bit 2: value is greater than maximum
    
    # Compare to min and max values, and log any problems found
    if remote_name in transfer_table and                           \
        transfer_table[remote_name]['min_value'] is not None and   \
        transfer_table[remote_name]['min_value'] > converted_value:
        quality_flags += "1"
    else:
        quality_flags += "0"


    if remote_name in transfer_table and                           \
        transfer_table[remote_name]['max_value'] is not None and   \
        transfer_table[remote_name]['max_value'] < converted_value:
        quality_flags += "1"
    else:
        quality_flags += "0"


    return {'name':eos_name, 'value':converted_value, 'timestamp':timestamp, 'resolution':input_resolution, 'quality_flags':quality_flags}



def addDbRecord(remote_name, input_value, input_units, input_resolution, timestamp):
    '''
    remote_name is the name used by the remote system (not the EOS nomenclature name)

    Adds a record to the list of stuff we will add to the database.  Handles all unit conversion, lookups, etc.
    '''
    global db_records

    record = getDbRecord(remote_name, input_value, input_units, input_resolution, timestamp)

    if record:
        db_records.append(record)



SECONDS_IN_DAY = 86400


def getAggregationSql(interval, interval_constraint):
    '''
    Returns sql for aggregating plant_data to create agg_plant_data, which is aggregated
    over various intervals.
    '''

    val_query = (
        "CASE t.replacement_strategy_id WHEN 3 THEN "
        "   GREATEST(t.min_value, LEAST(t.max_value, value)) "
        "ELSE value END"
    )

    return "INSERT INTO agg_plant_data(sensor_id, value, component_count, zero_count,                    \
                                       date, interval_id, interval_type, plant)                          \
            /* This WITH query (including the data clause below) ran in < 7 mins in early Aug 2014 */    \
            WITH core_data AS (                                                                          \
                SELECT                                                                                   \
                    SUM(" + val_query + ") as sum,                                                       \
                    AVG(" + val_query + ") as avg,                                                       \
                    MAX(" + val_query + ") - MIN(" + val_query + ") as maxmin,                           \
                    COUNT(*) as count,                                                                   \
                    COUNT(nullif(value, 0)) as nonzeros,                                                 \
                    sensor_id, " + interval + "_id                                                       \
                FROM plant_data AS d                                                                     \
                INNER JOIN transfer AS t ON concat(inners_stem, '_', target_resolution) = d.sensor_id    \
                WHERE d.sensor_id IN (                                                                   \
                    /* Get a list of all nomenclature names that have a defined aggregation_method   */  \
                    /*                       This inner block runs in 12-15ms                        */  \
                    SELECT DISTINCT t.name                                                               \
                    FROM (                                                                               \
                            SELECT inners_stem || '_' || target_resolution AS name                       \
                            FROM transfer                                                                \
                            WHERE aggregation_method IS NOT NULL                                         \
                         ) AS t                                                                          \
                )                                                                                        \
                AND t.aggregation_method IS NOT NULL                                                     \
                AND " + interval_constraint + "                                                          \
                /* The following skips records where replacement_stragety is 2 and either of         */  \
                /* the first two quality bits is set (value too low or too high )                    */  \
                AND NOT (t.replacement_strategy_id = 2 AND (d.quality_flags & B'11')::int > 0)           \
                GROUP BY sensor_id, " + interval + "_id                                                  \
            ),                                                                                           \
            /* We need to adjust sum to account for potentially missing samples.  This is faster to    */ \
            /* do as a separate query because we need to divide by one of our aggregates (count), and  */ \
            /* we don't want to have to recompute that, as it's rather expensive.                      */ \
            data AS (                                                                                    \
                SELECT /* Note that the following list should match the col list in core_data above  */  \
                    sum *                                                                                \
                        /* Adjust sum to account for missing values by multiplying by                */  \
                        /*           (expected number of values / actual number of values)           */  \
                        /* Append '.0' to force computation to be casted to float                    */  \
                        ((" + str(SECONDS_IN_DAY) + ".0  * days_in_interval(" + interval + "_id, '" + interval + "') / \
                        res.resolution_in_secs) / count) AS sum,                                         \
                    avg,                                                                                 \
                    maxmin,                                                                              \
                    count,                                                                               \
                    nonzeros,                                                                            \
                    sensor_id,                                                                           \
                    " + interval + "_id                                                                  \
                FROM core_data AS cd                                                                     \
                INNER JOIN transfer AS t ON concat(inners_stem, '_', target_resolution) = cd.sensor_id   \
                INNER JOIN nomenclature_resolution AS res ON t.target_resolution = res.key               \
            )                                                                                            \
                                                                                                         \
                                                                                                         \
            SELECT DISTINCT regexp_replace(t.inners_stem,                                                \
                        '^([A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*)_[A-Z]+_([A-Za-z0-9].*$)',       \
                        '\\1_AVG_\\2') AS sensor_id,                                                     \
                avg AS value, count as component_count, count - nonzeros as zero_count,                  \
                timestamp_from_" + interval + "_id(" + interval + "_id) AS date, " +                     \
                interval + "_id AS interval_id, '" + interval + "' AS interval_type, t.plant AS plant    \
            FROM data d                                                                                  \
            INNER JOIN transfer t ON d.sensor_id = t.inners_stem || '_' || t.target_resolution           \
            where t.aggregation_method = 'AVG'                                                           \
                                                                                                         \
            UNION ALL                                                                                    \
                                                                                                         \
            SELECT DISTINCT regexp_replace(t.inners_stem,                                                \
                        '^([A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*)_[A-Z]+_([A-Za-z0-9].*$)',       \
                        '\\1_SUM_\\2') AS sensor_id,                                                     \
                sum AS value, count as component_count, count - nonzeros as zero_count,                  \
                timestamp_from_" + interval + "_id(" + interval + "_id) AS date, " +                     \
                interval + "_id AS interval_id, '" + interval + "' AS interval_type, t.plant AS plant    \
            FROM data d                                                                                  \
            INNER JOIN transfer t ON d.sensor_id = t.inners_stem || '_' || t.target_resolution           \
            where t.aggregation_method = 'SUM'                                                           \
                                                                                                         \
            UNION ALL                                                                                    \
                                                                                                         \
            SELECT DISTINCT regexp_replace(t.inners_stem,                                                \
                        '^([A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*)_[A-Z]+_([A-Za-z0-9].*$)',       \
                        '\\1_MAX_MIN_\\2') AS sensor_id,                                                 \
                maxmin AS value, count as component_count, count - nonzeros as zero_count,               \
                timestamp_from_" + interval + "_id(" + interval + "_id) AS date, " +                     \
                interval + "_id AS interval_id, '" + interval + "' AS interval_type, t.plant AS plant    \
            FROM data d                                                                                  \
            INNER JOIN transfer t ON d.sensor_id = t.inners_stem || '_' || t.target_resolution           \
            where t.aggregation_method = 'MAX_MIN'"



def getStaticDataValues(sensor_id, value, integrate_over_time, plant, where_clause=""):
    '''
    Generate SQL for creating values for inserting static data or similar into the agg_plant_data table.
    We always insert values as 1 day values.

    Broken out separately for testing purposes.  
    '''
    valstr = str(value)

    if integrate_over_time:
        valstr += ' * days_in_interval(interval_id, interval_type)'

    if where_clause != "":
        where_clause = "WHERE " + where_clause


    return "SELECT MAX(date), '" + sensor_id + "', " + valstr + ",  \
                   interval_id, interval_type, '" + plant + "'      \
            FROM agg_plant_data                                     \
            " + where_clause + "                                    \
            GROUP BY interval_id, interval_type "



def getStaticDataSql(sensor_id, value, integrate_over_time, plant, where_clause=""):
    # Clear out any existing static data... it's easier to insert all of it freshly than it
    # would be to figure out what we need to add

    return "DELETE FROM agg_plant_data WHERE sensor_id = '" + sensor_id + "';                          \
            INSERT INTO agg_plant_data (date, sensor_id, value, interval_id, interval_type, plant) " + \
            getStaticDataValues(sensor_id, value, integrate_over_time, plant, where_clause) + "; "



def insertStaticData():
    '''
    Insert static data into the agg_plant_data table
    '''

    initDatabaseConn()

    dbCursor.execute("select sensor_id, daily_value, sum_when_aggregating, plant from static_data")

    for row in dbCursor:
        sql = getStaticDataSql(row[0], row[1], row[2], row[3])  # No where clause

        dbCursor2.execute(sql)
        dbConn.commit()



def getMinMaxInterval(interval, method):
    '''
    interval should be one of 'day', 'week', 'month', 'year'
    method should be 'MIN' or 'MAX'
    returns the min (or max) interval_id of the specified type, -1 if there are no intervals of that type
    '''
    initDatabaseConn()

    dbCursor.execute("SELECT " + method + "(interval_id)           \
                      FROM intervals_to_update                     \
                      WHERE interval_type = '" + interval + "'")
    return dbCursor.fetchone()[0] or None



def computeAggregateData(dump_only=False):
    '''
    This function is responsible for aggregating and copying data from the plant_data table
    to the agg_plant_data table.  It also inserts static data into agg_plant_data.

    Passing True will cause generated sql to be printed rather than run.
    '''
    initDatabaseConn()

    if not dump_only:
        createAggregationTodoTable()        # Create intervalsToUpdateTable if it doesn't already exist


    for interval in getAggregationIntervals():
        # If there are no records in the intervalsToUpdate table for a given interval, the
        # min/max value will be set to -1 which will tell the script to skip that interval

        interval_min_val = getMinMaxInterval(interval, 'MIN')
        interval_max_val = getMinMaxInterval(interval, 'MAX')

        if(interval_min_val == None or interval_max_val == None):
            continue

        if not dump_only:
            # Clear out any data that might need to be recalcuated, so we have no conflicts
            dbCursor.execute("DELETE FROM agg_plant_data "
                             "WHERE interval_type = '" + interval + "' AND "
                                    "interval_id >= " + str(interval_min_val) + " AND "
                                    "interval_id <= " + str(interval_max_val))

        interval_constraint = ( interval + "_id >= " + str(interval_min_val) + " AND " + 
                                interval + "_id <= " + str(interval_max_val) )


        sql = getAggregationSql(interval, interval_constraint)

        if not dump_only:
            dbCursor.execute(sql)
            dbConn.commit()
        else:
            print sql

    if not dump_only:        
        dbCursor.execute("TRUNCATE intervals_to_update")
        dbConn.commit()

        # Insert our static data (has to be done after the deletes in the block above)
        insertStaticData()



def createAggregationTodoTable():
    '''
    Creates the intervalsToUpdateTable table intervals_to_update if it doesn't already exist
    '''

    initDatabaseConn()

    dbCursor.execute("SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_name=%s)", ("intervals_to_update",))
    exists = dbCursor.fetchone()[0]

    if(not exists):
        dbCursor.execute("CREATE TABLE intervals_to_update (interval_id integer, interval_type text)") 

    dbConn.commit()  



def getVirtNomenclatureTable(method):
    '''
    Creates a virtual look up table of the important nomenclature parts from values lists in the transfer table as well as those
    in the static_data table
    '''

    sql = "SELECT concat(x.stage_substage, x.stage_number, '_', x.device_medium, x.device_number, '_',                                            \
                              '" + method + "', '_', x.measurement_subject) AS name,                                                              \
                       plant, stage_substage, stage_number, device_medium, device_number, measurement_subject                                     \
                                                                                                                                                  \
                FROM (                                                                                                                            \
                    SELECT plant,                                                                                                                 \
                           substring(inners_stem, '^[A-Z]+_([A-Z]+[a-z]*)[0-9]*_[A-Z]+[a-z]*[0-9]*_[A-Z]+_[A-Za-z0-9]+$') AS stage_substage,      \
                           substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*([0-9]*)_[A-Z]+[a-z]*[0-9]*_[A-Z]+_[A-Za-z0-9]+$') AS stage_number,        \
                           substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_([A-Z]+[a-z]*)[0-9]*_[A-Z]+_[A-Za-z0-9]+$') AS device_medium,       \
                           substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*([0-9]*)_[A-Z]+_[A-Za-z0-9]+$') AS device_number,       \
                           substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*_[A-Z]+_([A-Za-z0-9]+)$') AS measurement_subject  \
                    FROM (                                                                                                                        \
                            SELECT plant, inners_stem FROM transfer                                                                               \
                        UNION                                                                                                                     \
                            SELECT plant, sensor_id AS inners_stem FROM static_data                                                               \
                    ) AS transfer_static                                                                                                          \
                ) AS x"

    return sql



def getAggregationQuerySqlPart(element_name, method, where, group_by):
    '''
    Used for generating kpi_parts by auto-aggregating various combinations of agg_plant_data records

    Sample parameter values:
        element_name = "concat(n.stage_substage, n.stage_number, '_', n.device_medium, n.device_number, '_', n.measurement_subject)"
        method      = "SUM"
        group_by     = "interval_type, interval_id, n.plant, n.stage_substage, n.stage_number, n.device_medium, n.device_number, n.measurement_subject"
    '''

    if where != "":
        where = " AND " + where

    sql =  "WITH n as ( " + getVirtNomenclatureTable(method) + ")                                                                         \
                                                                                                                                          \
            SELECT " + element_name + " AS sensor_id, n.plant, MAX(date) AS date, SUM(value) AS value, interval_id, interval_type          \
            FROM agg_plant_data AS d                                                                                                      \
            INNER JOIN n ON d.sensor_id = concat(n.plant, '_', n.name)                                                                    \
            WHERE d.sensor_id IN (                                                                                                        \
                SELECT t.name                                                                                                             \
                FROM (                                                                                                                    \
                        SELECT                                                                                                            \
                            /* The following converts BUR_ANDIi1_FLOraw_I_VOL into BUR_ANDIi1_FLOraw_SUM_VOL */                           \
                            regexp_replace(inners_stem, '^([A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*_)[A-Z]+(_[A-Za-z0-9].*$)',        \
                                                        '\\1" + method + "\\2') AS name,                                                  \
                            /* The following plucks out measurement_subject from the inners_stem */                                       \
                            substring(inners_stem, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*_[A-Z]+_([A-Za-z0-9]+)$') AS meas_subj   \
                        FROM (                                                                                                            \
                            SELECT plant, inners_stem FROM transfer                                                                       \
                            UNION                                                                                                         \
                            SELECT plant, sensor_id as inners_stem FROM static_data                                                       \
                        ) AS transfer_static                                                                                              \
                ) AS t                                                                                                                    \
                INNER JOIN nomenclature_measurement_subject AS ms ON ms.key = meas_subj                                                   \
            ) " + where + "                                                                                                               \
            GROUP BY " + group_by

    return sql



def getAggregationQuerySql(element_name, where, group_by, include_avg_items):
    '''
    Generate SQL for creating KPI parts.  

    Here we will automatically aggregate things based on the group_by clause we are passed.  
    Note that there is no logical way to aggregate AVG items... so we don't attempt to;
    Should generate a query with these columns: "sensor_id, plant, date, value, interval_id, interval_type"
    Wrap parts in ()s to avoid clashes with WITH directive which has the same name in both parts
    '''

    sql = ( "(" + getAggregationQuerySqlPart(element_name, "MAX_MIN", where, group_by) + ")" + 
            " UNION ALL " +                                                                  
            "(" + getAggregationQuerySqlPart(element_name, "SUM", where, group_by) + ")"    )    

    if include_avg_items:
        sql += (" UNION ALL (" + getAggregationQuerySqlPart(element_name, "AVG", where, group_by) + ")" + 
                " UNION ALL (" + getAggregationQuerySqlPart(element_name, "D",   where, group_by) + ")" )

    return sql           



def getAggregationReportSql(tier, element_name, where, group_by, include_avg_items):
    # Note that tier is only used as a sorting element
    # Distinct not strictly needed, but makes things run faster by weeding out dupes earlier

    if where != "":
        where = " WHERE " + where

    sql = "(                                                                                          \
        WITH n AS (" +                                                                                \
                getVirtNomenclatureTable("SUM") +                                                     \
            " UNION ALL " +                                                                           \
                getVirtNomenclatureTable("MAX_MIN")

    if include_avg_items:
        sql += " UNION ALL " +  getVirtNomenclatureTable("AVG") + \
               " UNION ALL " +  getVirtNomenclatureTable("D")

    sql += ")                                                                                         \
                                                                                                      \
        SELECT DISTINCT " + str(tier) + " AS tier, " + element_name + " AS name, sensor_id, n.plant   \
        FROM agg_plant_data AS d                                                                      \
        INNER JOIN n ON d.sensor_id = concat(n.plant, '_', n.name)                                    \
        INNER JOIN nomenclature_measurement_subject AS ms ON ms.key = n.measurement_subject           \
        " + where + "                                                                                 \
        GROUP BY " + group_by + ", sensor_id                                                          \
    )"

    return sql



# Import all items with no aggregation
tierZeroName    = "concat(n.stage_substage, n.stage_number, '_', n.device_medium, n.device_number, '_', n.measurement_subject)"
tierZeroWhere   = ""
tierZerogroup_by = "interval_type, interval_id, n.plant, n.stage_substage, \
                   n.stage_number, n.device_medium, n.device_number, n.measurement_subject"


# Sum over mulitple devices in same stage/substage and device/medium... 
# i.e. generates BIO1_STIR_SUM_EA from BIO1_STIRww1_I_EA and BIO1_STIRww2_I_EA
tierFourName     = "concat(n.stage_substage, n.stage_number, '_', n.device_medium, '_', 'SUM', '_', n.measurement_subject)"
tierFourWhere    = "n.stage_number != ''"       # Avoids duplicating tierOne items below
tierFourgroup_by  = "interval_type, interval_id, n.plant, n.stage_substage, \
                    n.stage_number, n.device_medium, n.measurement_subject"


# HSG_BIO1_TAN_D_VOL
# HSG_BIO2_TAN_D_VOL

# Aggregate by plant, stage, and device -- generates 
#   "SAND_BLO_SUM_EA", "AIR_UVL_SUM_EA", "WWTPi_WWTP_SUM_CODM", etc.
tierOneName      = "concat(n.stage_substage, '_', n.device_medium, '_', 'SUM', '_', n.measurement_subject)"
tierOneWhere     = ""
tierOnegroup_by   = "interval_type, interval_id, n.plant, n.stage_substage, n.device_medium, n.measurement_subject"


# Aggregated by plant and stage -- generates "SSTO_SUM_EA", "BIO_SUM_VOL", "SCL_SUM_EA", etc.
tierTwoName      = "concat(n.stage_substage, '_', 'SUM', '_', n.measurement_subject)"
tierTwoWhere     = ""
tierTwogroup_by   = "interval_type, interval_id, n.plant, n.stage_substage, n.measurement_subject"


# Aggregated for all elements in a plant -- generates "SUM_VOL", "SUM_CODM", "SUM_EA", etc.
tierThreeName    = "concat('SUM', '_', n.measurement_subject)"
tierThreeWhere   = ""
tierThreegroup_by = "interval_type, interval_id, n.plant, n.measurement_subject"


def generateEquationText(equation, val_names):
    '''
    equation looks like item1.value / item2.value
    val_names looks like "('BIO_STIRww_SUM_EA', 'BIO_TAN_SUM_VOL')"

    Return somethign like 'BIO_STIRww_SUM_EA / BIO_TAN_SUM_VOL'
    '''
    try:
        val_names = tuplize(val_names)
    except:
        return "*** Unparsable component_list ***"

    index = 1
    for valName in val_names:
        equation = equation.replace('item' + str(index) + '.value', valName)
        index += 1

    equation = re.sub("days_in_interval *\([^)]+\)", "days_in_interval", equation)

    return equation



def reportKpiParts():
    '''
    Generate a report about what items are aggregated into which aggregationSumOfParts -- this is purely for documentation purposes
    '''
    initDatabaseConn()

    dbCursor.execute(getAggregationReportSql(0, tierZeroName,  tierZeroWhere,  tierZerogroup_by,  True)  + " UNION " +          \
                     getAggregationReportSql(1, tierFourName,  tierFourWhere,  tierFourgroup_by,  False) + " UNION " +          \
                     getAggregationReportSql(2, tierOneName,   tierOneWhere,   tierOnegroup_by,   False) + " UNION " +          \
                     getAggregationReportSql(3, tierTwoName,   tierTwoWhere,   tierTwogroup_by,   False) + " UNION " +          \
                     getAggregationReportSql(4, tierThreeName, tierThreeWhere, tierThreegroup_by, False) + " ORDER BY tier, name, plant")

    print "Generation of kpi_parts from agg_plant_data"
    print "These items are automatically aggregated together to create weekly, monthly, and annual values by adding daily values:"

    ctr = 1
    lastItem = None
    for row in dbCursor:
        if row[1] != lastItem:
            print
            print "[Tier " + str(row[0]) + "]  " + str(ctr) + " " + row[1], " ==> ", row[2]
            city = row[3]
            lastItem = row[1]
            ctr += 1
        else:
            if city != row[3]:
                city = row[3]
                print " " * (len(str(ctr)) + len(row[1]) + 17), "====="
            print " " * (len(str(ctr)) + len(row[1]) + 17), row[2]

    print
    print "The following items are also inserted into the kpi_parts table.  These are defined in the \n"  \
          "special_kpi_parts table. Only the first item with data in each series will be used."

    for item in getSpecialKpiParts():
        print str(ctr) + " " + item[0], " ==> ", ' |OR| '.join(item[1])
        ctr += 1


    print
    print "The following items are also inserted into the kpi_parts table. These are defined in the \n"   \
          "kpi_equations table, and are calculated from other kpi_parts."

    '''
    row[0] = kpi_name
    row[1] = component_list
    row[2] = equation
    row[3] = constraints

    "KPI_BIO_CONS_EAstir_perVOLtan";
    "('BIO_STIRww_SUM_EA', 'BIO_TAN_SUM_VOL')";
    "item1.value / item2.value";
    "item2.value <> 0"
    '''
    for row in getKpiEquations(False):
        equation = generateEquationText(row[2], row[1])

        # Now we need to swap out item<n> with the associated name in the component_list
        print str(ctr) + " " + str(row[0]), " ==> ", equation 
        ctr += 1        

        

# TODO: Needs to be tested with actual tiered data
def getSpecialAggregationSql():
    '''
    getSpecialKpiParts() will return an array that looks like this:
    ["WWTPi_TOTAL_VOL", ["WWTPi%_FLOww_SUM_VOL", "WWTPi%_PUMww_SUM_VOL"]]
        aggItem[0]                         aggItem[1]
         kpiPart                 item                      item

    This function iterates over the rows in this array, and does the following for each:
    1) Creates a variable in special_kpi_parts called WWTPi_TOTAL_VOL
    2) Sets the value to:
        2a) the sum of all agg_plant_data values that look like WWTPi%_FLOww_SUM_VOL (this script will
            prepend the appropriate plant name); or
        2b) if (2a) produces no values, will sum values in agg_plant_data that look like WWTPi%_PUMww_SUM_VOL
        ... 
        and so on, until either a value is found, or all options are exhausted
    '''
    first = True

    sql = "/* plants creates a lookup to get the plant name given a sensor */                      \
           WITH plants AS (                                                                        \
                SELECT name, substring(name, '^([A-Z]+)_') AS plant                                \
                FROM (                                                                             \
                    SELECT DISTINCT sensor_id AS name FROM agg_plant_data                          \
                ) AS agg_plant_data_names                                                          \
            ) /* end of WITH plants AS -- 134 ms */                                                \
                                                                                                   \
             /* tiered_data is a ranked list of possible ways of computing a kpi_part   */         \
             /* we'll choose the highest tier from the available data for each kpi_part */         \
            , tiered_data AS ( "


    for aggItem in getSpecialKpiParts():
        kpiPart = aggItem[0]

        tier = 0

        for item in aggItem[1]:
            if not first:
                sql += " UNION ALL "  # UNION ALL to avoid sorting and uniquing; it is just useless work

            sql += ("SELECT " + str(tier) + " AS tier, '%s' as sensor_id, plants.plant, date, "
                             "sum(value) AS value, interval_id, interval_type "
                    "FROM agg_plant_data AS d "
                    "LEFT JOIN plants ON d.sensor_id = plants.name "
                    "WHERE sensor_id LIKE plants.plant || '_' || '%s' "
                    "GROUP BY plants.plant, date, interval_id, interval_type" % (kpiPart, item))

            first = False
            tier += 1

    sql += ") /* end of WITH tiered_data AS -- 775 ms */                                           \
                                                                                                   \
             /* filtered_data filters out highest tier of data from tiered_data */                 \
             , filtered_data AS (                                                                  \
                SELECT MIN(tier), plant, interval_id, interval_type from tiered_data               \
                GROUP BY sensor_id, interval_type, interval_id, plant                              \
                )   /* end WITH filtered_data AS -- 815 ms */                                      \
                                                                                                   \
            /* >>> Beginning of main query <<< */                                                  \
            SELECT distinct sensor_id, tiered_data.plant, tiered_data.date, tiered_data.value,     \
                   tiered_data.interval_id, tiered_data.interval_type                              \
            FROM filtered_data                                                                     \
            INNER JOIN tiered_data ON tiered_data.plant         = filtered_data.plant        AND   \
                                      tiered_data.interval_id   = filtered_data.interval_id  AND   \
                                      tiered_data.interval_type = filtered_data.interval_type      \
            /* >>> End main query -- 1443 ms <<< */"

    return sql



def fillKpiParts():
    '''
    This function fills the kpi_parts table in three stages 
    1. Calculate automatic aggregations 
    2. Calculate KPI Parts specified in the special_kpi_parts table
    3. Calculate more KPI Parts as specified in the kpi_equations table, where is_kpi is false
    '''
    initDatabaseConn()

    # These calculate so quickly that it's easier to just recompute everything rather than try to pick and choose
    dbCursor.execute("TRUNCATE kpi_parts")

    kpiPartsColList = "sensor_id, plant, date, value, interval_id, interval_type"


    # STEP 1
    # Calculate automatic aggregations -- see comments associated with variables below (i.e. tierZeroName, etc.)
    # By doing this as one large statement with a UNION, we will avoid any duplicates that might be 
    # generated by tierOne and tierFour where stage_number is NULL.  See comments where these are defined.
    dbCursor.execute("INSERT INTO kpi_parts (" + kpiPartsColList + ") ("     +                                      \
                     getAggregationQuerySql(tierZeroName,  tierZeroWhere,  tierZerogroup_by,  True)  + " UNION " +  \
                     getAggregationQuerySql(tierOneName,   tierFourWhere,  tierOnegroup_by,   False) + " UNION " +  \
                     getAggregationQuerySql(tierFourName,  tierOneWhere,   tierFourgroup_by,  False) + " UNION " +  \
                     getAggregationQuerySql(tierTwoName,   tierTwoWhere,   tierTwogroup_by,   False) + " UNION " +  \
                     getAggregationQuerySql(tierThreeName, tierThreeWhere, tierThreegroup_by, False) + ")" )

    dbConn.commit()

    # # See itemsThatAreAggregatedByCopying for elements here -- they are basically copied and renamed
    # for item in itemsThatAreAggregatedByCopying:
    #     sql = "                                                           \
    #         INSERT INTO kpi_parts (" + kpiPartsColList + ")               \
    #             SELECT '" + item[0] + "' as sensor_id,                    \
    #                     substring(sensor_id, '^([A-Z]+)_') as plant,      \
    #                     date, value, interval_id, interval_type           \
    #             FROM agg_plant_data AS d                                  \
    #             WHERE sensor_id = '" + item[1][0] + "' "

    #     dbCursor.execute(sql)
    #     dbConn.commit()  


    # STEP 2
    sql = getSpecialAggregationSql()

    print "========-------=======\n", sql

    dbCursor.execute("INSERT INTO kpi_parts (" + kpiPartsColList + ")" + sql )
    dbConn.commit()

    # STEP 3
    fillKpiPartsFromEquationsTable()



def getKpiCalculationSql(table, kpi_id, kpi_name, component_list, equation, where_clause, insert):
    ''' 
    Pass True or False for insert to specify whether query should be structured as an insert or a simple select

    Should create something that looks like this:
    INSERT INTO kpis (kpi_id, value, date, interval_id, interval_type, plant)                                    
    (                                                                                                          
    SELECT 'WWTP_SLUDGE_AGE' AS sensor_id, item1.value / (item2.value * item3.value) / 24 AS value,          
                                        item1.date, item1.interval_id, item1.interval_type, item1.plant    
    FROM kpi_parts AS item1                                                                                
                                                                                                          
    INNER JOIN kpi_parts AS item2 ON                                                                       
            item1.interval_id   = item2.interval_id   AND                                                  
            item1.interval_type = item2.interval_type AND                                                  
            item2.plant        = item1.plant                                                               
                                                                                                          
    INNER JOIN kpi_parts AS item3 ON                                                                      
            item1.interval_id   = item3.interval_id   AND                                                 
            item1.interval_type = item3.interval_type AND                                                 
            item1.plant         = item3.plant                                                              
                                                                                                          
    WHERE item1.sensor_id    = 'BIO_SUM_TSSM'     AND                                             
            item2.sensor_id  = 'BIO_SCLo_FLOslu'  AND                                              
            item3.sensor_id  = 'BIO_SCLo_TSSslu'  AND                                             
            item2.value <> 0 
    AND
            item2.value <> 0 AND item3.value <> 0  
    )                                                                         
    '''

    sql = ""
    if insert:
        sql += "INSERT INTO " + table + " (" + kpi_id + ", value, date, interval_id, interval_type, plant)"

    # Replace any constants with their numeric value
    # Look for anything that resembles [XXX] where XXX is the name of the constant
    equation = re.sub("\[\w+\]", get_const, equation)

    sql += ("SELECT '" + kpi_name + "' AS sensor_id, " + equation + " as value, "
                    "item1.date,          item1.interval_id, "
                    "item1.interval_type, item1.plant "
            "FROM kpi_parts AS item1 ")
                                                                                        
    # Create the joins
    ctr = 0
    for name in component_list:
        if ctr > 0:        # Skip the first item -- no need to join item1 to itself
            item = "item" + str(ctr + 1)        # item2 or item3

            sql += ("INNER JOIN kpi_parts AS "   + item + " ON "
                        "item1.interval_id   = " + item + ".interval_id   AND "
                        "item1.interval_type = " + item + ".interval_type AND "
                        "item1.plant         = " + item + ".plant ")
        ctr += 1

    # Add the general where clause that applies to all queries
    sql += " WHERE "

    ctr = 0
    for name in component_list:                    
        sql += "item" + str(ctr + 1) + ".sensor_id  = '" + name + "' "
        if ctr < len(component_list) - 1:
            sql += " AND "
        ctr += 1

    # Add an additional where clause, if specified
    if(where_clause is not None):
        sql += " AND " + where_clause

    return sql



# Pass True or False to is_kpi
def getKpiEquations(is_kpi):
    '''
    row[0] = kpi_name
    row[1] = component_list
    row[2] = equation
    row[3] = constraints
    '''
    initDatabaseConn()

    sql = ("SELECT kpi_name, component_list, equation, constraints "
           "FROM kpi_equations where is_kpi = " + str(is_kpi))

    dbCursor.execute(sql)

    rows = []

    # Move contents of select statement to rows array so we can free up our cursor
    for row in dbCursor:
        rows.append(row)

    return rows



def tuplize(item):
    '''
    Evaluate item, and fix one-element tuple... convert ('XYZZY') into ('XYZZY',).  Lame.
    ''' 

    # literal_eval converts row[1] from a string to a tuple   ==>  
    #       "('WWTP_WWTP_SUM_EA', 'WWTPi_WWTP_SUM_CODM')" to ('WWTP_WWTP_SUM_EA', 'WWTPi_WWTP_SUM_CODM')"

    try:
        evaled = literal_eval(item.strip())
    except Exception, ex:
        logerr("Error evaluating item " + str(ex) + " (" + item.strip() + ")")
        quit()

    if type(evaled) is not tuple:
        evaled = (evaled,)      # Make it a tuple

    return evaled



def getFillKpisSql(insert):
    '''
    Generate SQL for all queries needed to fill the kpis table when all kpi parts are available
    '''
    sql = []
    initDatabaseConn()
    rows = getKpiEquations(True)
    for row in rows:
        # Protect against bad values in the kpi_equations table
        if row[0] == "" or row[1] == "" or row[2] == "" or row[3] == "":
            logerr("Bad data in kpi_equations: kpi = " + row[0])
            continue
        
        kpi_name, components, equation, constraints = row
        try:
            component_list = tuplize(components)

        except:
            logerr("Unparsable component_list in kpi = " + kpi_name)
            continue

        # If any elements in the tuple are "DAYS_IN_INTERVAL", then convert it to the appropriate function

        kpiSql = getKpiCalculationSql('kpis', 'kpi_id', kpi_name, component_list, equation, constraints, insert)
        sql.append(kpiSql)

    return sql



def printKpiSql():
    queries = getFillKpisSql(True)

    for query in queries:
        print query



def fillKpis():
    '''
    Calculate all kpis
    '''
    initDatabaseConn()

    dbCursor.execute("TRUNCATE kpis")
    dbConn.commit()

    queries = getFillKpisSql(True)

    for query in queries:
        try:
            dbCursor.execute(query)
            dbConn.commit()
        except Exception, ex:
            dbConn.rollback()
            logerr("Error calculating KPI: " + str(ex) + " (" + query + ")")



def fillKpiPartsFromEquationsTable():
    '''
    Calculate all kpi_parts that have been defined in the kpi_equations table.
    This gets run after other kpi_parts have been calcualted, so the parts can be used in the equations.
    '''
    initDatabaseConn()

    rows = getKpiEquations(False)

    for row in rows:
        # literal_eval converts row[1] from a string to a tuple   ==>  
        #       "('WWTP_WWTP_SUM_EA', 'WWTPi_WWTP_SUM_CODM')" to ('WWTP_WWTP_SUM_EA', 'WWTPi_WWTP_SUM_CODM')
        kpi_name, component_list, equation, constraints = row[0], tuplize(row[1]), row[2], row[3]

        sql = getKpiCalculationSql('kpi_parts', 'sensor_id', kpi_name, component_list, equation, constraints, True)

        print sql

        try:
            dbCursor.execute(sql)
            dbConn.commit()
        except Exception, ex:
            dbConn.rollback()
            logerr("Error calculating KPI_Part: " + str(ex) + " (" + sql + ")")



def loadCustomUnits():
    '''
    These are some custom units and other junk we've seen from partners' datasets.  Here
    we attempt to map some of the malformed units to known good units to make unit parsing
    work better.  Also, see the broken_units table in the database for the _really_ weird units.
    '''
    pq.UnitQuantity ('BOD5',              1*pq.dimensionless,    symbol='BOD5',          aliases=['BSB5'])
    pq.UnitQuantity ('pH',                1*pq.dimensionless,    symbol='pH')
    pq.UnitQuantity ('NTU',               1*pq.dimensionless,    symbol='NTU')
    pq.UnitQuantity ('Euro',              1*pq.dimensionless,    symbol='Euro')
    pq.UnitQuantity ('parts per million', 1e-6*pq.dimensionless, symbol='ppm')
    pq.UnitQuantity ('count',             1e-6*pq.dimensionless, symbol='ct',            aliases=['counts', 'cts'])

    pq.UnitQuantity ('Einwohner',         1*pq.dimensionless,    symbol='EW')
    pq.UnitQuantity ('microsiemens',      1e-6*pq.S,             symbol='µS')
    pq.UnitMass     ('megagram',          1e6*pq.gram,           symbol='Mg',            aliases=['megagrams'])
    # pq.UnitQuantity('kVA',              1000*pq.VA,            symbol='kVA',           aliases=['kilo_volt_ampere'])
    pq.UnitQuantity ('var',                                      symbol='var',           aliases=['volt_ampere_reactive'])
    # pq.UnitQuantity('kvar',             1000*pq.var,           symbol='kvar',          aliases=['kilo_volt_ampere_reactive'])

    pq.UnitQuantity ('millibar',          0.001*pq.bar,          symbol='mbar',          aliases=['millibars'])
    pq.UnitSubstance('millimole',         0.001*pq.mol,          symbol='mmol')
    pq.UnitSubstance('moleMe',                                   symbol='molMe')
    pq.UnitSubstance('moleP',                                    symbol='molP',          aliases=['moleP'])
    pq.UnitTime     ('half_hour',         30*pq.min,             symbol='half-hour')

    # Some aliases of existing units
    pq.UnitTime     ('Monat',             1*pq.month,            symbol=pq.month.symbol, aliases=['monat'])
    pq.UnitLength   ('ml',                1*pq.mL,               symbol=pq.mL.symbol)

    pq.UnitQuantity ('watts',             1*pq.watt,             symbol=pq.watt.symbol,  aliases=['watts', 'volt_ampere', 'volt_amperes', 'VA'])
    pq.UnitQuantity ('kilowatts',         1*pq.kW,               symbol=pq.kW.symbol,    aliases=['KW', 'Kw', 'kilowatts'])
    pq.UnitQuantity ('kilowatthour',      1*pq.kWh,              symbol=pq.kWh.symbol,   aliases=['kilowatthours', 'kilowatt_hours'])   



def getImportTableName():
    return "import_data"



def getInsertSql(record):
    return "INSERT INTO " + getImportTableName() + "                                                                                                                                                                               \
                   (    date,           sensor_id,           value,              day_id,                        week_id,                       month_id,                      year_id,          quality_flags)    \
            VALUES (    '%s',             '%s',               %f,        day_id_from_timestamp('%s'), week_id_from_timestamp('%s'), month_id_from_timestamp('%s'), year_id_from_timestamp('%s'),    '%s')" %      \
                (record["timestamp"], record["name"], record["value"].item(), record["timestamp"],         record["timestamp"],            record["timestamp"],            record["timestamp"], record["quality_flags"])


def runSql(sql):
    '''
    Runs a snippet of SQL... primarily used by import scripts
    '''
    dbCursor.execute(sql)


def cleanup():
    '''
    Close all database connections, and do anything else we need to cleanup when we're done
    '''
    dbCursor.close()
    dbConn.close()



def arrayToPostgresArray(items):
    '''
    Convert list from a Python array to the format used by Postgres to store arrays.
    '''

    list = ', '.join(['"' + sanitizeQutes(str(item)) + '"' for item in items])
    list = "{" + list + "}"

    return list



def writeImportReport():
    '''
    Import reports log various details about the import process.  The data is generated automatically duing the normal course of importation.
    See http://www.postgresql.org/docs/9.1/static/arrays.html for more information.
    '''

    initDatabaseConn()

    # Convert some of our variables into a form acceptable to Postgres
    vars_i = arrayToPostgresArray(vars_imported)
    vars_u = arrayToPostgresArray(vars_unknown)
    warns  = arrayToPostgresArray(import_warnings)
    errs   = arrayToPostgresArray(import_errors)


    sql = "INSERT INTO import_reports (date,       filename,      plant,   vars_imported, vars_unknown, warnings, errors)       \
           VALUES (                    now(),        '%s',        '%s',        '%s',          '%s',      '%s',     '%s'  )" %   \
                  (                             importFileName, plant_name,   vars_i,        vars_u,      warns,    errs)

    dbCursor.execute(sql)
    dbConn.commit()



def writeToDatabase(pre_insert_function=None):
    '''
    Take a dictionary, generate some SQL, and store the values in the database.
    Dict should have 3 elements: INNERS nomenclature 'name',  'value' (in target units), and 'timestamp' for beginning of the period this item is for.

    When finished, this function closes all the database connections.  This should be the last thing the import script does.

    If we pass a function for pre_insert_function, we'll call it after we insert our data into the temp table import_data, but before
    we move that data into the plant_data table.  The passed function can operate on values in import_data and those will be reflected
    in the values written to plant_data.
    '''

    initDatabaseConn()

    import_table_name = getImportTableName()

    global db_records

    # Create a temporary table to hold our import data.  Here we can do some de-duping, and other cleanup, before transferring it
    # to the plant_data table.  This table should be automatically removed by Postgres when the session is finished.
    dbCursor.execute("CREATE TEMP TABLE " + import_table_name + " (LIKE plant_data INCLUDING INDEXES)")
    dbCursor.execute("ALTER TABLE "       + import_table_name + " ALTER id SET DEFAULT nextval('plant_data_id_seq'::regclass)")

    for record in db_records:
        dbCursor.execute(getInsertSql(record))

    # Occasionally, we get data from Siden containing duplicate records.  This will remove items with the same sensor_id and date.
    sql = ( "DELETE FROM " + import_table_name + " USING " + import_table_name + " d  " +
            "WHERE " + import_table_name + ".date = d.date AND " + 
                import_table_name + ".sensor_id = d.sensor_id AND " + 
                import_table_name + ".id < d.id" )

    dbCursor.execute(sql)


    # If we've passed a function, call it now
    if pre_insert_function:
        pre_insert_function()


    # Delete any pre-existing duplicate values in the plant data table
    sql = ( "DELETE FROM plant_data AS p " + 
            "WHERE EXISTS (SELECT * FROM " + import_table_name + " AS d " +
            "WHERE p.date = d.date AND p.sensor_id = d.sensor_id)" )
    dbCursor.execute(sql)


    # Copy the data to plant_data
    colList = "sensor_id, value, date, day_id, week_id, month_id, year_id, quality_flags" 

    sql = ("INSERT INTO plant_data (" + colList + ") " 
           "SELECT " + colList + " " +                 
           "FROM " + import_table_name )
    dbCursor.execute(sql)
    dbConn.commit() 

    # Do any aggregation or quality checks here that can be generalized to all plants

    # See if we have a table for listing altered aggregationIntervals

    createAggregationTodoTable()

    # See if we can find any missing data.  Assumes that data imported represents an entire day.
    # We look for sensor readings that have fewer values than their resolution would suggest they should.
    # This will not report sensors that have 0 values, regardless of how many should be present.
    # This query runs very quickly (~215ms).
    sql = "                                                                                                                                             \
        SELECT sensor_id, wanted, has FROM (                                                                                                            \
            SELECT subq.sensor_id, " + str(SECONDS_IN_DAY) + " / res.resolution_in_secs AS wanted, counts.count AS has FROM (                           \
                    SELECT sensor_id, substring(sensor_id, '^[A-Z]+_[A-Z]+[a-z]*[0-9]*_[A-Z]+[a-z]*[0-9]*_[A-Z]+_[A-Za-z0-9]+_([A-Z0-9]+)$') AS reskey  \
                    FROM (                                                                                                                              \
                            SELECT DISTINCT sensor_id FROM " + import_table_name + "                                                                    \
                        ) AS inner_subquery                                                                                                             \
                ) AS subq                                                                                                                               \
            LEFT JOIN nomenclature_resolution AS res ON res.key = subq.reskey                                                                           \
            LEFT JOIN (                                                                                                                                 \
                    SELECT sensor_id, count(sensor_id) AS count                                                                                         \
                    FROM " + import_table_name + "                                                                                                      \
                    GROUP BY sensor_id                                                                                                                  \
                ) AS counts                                                                                                                             \
                ON counts.sensor_id = subq.sensor_id                                                                                                    \
        ) AS outer_query                                                                                                                                \
        WHERE wanted != has "                                                                                                                              


    dbCursor.execute(sql)
    dbConn.commit() 

    for row in dbCursor:
        sensor, wanted, has = row

        logwarnx("Missing data detected for sensor " + sensor + ": Expected " + str(wanted) + ", found " + str(has))


    # Record which aggregation intervals have been affected by this data import -- we'll use these to limit our aggregation, later
    for interval in getAggregationIntervals():
        dbCursor.execute("INSERT INTO intervals_to_update SELECT MIN(" + interval + "_id), '" + interval + "' FROM plant_data")
        dbCursor.execute("INSERT INTO intervals_to_update SELECT MAX(" + interval + "_id), '" + interval + "' FROM plant_data")

    dbConn.commit()  # Commit all these database inserts to the database

    writeImportReport()

    cleanup()



def writeToConsole():
    '''
    Write records to the console.  This is simiar to the above, but primarily intended for debugging purposes.
    '''

    global db_records

    for record in db_records:
        print getInsertSql(record)

    writeImportReport()  # TODO: Delete this

    cleanup()
