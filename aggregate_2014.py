#!/usr/bin/python

import inners_data_import_tools
import sys

inners_data_import_tools.setTestMode(True)      # Force logging to console


print "Aggregating data for 2014 only..."
inners_data_import_tools.createAggregationTodoTable()       # Will create connection to database that we can use below

intervals = inners_data_import_tools.getAggregationIntervals()
table     = inners_data_import_tools.getIntervalsToUpdateTable()

for interval in intervals:
    inners_data_import_tools.dbCursor.execute("insert into " + table + " (interval_id, interval_type) values(2014000, '" + interval + "')")
    inners_data_import_tools.dbCursor.execute("insert into " + table + " (interval_id, interval_type) values(2015001, '" + interval + "')")

inners_data_import_tools.dbConn.commit()



inners_data_import_tools.computeAggregateData()
inners_data_import_tools.fillKpiParts()
inners_data_import_tools.fillKpis()  
